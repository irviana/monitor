/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doca.dao;

import doca.modelo.DocaCorreoElectronico;
import java.sql.SQLException;
import jdbc.Conexion;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class DocaCorreoElectronicoDao {

    public DocaCorreoElectronicoDao() {

    }

    public RespuestaFuncion insertarCorreoElectronico(DocaCorreoElectronico correo) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "INSERT INTO doca_correo_electronico ("
                        + "         entidad_id, "
                        + "         registro_id,"
                        + "         fecha_registro, "
                        + "         enviado, "
                        + "         entregado, "
                        + "         correo_electronico_plantilla_id, "
                        + "         asunto, "
                        + "         body_html, "
                        + "         email_to, "
                        + "         id_auditoria) "
                        + "   VALUES (?,?,localtimestamp,?,?,?,?,?,?,?)";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setInt(1, correo.getEntidadId());
                conexion.ps.setLong(2, correo.getRegistroId());
                conexion.ps.setBoolean(3, false);
                conexion.ps.setBoolean(4, false);
                conexion.ps.setInt(5, correo.getCorreoElectronicoPlantillaId());
                conexion.ps.setString(6, correo.getAsunto());
                conexion.ps.setString(7, correo.getBodyHtml());
                conexion.ps.setString(8, correo.getEmailTo());
                conexion.ps.setString(9, correo.getIdAuditoria());
                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
