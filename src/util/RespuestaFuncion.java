package util;

public class RespuestaFuncion {

    private boolean ejecuto;
    private String mensaje;
    private Object objeto;


    public RespuestaFuncion() {

    }

    public boolean isEjecuto() {
        return ejecuto;
    }

    public void setEjecuto(boolean ejecuto) {
        this.ejecuto = ejecuto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

}
