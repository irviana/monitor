package util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;
import jdbc.Conexion;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


/**
 *
 * @author george
 */
public class ImpresionDocPdf {

    private StreamedContent file;
    //private final ParamJdbc paramJdbc;

    private static final String PATH_REPORTE_BLANCO = "/reportes/reporteBlanco.pdf";
    private static final String NOMBRE_ARCHIVO_BLANCO = "NoHayDatos.pdf";
    private static final String NOMBRE_ARCHIVO = "titulo";

    public ImpresionDocPdf() {
        //paramJdbc = new ParamJdbc();
    }

    public StreamedContent getFile() {
        if (file == null) {
            setReporteBlanco();
        }
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    private void setReporteBlanco() {
        /*InputStream stream = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                .getResourceAsStream(PATH_REPORTE_BLANCO);
        file = new DefaultStreamedContent(stream, Constantes.MIME_PDF, NOMBRE_ARCHIVO_BLANCO);*/
    }

    public void fileIreportDownload(String fileJasper, Map<String, Object> iparams) {
        Connection con;
        try {
            RespuestaFuncion respuesta;
            Conexion conexion = new Conexion();
            respuesta = conexion.conectarBaseDatos();
            con = conexion.cx;

            BufferedInputStream bIS;
            try (InputStream iS = new FileInputStream(fileJasper)) {
                bIS = new BufferedInputStream(iS);
//                Class.forName(parametros.getDriver()).newInstance();
//                //con = DriverManager.getConnection(paramJdbc.getUrl(), paramJdbc.getUser(), paramJdbc.getPass());
//                String url = parametros.getUrl() + parametros.getHost() + ":" + parametros.getPort() + "/" + parametros.getBaseDatos();
//                con = DriverManager.getConnection(url, parametros.getUser(), parametros.getPassword());

                JasperReport jReport = (JasperReport) JRLoader.loadObject(bIS);
                if (jReport.getLanguage().equals("java")) {
                    iparams.put(JRParameter.REPORT_LOCALE, new Locale("es", "US"));
                    JasperPrint jPrint = JasperFillManager.fillReport(jReport, iparams, con);
                    try (ByteArrayOutputStream bAOS = new ByteArrayOutputStream()) {

                        JRPdfExporter exporterPdf = new JRPdfExporter();
                        exporterPdf.setExporterInput(new SimpleExporterInput(jPrint));
                        exporterPdf.setExporterOutput(new SimpleOutputStreamExporterOutput(bAOS));
                        exporterPdf.exportReport();

                        try (InputStream stream = new ByteArrayInputStream(bAOS.toByteArray())) {
                            String nombreArchivo = (String) iparams.get(NOMBRE_ARCHIVO) + Constantes.EXTENSION_PDF;
                            file = new DefaultStreamedContent(stream, Constantes.MIME_PDF, nombreArchivo);
                            //file = stream;
                        }
                    }
                } else {
                    file = null;
                    System.out.println("El lenguaje del reportes debe ser java");
                }
            }
            bIS.close();
            con.close();
        } catch (IOException | SQLException | JRException ex) {
            file = null;
            ex.printStackTrace();
            System.out.println("Error en fileIreportDownload(): " + ex.getMessage());
        }

    }

}
