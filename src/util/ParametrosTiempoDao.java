package util;

import jdbc.*;
import java.io.File;
import jdbc.util.Constantes;

public class ParametrosTiempoDao {

    private final SimpleSerializable ss;

    public ParametrosTiempoDao() {

        File nuevo;
        nuevo = new File(Constantes.ARCHIVO_TIEMPOS);
        ss = new SimpleSerializable(nuevo);

        if (!nuevo.exists()) {
            ss.serializar(new ParametrosTiempo());
        }
    }

    public RespuestaFuncion setParametrosTiempo(ParametrosTiempo parametrosTiempo) {
        return ss.serializar(parametrosTiempo);
    }

    public RespuestaFuncion getParametrosTiempo() {
        return ss.desSerializar();
    }

}
