package util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constantes {

    public static final String SERVIDOR_EMAIL = "servidor_artesanos2";

    public static final int NUMERO_REGISTROS_CONSULTA = 5;

    public static final int SIN_DECIMALES = 0;
    public static final int NUMERO_DIGITOS_ESTANDAR = 2;
    public static final int NUMERO_DIGITOS_MEDIO = 4;
    public static final int NUMERO_DIGITOS_EXTENDIDO = 6;

    public static final int NUMERO_MINUTOS_HORA = 60;
    public static final int NUMERO_HORAS_DIAS = 24;
    public static final int NUMERO_DIAS_MES = 30;
    public static final int NUMERO_DIAS_ANIO = 360;

    public static final BigDecimal PORCENTAJE = BigDecimal.valueOf(100);

    public static final String MIME_PNG = "image/png";
    public static final String MIME_PDF = "application/pdf";
    public static final String MIME_HTML = "text/html";
    public static final String MIME_XML = "text/xml";
    public static final String MIME_TXT = "text";

    public static final String MIME_XMLAPP = "application/xml";
    public static final String MIME_XMLRFC = "application/rfc+xml";
    
    public static final String EXTENSION_PNG = ".png";
    public static final String EXTENSION_XML = ".xml";
    public static final String EXTENSION_PDF = ".pdf";
    public static final String EXTENSION_TXT = ".txt";

    public static final String MENSAJE_OK = "OK";
    public static final String MENSAJE_ERROR = "ERROR";

    //public static final CrecPeriodicidadPago PERIODICIDAD_PAGO_MENSUAL = new CrecPeriodicidadPago(4);

    /*
     Es la carpeta /tmp/ del servidor de aplicaciones, se usa como repositorios
     para dar formato y cargar archivos a la base
     */
    public static final String PATH_TMP = "/tmp/";
    public static final String FORMATO_UTF_8 = "UTF-8";

    public static final String ZONA_HORARIA_ECUADOR = "America/Guayaquil";

    public static final String PATH_XSD = "/xsd/";
    public static final String PATH_REPORTE = "/reportes/";
    public static final String PATH_LOGO_REPORTE = "/reportes/cooperativa_artesanos.png";
    public static final String PATH_CERTIFICADO = "/certificados/liliana_elizabeth_benitez_chamorro.p12";
    public static final String PATH_LOGO_GENERICO = "/resources/img/report.png";

    public static final String SECUENCIA_ORDEN_COMPRA = "seq_coma_pedido_cab_numero";
    public static final String SECUENCIA_PEDIDO_VENTA = "seq_coma_pedido_venta_numero";
    
    
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat dateFullFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
    public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
    
     /*
            CONSTANTES PARA SRI
    */
    public static final String AMBIENTE_PRUEBAS="1";
    public static final String AMBIENTE_PRODUCCION="2";
    
    public static final String AMBIENTE_PRUEBAS_TEXTO="PRUEBAS";
    public static final String AMBIENTE_PRODUCCION_TEXTO="PRODUCCION";
    
    public static final String TIPO_EMISION="EMISION NORMAL";
    
    /*
            CONSTANTES PARA LA TABLA LISTA DE COMPROBANTES
    */
    /* NOMBRES DE LA CABECERA DE LA TABLA*/
    public static final String NUMERO_COMPROBANTE_CAB="Número comprabante";
    public static final String COMPROBANTE_CAB="Comprabante";
    public static final String ENVIADO_CAB="Enviado";
    public static final String RECIBIDO_CAB="Recibido";
    public static final String AUTORIZACION_SRI_CAB="Autorización SRI";
    
    public static final int NUMERO_COLUMNAS=5;
    
    /*DETALLE DE LA TABLA*/
    public static final String AUTORIZADO="Autorizado";
    public static final String NO_AUTORIZADO="No Autorizado";
    public static final String ENVIADO="Enviado";
    public static final String NO_ENVIADO="No enviado";
    public static final String RECIBIDO="Recibido";
    public static final String NO_RECIBIDO="No Recibido";
    
    /*PARA HACER LA CONSULTA - PARA EL SELECT*/
    public static final boolean SRI_AUTORIZACION=false;
    public static final boolean SRI_ENVIADO=false;
    public static final boolean SRI_RECIBIDO=false;
    public static final boolean FIRMADO=false;
    public static final long TIPO_DIRECCION=1;
    
    /*NOMBRE DE TABLAS*/
    public static final String TABLA_COMA_FACTURA_CAB="coma_factura_cab";
    public static final String TABLA_COMA_RETENCION="coma_retencion";
    public static final String TABLA_COMA_COMPROBANTE_OBJETO="coma_comprobante_objeto";
    public static final String TABLA_COMA_COMPROBANTE_LOG_OBJETO="coma_comprobante_log_objeto";
    public static final String TABLA_COMA_RETENCION_DET="coma_retencion_det";
    
    public static final String TIPO_COMPROBANTE_FACTURA_VENTA="VENTA";
    public static final String TIPO_COMPROBANTE_RETENCION="RETENCION";
    public static final String TIPO_COMPROBANTE_NOTA_DEBITO="NOTA DE DÉBITO";
    public static final String TIPO_COMPROBANTE_NOTA_CREDITO="NOTA DE CRÉDITO";
    
    public static final String OBSERVACION_COMPROBANTE__OBJETO="COMPROBANTE AUTORIZADO";
    
    public static final String COMPROBANTE_AUTORIZADO="AUTORIZADO";
    public static final String COMPROBANTE_NO_AUTORIZADO="NO AUTORIZADO";
    
    public static final String CODIGO_SEGURIDAD="12345678";
    
    public static final String ESTADO_VALIDADO="VALIDADO";
    
    public static final Integer TIPO_TRNX_INTERES = 14;
    public static final Integer TIPO_TRNX_IMPUESTO = 16;
    
    public static final String NUMERO_DOC_SUSTENTO_INVERSION = "999999999999999";
    public static final String TIPO_IMPUESTO = "RENTA";
    
}
