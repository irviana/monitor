package util;


public class ConstantesCatalogo {
    
    public static final String ENTIDAD_CLIENTE = "Cliente";
    public static final String ENTIDAD_CREDITO = "Credito";
    public static final String ENTIDAD_CREDITO_CONSUMO = "CreditoConsumo";
    public static final String ENTIDAD_MICROCREDITO = "Microcredito";
    public static final String ENTIDAD_MICROCREDITOAGRO = "Microcreditoagro";
    public static final String ENTIDAD_FACTURA = "FacturaCab";
    public static final String ENTIDAD_RETENCION = "Retencion";
    public static final String ENTIDAD_GARANTIA = "Garantia";
    public static final String ENTIDAD_EMPLEADO = "Empleado";

    public static final String CAMPO_TIPO_PERSONA = "tipoPersonaId";
    public static final String CAMPO_TIPO_FUNCIONALIDAD_VENTAS = "tipoFuncionalidadVentasId";
    public static final String CAMPO_DESTINO_FINANCIERO_DETALLE = "destinoFinancieroDetalleId";
    public static final String CAMPO_TIPO_GARANTIA = "tipoGarantiaId";


    public static final Integer TIPO_CUENTA_BALANCE_SITUACION = 1;
    public static final Integer TIPO_CUENTA_BALANCE_RESULTADOS = 2;
    
    // public static final String GRUPO_CUENTA_BALANCE_ACTIVOS = "A";
    //public static final String GRUPO_CUENTA_BALANCE_PASIVOS = "P";
    //public static final String GRUPO_CUENTA_BALANCE_EGRESOS = "E";
    //public static final String GRUPO_CUENTA_BALANCE_INGRESOS = "I";
}
