package util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ParametrosTiempo implements Serializable {

    private int tiempoCargaDocumentos;
    private int tiempoFirmarDocumentos;
    private int tiempoEnvioSri;
    private int tiempoEnvioCorreo;
    private int numComprobantes;

    public ParametrosTiempo() {
        tiempoCargaDocumentos = 2;
        tiempoFirmarDocumentos = 2;
        tiempoEnvioSri = 2;
        tiempoEnvioCorreo = 2;
        numComprobantes = 10;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        ois.defaultReadObject();
    }

    public int getTiempoCargaDocumentos() {
        return tiempoCargaDocumentos;
    }

    public void setTiempoCargaDocumentos(int tiempoCargaDocumentos) {
        this.tiempoCargaDocumentos = tiempoCargaDocumentos;
    }

    public int getTiempoFirmarDocumentos() {
        return tiempoFirmarDocumentos;
    }

    public void setTiempoFirmarDocumentos(int tiempoFirmarDocumentos) {
        this.tiempoFirmarDocumentos = tiempoFirmarDocumentos;
    }

    public int getTiempoEnvioSri() {
        return tiempoEnvioSri;
    }

    public void setTiempoEnvioSri(int tiempoEnvioSri) {
        this.tiempoEnvioSri = tiempoEnvioSri;
    }

    public int getTiempoEnvioCorreo() {
        return tiempoEnvioCorreo;
    }

    public void setTiempoEnvioCorreo(int tiempoEnvioCorreo) {
        this.tiempoEnvioCorreo = tiempoEnvioCorreo;
    } 

    public int getNumComprobantes() {
        return numComprobantes;
    }

    public void setNumComprobantes(int numComprobantes) {
        this.numComprobantes = numComprobantes;
    }
    
}
