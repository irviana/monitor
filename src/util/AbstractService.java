package util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class AbstractService {


    public AbstractService() {

    }

    protected RespuestaFuncion validaRespuestaDao(RespuestaFuncion resp) {
        if (!resp.isEjecuto()) {
            resp.setObjeto(null);
            if (resp.getMensaje() == null) {
                resp.setMensaje(Mensajes.ERROR_SIN_MENSAJE_DESDE_DAO);
            }
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, resp.getMensaje());
        }
        return resp;
    }

}
