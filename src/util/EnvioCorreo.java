package util;

import coma.modelo.PlantillaCorreoElectronico;
import coma.modelo.ServidorCorreoElectronico;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EnvioCorreo {

    private static String SMTP_AUTH_USER;
    private static String SMTP_AUTH_PWD;

    public static void enviarCorreoDesdeBd(ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, String nombre,
            byte[] ride,byte[] xml,String emailTo,String asunto,String bodyHtml) {
        System.out.println("emailTo: "+emailTo);
        long t1 = Calendar.getInstance().getTimeInMillis();
        SMTP_AUTH_USER = servidorCorreoElectronico.getSmtpUsuario();
        SMTP_AUTH_PWD = servidorCorreoElectronico.getSmtpClave();

        Properties properties = new Properties();
        properties.put("mail.smtp.host", servidorCorreoElectronico.getSmtpHost());
        properties.put("mail.smtp.socketFactory.port", servidorCorreoElectronico.getSmtpPuerto());
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", servidorCorreoElectronico.getSmtpPuerto());
        long t2 = Calendar.getInstance().getTimeInMillis();
        Authenticator autenticador = new SMTPAuthenticator();
        Session session = Session.getInstance(properties, autenticador);
        long t3 = Calendar.getInstance().getTimeInMillis();
        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EnvioCorreo.SMTP_AUTH_USER));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
            //message.setSubject(plantillaCorreoElectronico.getAsunto());
            message.setSubject(asunto);
            message.setSentDate(new Date());
            long t4 = Calendar.getInstance().getTimeInMillis();
            MimeBodyPart messagePart = new MimeBodyPart();
            //messagePart.setContent(plantillaCorreoElectronico.getBodyHtml(), Constantes.MIME_HTML);
            messagePart.setContent(bodyHtml, Constantes.MIME_HTML);
            long t5 = Calendar.getInstance().getTimeInMillis();
            // Archivo XML
            MimeBodyPart attachmentPartXML = new MimeBodyPart();
            attachmentPartXML.setDataHandler(new DataHandler(xml, Constantes.MIME_XMLRFC));
            attachmentPartXML.setFileName(nombre + Constantes.EXTENSION_XML);

            long t6 = Calendar.getInstance().getTimeInMillis();
            // Archivo RIDE
            MimeBodyPart attachmentPartRIDE = new MimeBodyPart();
            attachmentPartRIDE.setDataHandler(new DataHandler(ride, Constantes.MIME_PDF));
            attachmentPartRIDE.setFileName(nombre + Constantes.EXTENSION_PDF);

            long t7 = Calendar.getInstance().getTimeInMillis();
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(attachmentPartXML);
            multipart.addBodyPart(attachmentPartRIDE);
            long t8 = Calendar.getInstance().getTimeInMillis();
            message.setContent(multipart);
            long t9 = Calendar.getInstance().getTimeInMillis();
            System.out.println("SIze mensaje: " + message.getSize());
            Transport.send(message);
            long t10 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Propiedades: " + (t2 - t1) + "ms");
            System.out.println("Session: " + (t3 - t2) + "ms");
            System.out.println("Mensaje: " + (t4 - t3) + "ms");
            System.out.println("Cuerpo Html: " + (t5 - t4) + "ms");
            System.out.println("Obtiene Xml: " + (t6 - t5) + "ms");
            System.out.println("Obtiene Pdf: " + (t7 - t6) + "ms");
            System.out.println("Une las partes: " + (t8 - t7) + "ms");
            System.out.println("Agrega al mensaje: " + (t9 - t8) + "ms");
            System.out.println("Envia mensaje: " + (t10 - t9) + "ms");
            System.out.println("Total: " + (t10 - t1) + "ms");

        } catch (MessagingException ex) {
            System.out.println("Envío de Correo Electrónico: " + ex);

        }
    }

//    public static void enviarCorreoDesdePrueba(ServidorCorreoElectronico servidorCorreoElectronico,
//            PlantillaCorreoElectronico plantillaCorreoElectronico, String nombre,
//             String emailTo, String asunto, String bodyHtml) {
//        System.out.println("emailTo: " + emailTo);
//        long t1 = Calendar.getInstance().getTimeInMillis();
//        SMTP_AUTH_USER = servidorCorreoElectronico.getSmtpUsuario();
//        SMTP_AUTH_PWD = servidorCorreoElectronico.getSmtpClave();
//
////        Properties properties = new Properties();
////        properties.put("mail.smtp.host", servidorCorreoElectronico.getSmtpHost());
////        properties.put("mail.smtp.socketFactory.port", servidorCorreoElectronico.getSmtpPuerto());
////        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////        properties.put("mail.smtp.auth", "true");
////        properties.put("mail.smtp.port", servidorCorreoElectronico.getSmtpPuerto());
////        long t2 = Calendar.getInstance().getTimeInMillis();
////        Authenticator autenticador = new SMTPAuthenticator();
////        Session session = Session.getInstance(properties, autenticador);
////        long t3 = Calendar.getInstance().getTimeInMillis();
//        try {
//            
//            Properties props = new Properties();
//	//设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
//	props.put("mail.smtp.host", servidorCorreoElectronico.getSmtpHost());
//	//需要经过授权，也就是有户名和密码的校验，这样才能通过验证
//	props.put("mail.smtp.auth", "true");
//	//用刚刚设置好的props对象构建一个session
//	Session session = Session.getDefaultInstance(props);
//	//有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使
//	//用（你可以在控制台（console)上看到发送邮件的过程）
////	session.setDebug(true);
//	//用session为参数定义消息对象
//	MimeMessage message = new MimeMessage(session);
//	
//		//加载发件人地址
//		message.setFrom(new InternetAddress(EnvioCorreo.SMTP_AUTH_USER));
//		//加载收件人地址
//		message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailTo));
//		//加载标题
//		message.setSubject("Prueba");
//		// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
//		Multipart multipart = new MimeMultipart();         
//
//		//设置邮件的文本内容
//		BodyPart contentPart = new MimeBodyPart();
//		contentPart.setText("Prueba de envio de correo");
//		multipart.addBodyPart(contentPart);
//		//添加附件
////		if(affix!=null&&!"".equals(affix)&&affixName!=null&&!"".equals(affixName)){
////			BodyPart messageBodyPart= new MimeBodyPart();
////			DataSource source = new FileDataSource(affix);
////			//添加附件的内容
////			messageBodyPart.setDataHandler(new DataHandler(source));
////			//添加附件的标题
////			messageBodyPart.setFileName(MimeUtility.encodeText(affixName));
////			multipart.addBodyPart(messageBodyPart);
////		}
//		//将multipart对象放到message中
//		message.setContent(multipart);
//		//保存邮件
//		message.saveChanges();
//                long t9 = Calendar.getInstance().getTimeInMillis();
//		//   发送邮件
//		Transport transport = session.getTransport("smtp");
//		//连接服务器的邮箱
//		transport.connect(servidorCorreoElectronico.getSmtpHost()
//                        , EnvioCorreo.SMTP_AUTH_USER, EnvioCorreo.SMTP_AUTH_PWD);
//		//把邮件发送出去
//		transport.sendMessage(message, message.getAllRecipients());
//		transport.close();
//                long t10 = Calendar.getInstance().getTimeInMillis();
//
////            MimeMessage message = new MimeMessage(session);
////            message.setFrom(new InternetAddress(EnvioCorreo.SMTP_AUTH_USER));
////            message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
////            message.setSubject("COMPROBANTE ELECTRÓNICO");
////            message.setSentDate(new Date());
////            long t4 = Calendar.getInstance().getTimeInMillis();
////            MimeBodyPart messagePart = new MimeBodyPart();
////            messagePart.setContent(bodyHtml, Constantes.MIME_HTML);
////            long t5 = Calendar.getInstance().getTimeInMillis();
////            // Archivo XML
//////            MimeBodyPart attachmentPartXML = new MimeBodyPart();
//////            attachmentPartXML.setDataHandler(new DataHandler(xml, Constantes.MIME_XMLRFC));
//////            attachmentPartXML.setFileName(nombre + Constantes.EXTENSION_XML);
////
////            long t6 = Calendar.getInstance().getTimeInMillis();
////            // Archivo RIDE
//////            MimeBodyPart attachmentPartRIDE = new MimeBodyPart();
//////            attachmentPartRIDE.setDataHandler(new DataHandler(ride, Constantes.MIME_PDF));
//////            attachmentPartRIDE.setFileName(nombre + Constantes.EXTENSION_PDF);
////            long t7 = Calendar.getInstance().getTimeInMillis();
////            Multipart multipart = new MimeMultipart();
////            multipart.addBodyPart(messagePart);
//////            multipart.addBodyPart(attachmentPartRIDE);
////            long t8 = Calendar.getInstance().getTimeInMillis();
////            message.setContent(multipart);
////            long t9 = Calendar.getInstance().getTimeInMillis();
////
////            Transport.send(message);
////            long t10 = Calendar.getInstance().getTimeInMillis();
//
////            System.out.println("Propiedades: " + (t2 - t1) + "ms");
////            System.out.println("Session: " + (t3 - t2) + "ms");
////            System.out.println("Mensaje: " + (t4 - t3) + "ms");
////            System.out.println("Cuerpo Html: " + (t5 - t4) + "ms");
////            System.out.println("Obtiene Xml: " + (t6 - t5) + "ms");
////            System.out.println("Obtiene Pdf: " + (t7 - t6) + "ms");
////            System.out.println("Une las partes: " + (t8 - t7) + "ms");
////            System.out.println("Agrega al mensaje: " + (t9 - t8) + "ms");
//            System.out.println("Envia mensaje: " + (t10 - t9) + "ms");
////            System.out.println("Total: " + (t10 - t1) + "ms");
//
//        } catch (MessagingException ex) {
//            System.out.println("Envío de Correo Electrónico: " + ex);
//
//        }
//    }

//    public static boolean enviarCorreo(ServidorCorreoElectronico servidorEmail, String destinatario,
//            String copiaCorreo, String copiaOcultaCorreo, String asunto, String mensaje) {
//
//        long t1 = Calendar.getInstance().getTimeInMillis();
//        SMTP_AUTH_USER = servidorEmail.getSmtpUsuario();
//        SMTP_AUTH_PWD = servidorEmail.getSmtpClave();
//
//        Properties properties = new Properties();
//        properties.put("mail.smtp.host", servidorEmail.getSmtpHost());
//        properties.put("mail.smtp.socketFactory.port", servidorEmail.getSmtpPuerto());
//        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.port", servidorEmail.getSmtpPuerto());
//        long t2 = Calendar.getInstance().getTimeInMillis();
//        Authenticator autenticador = new SMTPAuthenticator();
//        Session session = Session.getInstance(properties, autenticador);
//        long t3 = Calendar.getInstance().getTimeInMillis();
//        try {
//            MimeMessage message = new MimeMessage(session);
//            message.setFrom(new InternetAddress(EnvioCorreo.SMTP_AUTH_USER));
//            message.setRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
//
//            if (copiaCorreo != null && !copiaCorreo.trim().equals("")) {
//                String[] recipientList = copiaCorreo.split(",");
//                InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
//                int counter = 0;
//                for (String recipient2 : recipientList) {
//                    recipientAddress[counter] = new InternetAddress(recipient2.trim());
//                    counter++;
//                }
//                message.setRecipients(Message.RecipientType.CC, recipientAddress);
//            }
//
//            if (copiaOcultaCorreo != null && !copiaOcultaCorreo.trim().equals("")) {
//                String[] recipientListOculta = copiaOcultaCorreo.split(",");
//                InternetAddress[] recipientAddressOculta = new InternetAddress[recipientListOculta.length];
//                int counter2 = 0;
//                for (String recipient2 : recipientListOculta) {
//                    recipientAddressOculta[counter2] = new InternetAddress(recipient2.trim());
//                    counter2++;
//                }
//                message.setRecipients(Message.RecipientType.BCC, recipientAddressOculta);
//            }
//            message.setSubject(asunto);
//            message.setSentDate(new Date());
//            long t6 = Calendar.getInstance().getTimeInMillis();
//            MimeBodyPart messagePart = new MimeBodyPart();
//            messagePart.setContent(mensaje, "text/html");
//            long t7 = Calendar.getInstance().getTimeInMillis();
//            Multipart multipart = new MimeMultipart();
//            multipart.addBodyPart(messagePart);
//            long t8 = Calendar.getInstance().getTimeInMillis();
//            message.setContent(multipart);
//            
//            
//            /*pruebas*/
////            InternetAddress[] cc = new InternetAddress[2];
////            cc[0]=new InternetAddress("irvc22gj@hotmail.com");
////            cc[1]=new InternetAddress("irvc22gj@hotmail.com");
////            session.getTransport().sendMessage(message, cc);
//
//            long t9 = Calendar.getInstance().getTimeInMillis();
//
//            Transport.send(message);
//            long t10 = Calendar.getInstance().getTimeInMillis();
//            System.out.println("Correo Electrónico enviado correctamente ha: " + destinatario);
//
//            System.out.println("Propiedades: " + (t2 - t1) + "ms");
//            System.out.println("Session: " + (t3 - t2) + "ms");
//            System.out.println("Mensaje: " + (t6 - t3) + "ms");
////            System.out.println("Cuerpo Html: " + (t5 - t4) + "ms");
////            System.out.println("Obtiene Xml: " + (t6 - t5) + "ms");
//            System.out.println("Obtiene Pdf: " + (t7 - t6) + "ms");
//            System.out.println("Une las partes: " + (t8 - t7) + "ms");
//            System.out.println("Agrega al mensaje: " + (t9 - t8) + "ms");
//            System.out.println("Envia mensaje: " + (t10 - t9) + "ms");
//            System.out.println("Total: " + (t10 - t1) + "ms");
//
//            return true;
//        } catch (MessagingException ex) {
//            System.out.println("Envío de Correo Electrónico: " + ex);
//            return false;
//        }
//    }

    private static class SMTPAuthenticator extends Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
            String username = EnvioCorreo.SMTP_AUTH_USER;
            String password = EnvioCorreo.SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }

}
