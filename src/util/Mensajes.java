package util;

public class Mensajes {

    public static final String MENSAJE_CORRECTO = "OK";
    public static final String ERROR_CONVERSION_OBJETO = "Error de conversión de objetos";
    
    public static final String INFO_REGISTRO_AGREGADO = "El registro se agregó";
    public static final String INFO_REGISTRO_GUARDADO = "El registro se guardó";
    public static final String INFO_REGISTRO_ELIMINADO = "El registro se eliminó";
    public static final String INFO_REGISTRO_RECUPERADO = "El registro se recuperó";
    public static final String INFO_REGISTRO_ENVIADO = "El registro se envió";
    public static final String ERROR_REGISTRO_GUARDADO = "El registro no se guardó: ";
    public static final String ERROR_REGISTRO_ELIMINADO = "El registro no se eliminó: ";
    public static final String ERROR_REGISTRO_RECUPERADO = "El registro no se recuperó: ";
    public static final String ERROR_REGISTRO_ENVIADO = "El registro no se envió";
    public static final String ERROR_REGISTRO_AGREGADO = "El registro no se agregó: ";
    
    public static final String ERROR_REGISTRO_BUSQUEDA = "Registro no encontrado";
    
    public static final String ERROR_EJECUCION_REPORTE = "Error en la ejecución del reporte: ";
    
    public static final String INFO_REGISTRO_DOCUMENTO_FIRMADO= "El documento se firmó correctamente";
    public static final String ERROR_REGISTRO_DOCUMENTO_FIRMADO= "El documento no se firmó";
    
    public static final String INFO_CORREO_ELECTRONICO_ENVIADO = "Correo Electrónico enviado correctamente";
    public static final String ERROR_CORREO_ELECTRONICO_ENVIADO = "No se pudo enviar el correo electrónico: ";
    
    public static final String ERROR_NO_EXISTE_CANTON = "No existe el cantón de la parroquia";
    public static final String ERROR_NO_EXISTE_PROVINCIA = "No existe la provincia del cantón";
    public static final String ERROR_NO_EXISTE_PARROQUIA = "No existe la parroquia";
    public static final String ERROR_NO_EXISTE_UBICACION = "No existe la ubicación seleccionada";
    
    public static final String ERROR_SIN_MENSAJE_DESDE_DAO = "Error sin mensaje desde Dao";
    public static final String ERROR_SIN_MENSAJE_DESDE_SERVICE = "Error sin mensaje desde Service";
    
}
