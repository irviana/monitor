package jdbc;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import util.RespuestaFuncion;

public class PruebaConexion {

    public PruebaConexion() {

    }

    public RespuestaFuncion getConexionBaseDatos() {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        try {
            respuesta = conexion.conectarBaseDatos();
            if (respuesta.isEjecuto()) {
                String msgConexionExitosa = respuesta.getMensaje();
                respuesta = conexion.desconectar();
                respuesta.setMensaje(msgConexionExitosa + ". " + respuesta.getMensaje());
            }
        } catch (Exception ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }
        return respuesta;
    }

    public RespuestaFuncion getConexionBaseObjetos() {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        try {
            respuesta = conexion.conectarBaseObjetos();
            if (respuesta.isEjecuto()) {
                String msgConexionExitosa = respuesta.getMensaje();
                respuesta = conexion.desconectar();
                respuesta.setMensaje(msgConexionExitosa + ". " + respuesta.getMensaje());
            }
        } catch (Exception ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }
        return respuesta;
    }
}
