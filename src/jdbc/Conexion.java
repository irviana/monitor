package jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import jdbc.util.Constantes;
import util.RespuestaFuncion;

public class Conexion {

    public Connection cx;
    public PreparedStatement ps;
    public CallableStatement cs;
    public ResultSet rs;

    public Conexion() {
        this.cx = null;
        this.ps = null;
        this.rs = null;
    }

    private RespuestaFuncion conectar(ParametrosJdbc pjdbc) {
        RespuestaFuncion respuesta = new RespuestaFuncion();
        try {
            Class.forName(pjdbc.getDriver()).newInstance();
            cx = DriverManager.getConnection(pjdbc.getUrl(), pjdbc.getUsuario(), pjdbc.getClave());
            respuesta.setEjecuto(true);
            respuesta.setMensaje("Conexión Exitosa!");
        } catch (SQLException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje("SQLException: " + e.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, e.getMessage());
        } catch (ClassNotFoundException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje("ClassNotFoundException: " + e.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, e.getMessage());
        } catch (IllegalAccessException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje("IllegalAccessException: " + e.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, e.getMessage());
        } catch (InstantiationException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje("InstantiationException: " + e.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, e.getMessage());
        }
        return respuesta;
    }

    public RespuestaFuncion desconectar() {
        RespuestaFuncion respuesta = new RespuestaFuncion();
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (ps != null) {
                ps.close();
                ps = null;
            }
            if (cs != null) {
                cs.close();
                cs = null;
            }
            if (cx != null) {
                cx.close();
                cx = null;
            }
            respuesta.setEjecuto(true);
            respuesta.setMensaje("Desconexión Correcta!");
        } catch (SQLException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje("SQLException: " + e.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, e.getMessage());
        }
        return respuesta;
    }

    public RespuestaFuncion conectarBaseDatos() {
        ParametrosJdbcDao pjdbcDao = new ParametrosJdbcDao(Constantes.BBDD_DATOS);
        ParametrosJdbc pjdbc = (ParametrosJdbc) pjdbcDao.getParametrosJdbc().getObjeto();
        return conectar(pjdbc);
    }

    public RespuestaFuncion conectarBaseObjetos() {
        ParametrosJdbcDao pjdbcDao = new ParametrosJdbcDao(Constantes.BBDD_OBJETOS);
        ParametrosJdbc pjdbc = (ParametrosJdbc) pjdbcDao.getParametrosJdbc().getObjeto();
        return conectar(pjdbc);
    }
}
