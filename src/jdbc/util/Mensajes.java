package jdbc.util;

public class Mensajes {

    public static final String TITLE_BBDD_DATOS = "BASE DE DATOS";
    public static final String TITLE_BBDD_OBJETOS = "BASE DE OBJETOS";

    public static final String getMensajeAyuda() {
        StringBuilder sb = new StringBuilder();
        sb.append("# DRIVERS \n");
        sb.append("POSTGRESQL: org.postgresql.Driver \n");
        sb.append("ORACLE    : oracle.jdbc.driver.OracleDriver \n");
        sb.append("INFORMIX  : com.informix.jdbc.IfxDriver \n\n");

        sb.append("# URLS \n");
        sb.append("POSTGRESQL: jdbc:postgresql://[host]:[puerto]/[base] \n");
        sb.append("ORACLE    : jdbc:oracle:thin:@[host]:[puerto]:[base] \n");
        sb.append("INFORMIX  : jdbc:informix-sqli://[host]:[puerto]/[base]:INFORMIXSERVER=[ids] \n");
        return sb.toString();
    }
    
}
