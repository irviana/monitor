package jdbc.util;

public class Constantes {


    public static final String ARCHIVO_JDBC_BBDD_DATOS = "JdbcDatos.txt";
    public static final String ARCHIVO_JDBC_BBDD_OBJETOS = "JdbcObjetos.txt";
    public static final String CARACTER_FIN_LINEA = "\n";

    public static final String BBDD_DATOS = "BBDD_DATOS";
    public static final String BBDD_OBJETOS = "BBDD_OBJETOS";

    public static final String CADENA_OK = "OK";
    
    public static final String ARCHIVO_TIEMPOS = "tiemposMonitorFacturas.txt";

}
