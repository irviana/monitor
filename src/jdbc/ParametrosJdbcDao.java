package jdbc;

import java.io.File;
import jdbc.util.Constantes;
import util.RespuestaFuncion;

public class ParametrosJdbcDao {

    private final SimpleSerializable ss;

    public ParametrosJdbcDao(String tipoBase) {

        File nuevo;
        if (tipoBase.equals(Constantes.BBDD_DATOS)) {
            nuevo = new File(Constantes.ARCHIVO_JDBC_BBDD_DATOS);
        } else {
            nuevo = new File(Constantes.ARCHIVO_JDBC_BBDD_OBJETOS);
        } 
        ss = new SimpleSerializable(nuevo);

        if (!nuevo.exists()) {
            ss.serializar(new ParametrosJdbc());
        }
    }

    public RespuestaFuncion setParametrosJdbc(ParametrosJdbc parametrosJdbc) {
        return ss.serializar(parametrosJdbc);
    }

    public RespuestaFuncion getParametrosJdbc() {
        return ss.desSerializar();
    }

}
