package jdbc;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import util.RespuestaFuncion;

public class SimpleSerializable {

    private final File archivo; 
    private FileOutputStream flujoSalida;
    private ObjectOutputStream serializacion;
    private FileInputStream flujoEntrada;
    private ObjectInputStream desSerializacion;

    public SimpleSerializable(File archivo) {
        this.archivo = archivo;
    }

    public RespuestaFuncion serializar(Object obj) {
        RespuestaFuncion respuesta = new RespuestaFuncion();
        try {
            flujoSalida = new FileOutputStream(archivo);
            serializacion = new ObjectOutputStream(flujoSalida);
            serializacion.writeObject(obj);
            serializacion.close();
            flujoSalida.close();
            respuesta.setEjecuto(true);
        } catch (IOException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje(e.getMessage());
        }
        return respuesta;
    }

    public RespuestaFuncion desSerializar() {
        RespuestaFuncion respuesta = new RespuestaFuncion();
        try {
            flujoEntrada = new FileInputStream(archivo);
            desSerializacion = new ObjectInputStream(flujoEntrada);
            respuesta.setEjecuto(true);
            respuesta.setObjeto(desSerializacion.readObject());
            desSerializacion.close();
            flujoEntrada.close();
        } catch (IOException | ClassNotFoundException e) {
            respuesta.setEjecuto(false);
            respuesta.setMensaje(e.getMessage());
        }
        return respuesta;
    }

}
