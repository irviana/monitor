package aplicacion;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Application1 {

    boolean packFrame = true;

    //Construir la aplicacin
    public Application1() {
        Principal frame = new Principal();

        //Validar marcos que tienen tamaos preestablecidos
        //Empaquetar marcos que cuentan con informacin de tamao preferente til. Ej. de su diseo.
        if (packFrame) {
            frame.pack();
        } else {
            frame.validate();
        }
        /*
         //Centrar la ventana
         Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
         screenSize.setSize(screenSize.getWidth(), screenSize.getHeight() - 30);
         //frame.setSize(screenSize);//para maximisar quitar si no es necesario

         Dimension frameSize = frame.getSize();
         if (frameSize.height > screenSize.height) {
         frameSize.height = screenSize.height;
         }
         if (frameSize.width > screenSize.width) {
         frameSize.width = screenSize.width;
         }
         frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
         */
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        //BasicConfigurator.configure();
        try {

            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new Application1();
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }
}
