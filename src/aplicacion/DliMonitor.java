package aplicacion;

import coma.man.Comprobante;
import coma.man.ImpuestosRetencion;
import coma.service.CorreoElectronicoService;
import coma.modelo.PlantillaCorreoElectronico;
import coma.modelo.ServidorCorreoElectronico;
import coma.service.GenerarFirmarXMLService;
import coma.service.MonitorService;
import coma.util.ConstantesCompras;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import sri.modelo.factura.FacturaXML;
import sri.modelo.notacredito.NotaCredito;
import sri.modelo.notadebito.NotaDebito;
import sri.modelo.retencion.ComprobanteRetencionXML;
import sri.modelo.retencion.Impuesto;
import sri.util.FormGenerales;
import sri.util.TipoComprobanteEnum;
import util.Constantes;
import util.ParametrosTiempo;
import util.ParametrosTiempoDao;
import util.RespuestaFuncion;

public class DliMonitor extends javax.swing.JInternalFrame {

//    private ModeloDatosTablaComprobantes modeloTablaComprobantes;
    private MonitorService monitorService;
    private CorreoElectronicoService emailService;
    private GenerarFirmarXMLService generarFirmaXMLService;
    private ServidorCorreoElectronico servidorCorreoElectronico;
    private PlantillaCorreoElectronico plantillaCorreoElectronico;

    private List<Comprobante> listaComprobantes2;
    List<FacturaXML> listaFacturaXML2;

    int miliseg;
    int seg;
    int min;
    int hora;
    int min2;
    boolean estado;
    int minutosCarga;
    int minutosEnvioSRI;
    int numeroComprobantes;
    int banControlCarga;
    int cicloEnvioSri;
    int cicloCarga;

    String path;

    private boolean firmados;

    public DliMonitor() {
        initComponents();
        inicializa();
    }

    private void inicializa() {
        monitorService = new MonitorService();
        emailService = new CorreoElectronicoService();
        generarFirmaXMLService = new GenerarFirmarXMLService();

        servidorCorreoElectronico = new ServidorCorreoElectronico();
        plantillaCorreoElectronico = new PlantillaCorreoElectronico();

        getCorreoElectronicoPlantilla();
        getCorreoElectronicoServidor();

        listaFacturaXML2 = new ArrayList<>();
        listaComprobantes2 = new ArrayList<>();

        path = System.getProperty("user.dir");

        inicializaVariablesTiempo();
        firmados = false;
        btnCargar.setVisible(false);
        btnFirmar.setVisible(false);
        btnEnviar.setVisible(false);
        btnIniciarTiempos.setVisible(true);
        btnParar.setVisible(true);
        btnMan.setVisible(false);
        btnAuto.setVisible(true);
        btnParar.setEnabled(false);
        Logger.getLogger(DliMonitor.class.getName()).log(Level.INFO, "Iniciando monitor", null);
    }

    public void inicializaVariablesTiempo() {
        ParametrosTiempoDao ptDao = new ParametrosTiempoDao();
        ParametrosTiempo pt = (ParametrosTiempo) ptDao.getParametrosTiempo().getObjeto();

        miliseg = 0;
        seg = 0;
        min = 0;
        hora = 0;
        min2 = 0;
        estado = true;
        minutosCarga = pt.getTiempoCargaDocumentos();
        numeroComprobantes = pt.getNumComprobantes();
        minutosEnvioSRI = pt.getTiempoEnvioSri();
        banControlCarga = 0;
        cicloEnvioSri = 0;
        cicloCarga = 0;
        jLMinCarga.setText(minutosCarga + " min.:");
        jLMinEnvio.setText(minutosEnvioSRI + " min.:");
    }

    public void iniciarTiempos() {
        estado = true;

        Thread hilo = new Thread() {
            public void run() {
                String ceroSeg = "0";
                String ceroMin = "0";
                String ceroHora = "0";
                for (;;) {
                    if (estado == true) {
                        try {
                            sleep(1000);
                            seg++;
                            if (seg >= 10) {
                                ceroSeg = "";
                            } else {
                                ceroSeg = "0";
                            }

                            if (seg >= 60) {
                                ceroSeg = "0";
                                seg = 0;
                                if (banControlCarga != 0) {
                                    min++;
                                    cicloEnvioSri++;
                                } else {
                                    min2++;
                                }
                                cicloCarga++;
                                if (min >= 10) {
                                    ceroMin = "";
                                } else {
                                    ceroMin = "0";
                                }
                            }
                            if (min >= 60) {
                                miliseg = 0;
                                seg = 0;
                                min = 0;
                                hora++;
                                if (hora >= 10) {
                                    ceroHora = "";
                                } else {
                                    ceroHora = "0";
                                }
                            }
                            if (cicloCarga == minutosCarga && banControlCarga == 0) {
//                                System.out.println("hacia  getComrpobanteObjeto");
                                min2 = 0;
                                RespuestaFuncion r = monitorService.getComprobateObjeto(numeroComprobantes);
                                listaComprobantes2 = (List<Comprobante>) r.getObjeto();
                                if (!listaComprobantes2.isEmpty()) {
                                    cargarListaComprobantesAtabla();
                                    firmarComprobantes();
                                    //logs();
                                    banControlCarga = 1;
                                }
                                cicloCarga = 0;
                            }
                            if (cicloEnvioSri == minutosEnvioSRI) {
//                                System.out.println("cicloEnvioSri: "+cicloEnvioSri+"....minutosEnvioSRI: "+minutosEnvioSRI);
                                boolean envioSri = enviarComprobantesSri();
                                cicloEnvioSri = 0;
                                cicloCarga = 0;
                                if (envioSri) {
                                    //logs();
                                    resetearTiempos(true);
                                } else {
                                    min = 0;
                                }
                            }
//                            System.out.println("cicloCarga: "+cicloCarga);
//                            System.out.println("cicloEnvioSri: "+cicloEnvioSri+"  .....Bandera: "+banControlCarga+"......minutosEnvioSRI: "+minutosEnvioSRI+"...minutosCarga: "+minutosCarga);
                            if (banControlCarga != 0) {
                                jLTiempoEnvio.setText(ceroHora + hora + " : " + ceroMin + min + " : " + ceroSeg + seg);
                            } else {
                                jLTiempoCarga.setText(ceroHora + hora + " : " + ceroMin + min2 + " : " + ceroSeg + seg);
                            }

                        } catch (Exception ex) {
                            inicializaVariablesTiempo();
//                            System.out.println("EX cicloCarga: "+cicloCarga);
//                            System.out.println("EX cicloEnvioSri: "+cicloEnvioSri+"  .....Bandera: "+banControlCarga+"......minutosEnvioSRI: "+minutosEnvioSRI+"...minutosCarga: "+minutosCarga);
//                            System.out.println("en Exception iniciarCronometro");
                            System.out.println("error: " + ex.getMessage());
                            Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
                        }
                    } else {
                        resetearTiempos(false);
                        break;
                    }
                }
            }
        };
        hilo.start();
    }

    public void resetearTiempos(boolean estadoTiempos) {
        miliseg = 0;
        seg = 0;
        min = 0;
        min2 = 0;
        hora = 0;
        estado = estadoTiempos;
        banControlCarga = 0;
        jLTiempoEnvio.setText("00" + " : " + "00" + " : " + "00");
        jLTiempoCarga.setText("00" + " : " + "00" + " : " + "00");
    }

    private void getCorreoElectronicoPlantilla() {
        RespuestaFuncion r = emailService.getCorreoElectronicoPlantilla();
        List<PlantillaCorreoElectronico> plantillas = (List<PlantillaCorreoElectronico>) r.getObjeto();
        plantillaCorreoElectronico = plantillas.get(0);
    }

    private void getCorreoElectronicoServidor() {
        RespuestaFuncion r = emailService.getCorreoElectronicoServidor();
        List<ServidorCorreoElectronico> servidor = (List<ServidorCorreoElectronico>) r.getObjeto();
        servidorCorreoElectronico = servidor.get(0);
    }

    private boolean enviarSRI(String path, List<Comprobante> listaComprobantesEnviar) throws SQLException {
        boolean respuestaConeccionWS = FormGenerales.existConnection(Constantes.AMBIENTE_PRUEBAS, ConstantesCompras.RECEPCION_COMPROBANTES);
//        boolean respuestaConeccionWS = FormGenerales.existConnection(Constantes.AMBIENTE_PRODUCCION, ConstantesCompras.RECEPCION_COMPROBANTES);
        if (respuestaConeccionWS) {
            RespuestaFuncion r = monitorService.enviarAutorizar(path, listaComprobantesEnviar,
                     servidorCorreoElectronico, plantillaCorreoElectronico);
        }
        return respuestaConeccionWS;
    }

    public boolean enviarComprobantesSri() {
        boolean respuestaSri = false;
        if (!listaComprobantes2.isEmpty()) {
            try {
                respuestaSri = enviarSRI(path, listaComprobantes2);
            } catch (Exception ex) {
                Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
            }
        } else {
            System.out.println("No existen comprobantes para enviar al SRI");
            Logger.getLogger(DliMonitor.class.getName()).log(Level.INFO, "No existen comprobantes para enviar al SRI",
                     null);
        }
        logs();
        return respuestaSri;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        lblMensaje = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnEnviar = new javax.swing.JButton();
        btnFirmar = new javax.swing.JButton();
        btnIniciarTiempos = new javax.swing.JButton();
        jLTiempoCarga = new javax.swing.JLabel();
        jLTiempoEnvio = new javax.swing.JLabel();
        btnParar = new javax.swing.JButton();
        jLCarga = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLMinCarga = new javax.swing.JLabel();
        jLMinEnvio = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTALogs = new javax.swing.JTextArea();
        btnAuto = new javax.swing.JButton();
        btnMan = new javax.swing.JButton();

        setClosable(true);
        setResizable(true);
        setTitle("Parametros JDBC");

        lblTitulo.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(0, 102, 204));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplicacion/imagenes/network-server-database.png"))); // NOI18N
        lblTitulo.setText("MONITOR FACTURACIÓN");

        lblMensaje.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblMensaje.setForeground(new java.awt.Color(0, 102, 205));
        lblMensaje.setText("OK");
        lblMensaje.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnCargar.setText("Cargar");
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnEnviar.setText("Enviar");
        btnEnviar.setPreferredSize(new java.awt.Dimension(82, 25));
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        btnFirmar.setText("Firmar");
        btnFirmar.setPreferredSize(new java.awt.Dimension(82, 25));
        btnFirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirmarActionPerformed(evt);
            }
        });

        btnIniciarTiempos.setText("Iniciar");
        btnIniciarTiempos.setPreferredSize(new java.awt.Dimension(82, 25));
        btnIniciarTiempos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarTiemposActionPerformed(evt);
            }
        });

        jLTiempoCarga.setText("00 : 00 : 00");

        jLTiempoEnvio.setText("00 : 00 : 00");

        btnParar.setText("Parar");
        btnParar.setPreferredSize(new java.awt.Dimension(82, 25));
        btnParar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPararActionPerformed(evt);
            }
        });

        jLCarga.setText("Carga iniciará en");

        jLabel1.setText("Envio al SRI iniciara en");

        jLMinCarga.setText("..");

        jLMinEnvio.setText("..");

        jTALogs.setEditable(false);
        jTALogs.setColumns(20);
        jTALogs.setRows(5);
        jScrollPane2.setViewportView(jTALogs);

        btnAuto.setText("Automatico");
        btnAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAutoActionPerformed(evt);
            }
        });

        btnMan.setText("Manual");
        btnMan.setPreferredSize(new java.awt.Dimension(114, 25));
        btnMan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
                    .addComponent(lblMensaje, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLMinEnvio)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLTiempoEnvio))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLCarga)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLMinCarga)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLTiempoCarga))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(btnAuto)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnMan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnCargar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnFirmar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnIniciarTiempos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnParar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnSalir))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 899, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir)
                    .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFirmar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIniciarTiempos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnParar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAuto)
                    .addComponent(btnMan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCargar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLTiempoCarga)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLCarga)
                        .addComponent(jLMinCarga)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLTiempoEnvio)
                    .addComponent(jLMinEnvio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        if (!listaComprobantes2.isEmpty() && firmados) {
            enviarComprobantesSri();
        } else {
            Logger.getLogger(DliMonitor.class.getName()).log(Level.INFO, "No existe comprobantes para enviar al SRI", null);
        }
    }//GEN-LAST:event_btnEnviarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        estado = false;
        this.setVisible(false);
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        cargarListaComprobantesAtabla();
    }//GEN-LAST:event_btnCargarActionPerformed

    public void logs() {
        jTALogs.setText("");
        try {
            File file = new File(path + "/" + "monitorFacturas.log");
            BufferedReader leer = new BufferedReader(new FileReader(file));
            String linea = leer.readLine();
            while (linea != null) {
                jTALogs.append(linea + "\n");
                linea = leer.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, null, ex);
        }
    }

    public void cargarListaComprobantesAtabla() {
        try {
            firmados = false;
            RespuestaFuncion r = monitorService.getComprobateObjeto(numeroComprobantes);
            listaComprobantes2 = (List<Comprobante>) r.getObjeto();
        } catch (Exception ex) {
            Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
        logs();
    }


    private void btnFirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirmarActionPerformed
        // TODO add your handling code here:
        firmarComprobantes();
    }//GEN-LAST:event_btnFirmarActionPerformed

    private void btnIniciarTiemposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarTiemposActionPerformed
        estado = true;
        iniciarTiempos();
        btnIniciarTiempos.setEnabled(false);
        btnAuto.setEnabled(false);
        btnSalir.setEnabled(false);
        btnParar.setEnabled(true);
    }//GEN-LAST:event_btnIniciarTiemposActionPerformed

    private void btnPararActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPararActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(null, "Seguro desea parar el monitor?",
                "Para monitor", JOptionPane.YES_NO_OPTION);
        if (respuesta == 0) {
            estado = false;
            btnIniciarTiempos.setEnabled(true);
            btnAuto.setEnabled(true);
            btnSalir.setEnabled(true);
            btnParar.setEnabled(false);
//            resetearTiempos(false);
        }
    }//GEN-LAST:event_btnPararActionPerformed

    private void btnAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAutoActionPerformed
        // TODO add your handling code here:
        btnCargar.setVisible(true);
        btnFirmar.setVisible(true);
        btnEnviar.setVisible(true);
        btnIniciarTiempos.setVisible(false);
        btnParar.setVisible(false);
        btnMan.setVisible(true);
        btnAuto.setVisible(false);
        estado = false;
        resetearTiempos(false);
        btnIniciarTiempos.setEnabled(true);
    }//GEN-LAST:event_btnAutoActionPerformed

    private void btnManActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManActionPerformed
        // TODO add your handling code here:
        btnCargar.setVisible(false);
        btnFirmar.setVisible(false);
        btnEnviar.setVisible(false);
        btnIniciarTiempos.setVisible(true);
        btnParar.setVisible(true);
        btnAuto.setVisible(true);
        btnMan.setVisible(false);

    }//GEN-LAST:event_btnManActionPerformed

    public void firmarComprobantes() {
        try {
            if (!listaComprobantes2.isEmpty()) {
                firmados = true;
                for (Comprobante c : listaComprobantes2) {
                    if (c.getCodigoDocumento().compareTo(TipoComprobanteEnum.FACTURA.getCode()) == 0) {
                        RespuestaFuncion resp = monitorService.generarComprobanteFacturaVenta(c.getId());
                        List<FacturaXML> listaFacturaXML = (List<FacturaXML>) resp.getObjeto();

                        if (!listaFacturaXML.isEmpty()) {
                            FacturaXML factura = listaFacturaXML.get(0);
                            String nombreArchivo = factura.getInfoTributaria().getClaveAcceso() + Constantes.EXTENSION_XML;
                            RespuestaFuncion r = generarFirmaXMLService.generarFirmaXML(factura, path, nombreArchivo);
                            if (r.isEjecuto()) {
                                c.setNombre_xml(factura.getInfoTributaria().getClaveAcceso());
                                byte[] xml = (byte[]) r.getObjeto();
                                c.setXml(xml);
                                c.setCodigoDocumento(factura.getInfoTributaria().getCodDoc());
                            }
                        }
                    }

                    if (c.getCodigoDocumento().compareTo(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode()) == 0) {
                        if (c.getIdInversion() == null) {
                            RespuestaFuncion resp = monitorService.generarComprobanteRetencion(c.getId());
                            List<ComprobanteRetencionXML> listaRetencionXML = (List<ComprobanteRetencionXML>) resp.getObjeto();

                            if (!listaRetencionXML.isEmpty()) {
                                ComprobanteRetencionXML retencion = listaRetencionXML.get(0);
                                String nombreArchivo = retencion.getInfoTributaria().getClaveAcceso() + Constantes.EXTENSION_XML;
                                RespuestaFuncion r = generarFirmaXMLService.generarFirmaXML(retencion, path, nombreArchivo);
                                if (r.isEjecuto()) {
                                    c.setNombre_xml(retencion.getInfoTributaria().getClaveAcceso());
                                    byte[] xml = (byte[]) r.getObjeto();
                                    c.setXml(xml);
                                    c.setCodigoDocumento(retencion.getInfoTributaria().getCodDoc());
                                }
                            }
                        } else {
                            RespuestaFuncion resp = monitorService.generarComprobanteRetencionInversiones(c.getId());
                            List<ComprobanteRetencionXML> listaRetencionXML = (List<ComprobanteRetencionXML>) resp.getObjeto();

                            if (!listaRetencionXML.isEmpty()) {
                                ComprobanteRetencionXML retencion = listaRetencionXML.get(0);
                                List<ImpuestosRetencion> listaImpuestoRetencion = new ArrayList<>();
                                List<Impuesto> impuestos = retencion.getImpuestos().getImpuesto();

                                for (Impuesto imp : impuestos) {
                                    ImpuestosRetencion impuestoRetencion = new ImpuestosRetencion();
                                    impuestoRetencion.setRetencioId(c.getId());
                                    RespuestaFuncion res = monitorService.getRetencionNombreImpuestoInversion(imp.getCodigoRetencion(),
                                            c.getEjercicio());

                                    List<ImpuestosRetencion> impRet = (List<ImpuestosRetencion>) res.getObjeto();

                                    impuestoRetencion.setNombreImpuesto(impRet.get(0).getNombreImpuesto());
                                    impuestoRetencion.setBase(imp.getBaseImponible());
                                    impuestoRetencion.setImpuesto(imp.getValorRetenido());
                                    listaImpuestoRetencion.add(impuestoRetencion);
                                }

                                String nombreArchivo = retencion.getInfoTributaria().getClaveAcceso() + Constantes.EXTENSION_XML;
                                RespuestaFuncion r = generarFirmaXMLService.generarFirmaXML(retencion, path, nombreArchivo);
                                if (r.isEjecuto()) {
                                    c.setNombre_xml(retencion.getInfoTributaria().getClaveAcceso());
                                    byte[] xml = (byte[]) r.getObjeto();
                                    c.setXml(xml);
                                    c.setCodigoDocumento(retencion.getInfoTributaria().getCodDoc());
                                    c.setListaImpuestoRetencion(listaImpuestoRetencion);
                                }
                            }
                        }
                    }

                    if (c.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode()) == 0) {
                        RespuestaFuncion resp = monitorService.generarComprobanteNotaCredito(c.getId());
                        List<NotaCredito> listaNotaCreditoXML = (List<NotaCredito>) resp.getObjeto();

                        NotaCredito notaCredito = listaNotaCreditoXML.get(0);

                        if (!listaNotaCreditoXML.isEmpty()) {
                            String nombreArchivo = notaCredito.getInfoTributaria().getClaveAcceso() + Constantes.EXTENSION_XML;
                            RespuestaFuncion r = generarFirmaXMLService.generarFirmaXML(notaCredito, path, nombreArchivo);
                            if (r.isEjecuto()) {
                                c.setNombre_xml(notaCredito.getInfoTributaria().getClaveAcceso());
                                byte[] xml = (byte[]) r.getObjeto();
                                c.setXml(xml);
                                c.setCodigoDocumento(notaCredito.getInfoTributaria().getCodDoc());
                            }
                        }
                    }

                    if (c.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_DEBITO.getCode()) == 0) {
                        RespuestaFuncion resp = monitorService.generarComprobanteNotaDebito(c.getId());
                        List<NotaDebito> listaNotaDebitoXML = (List<NotaDebito>) resp.getObjeto();

                        NotaDebito notaDebito = listaNotaDebitoXML.get(0);

                        if (!listaNotaDebitoXML.isEmpty()) {
                            String nombreArchivo = notaDebito.getInfoTributaria().getClaveAcceso() + Constantes.EXTENSION_XML;
                            RespuestaFuncion r = generarFirmaXMLService.generarFirmaXML(notaDebito, path, nombreArchivo);
                            if (r.isEjecuto()) {
                                c.setNombre_xml(notaDebito.getInfoTributaria().getClaveAcceso());
                                byte[] xml = (byte[]) r.getObjeto();
                                c.setXml(xml);
                                c.setCodigoDocumento(notaDebito.getInfoTributaria().getCodDoc());
                            }
                        }
                    }
                }
            } else {
                Logger.getLogger(DliMonitor.class.getName()).log(Level.INFO, "No existen comprobantes para firmar", null);
            }
        } catch (Exception ex) {
            Logger.getLogger(DliMonitor.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
        logs();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAuto;
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnFirmar;
    private javax.swing.JButton btnIniciarTiempos;
    private javax.swing.JButton btnMan;
    private javax.swing.JButton btnParar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLCarga;
    private javax.swing.JLabel jLMinCarga;
    private javax.swing.JLabel jLMinEnvio;
    private javax.swing.JLabel jLTiempoCarga;
    private javax.swing.JLabel jLTiempoEnvio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTALogs;
    private javax.swing.JLabel lblMensaje;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables
}
