package aplicacion;

import java.awt.Toolkit;
import java.io.File;
import jdbc.util.Constantes;

public class Principal extends javax.swing.JFrame {

    public Principal() {
        initComponents();
        ((DesktopConFondo) pnlLogo).setImagen(new File("logo.jpg"));
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlLogo = new DesktopConFondo();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlEscritorio = new javax.swing.JDesktopPane();
        btnMonitorFacturacion = new javax.swing.JButton();
        barraMenu = new javax.swing.JMenuBar();
        mnuInicio = new javax.swing.JMenu();
        itemJdbcBaseDatos = new javax.swing.JMenuItem();
        itemJdbcBaseObjetos = new javax.swing.JMenuItem();
        itemTiempos = new javax.swing.JMenuItem();
        separador = new javax.swing.JSeparator();
        itemSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monitor Facturas");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImage(Toolkit.getDefaultToolkit().getImage("gindicadores.gif"));

        jScrollPane1.setAutoscrolls(true);

        pnlEscritorio.setAutoscrolls(true);
        jScrollPane1.setViewportView(pnlEscritorio);

        btnMonitorFacturacion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnMonitorFacturacion.setForeground(new java.awt.Color(0, 102, 204));
        btnMonitorFacturacion.setText("Monitorear Facturas");
        btnMonitorFacturacion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnMonitorFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMonitorFacturacionActionPerformed(evt);
            }
        });

        mnuInicio.setText("Inicio");

        itemJdbcBaseDatos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplicacion/iconos/db.png"))); // NOI18N
        itemJdbcBaseDatos.setText("JDBC Base Datos");
        itemJdbcBaseDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemJdbcBaseDatosActionPerformed(evt);
            }
        });
        mnuInicio.add(itemJdbcBaseDatos);

        itemJdbcBaseObjetos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplicacion/iconos/db.png"))); // NOI18N
        itemJdbcBaseObjetos.setText("JDBC Base Objetos");
        itemJdbcBaseObjetos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemJdbcBaseObjetosActionPerformed(evt);
            }
        });
        mnuInicio.add(itemJdbcBaseObjetos);

        itemTiempos.setText("Tiempos");
        itemTiempos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemTiemposActionPerformed(evt);
            }
        });
        mnuInicio.add(itemTiempos);
        mnuInicio.add(separador);

        itemSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplicacion/iconos/system-shutdown-6.png"))); // NOI18N
        itemSalir.setText("Salir");
        itemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSalirActionPerformed(evt);
            }
        });
        mnuInicio.add(itemSalir);

        barraMenu.add(mnuInicio);

        setJMenuBar(barraMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlLogo)
                    .addComponent(btnMonitorFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 956, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMonitorFacturacion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 337, Short.MAX_VALUE)))
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void btnMonitorFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMonitorFacturacionActionPerformed
        DliMonitor di = new DliMonitor();
        di.setLocation(5, 5);
        di.pack();
        pnlEscritorio.add(di);
        di.setVisible(true);
}//GEN-LAST:event_btnMonitorFacturacionActionPerformed

    private void itemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_itemSalirActionPerformed

    private void itemJdbcBaseDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemJdbcBaseDatosActionPerformed
        DliParametrosJdbc di = new DliParametrosJdbc(Constantes.BBDD_DATOS);
        di.setLocation(5, 5);
        di.pack();
        pnlEscritorio.add(di);
        di.setVisible(true);
    }//GEN-LAST:event_itemJdbcBaseDatosActionPerformed

    private void itemJdbcBaseObjetosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemJdbcBaseObjetosActionPerformed
        DliParametrosJdbc di = new DliParametrosJdbc(Constantes.BBDD_OBJETOS);
        di.setLocation(5, 5);
        di.pack();
        pnlEscritorio.add(di);
        di.setVisible(true);
    }//GEN-LAST:event_itemJdbcBaseObjetosActionPerformed

    private void itemTiemposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemTiemposActionPerformed
        DliParametrosTiempos di = new DliParametrosTiempos();
        di.setLocation(5, 5);
        di.pack();
        pnlEscritorio.add(di);
        di.setVisible(true);
    }//GEN-LAST:event_itemTiemposActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barraMenu;
    private javax.swing.JButton btnMonitorFacturacion;
    private javax.swing.JMenuItem itemJdbcBaseDatos;
    private javax.swing.JMenuItem itemJdbcBaseObjetos;
    private javax.swing.JMenuItem itemSalir;
    private javax.swing.JMenuItem itemTiempos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenu mnuInicio;
    private javax.swing.JDesktopPane pnlEscritorio;
    private javax.swing.JDesktopPane pnlLogo;
    private javax.swing.JSeparator separador;
    // End of variables declaration//GEN-END:variables

}
