package coma.util;



public class ConstantesCompras {

    public static final String PORCENTAJE_IVA = "12%";

    public static final String PATH_REPORTE_COMPRAS = "/reportes/compras/";

    public static final String PLANTILLA_EMAIL_REGISTRO_PROVEEDOR_CEL = "email_registro_proveedor_cel";
    public static final String PLANTILLA_EMAIL_SOLICITUD_COMPRA = "email_solicitud_compra";
    public static final String PLANTILLA_EMAIL_ORDEN_COMPRA = "email_orden_compra";
    public static final String PLANTILLA_EMAIL_COMPROBANTE_ELECTRONICO = "email_comprobante_electronico";

    public static final String REPORTE_SOLICITUD_COMPRA = "/reportes/compras/rptSolicitudCompra.jasper";
    public static final String REPORTE_SOLICITUD_PROVEEDURIA = "/reportes/compras/rptSolicitudProveeduria.jasper";
    public static final String REPORTE_ORDEN_COMPRA = "/reportes/compras/rptOrdenCompra.jasper";
    public static final String REPORTE_COMPROBANTE_RETENCION = "/reportes/compras/rptComprobanteRetencion.jasper";
    public static final String REPORTE_COMPROBANTE_RETENCION_CLIENTE = "/reportes/compras/rptComprobanteRetencionCliente.jasper";
    public static final String REPORTE_COMPROBANTE_RETENCION_CLIENTE_INVERSION = "/reportes/compras/rptComprobanteRetencionClienteInversion.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_COMPRA = "/reportes/compras/rptComprobanteContableCompra.jasper";
    public static final String REPORTE_KARDEX = "/reportes/compras/rptKardex.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_PAGO_COMPRA = "/reportes/compras/rptComprobanteContablePagoCompra.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_PAGO_VENTA = "/reportes/compras/rptComprobanteContablePagoVenta.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_VENTA = "/reportes/compras/rptComprobanteContableVenta.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_NOTA_DEBITO = "/reportes/compras/rptComprobanteContableNotaDebito.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_NOTA_CREDITO = "/reportes/compras/rptComprobanteContableNotaCredito.jasper";
    public static final String REPORTE_COMPROBANTE_CONTABLE_PROVEEDURIA = "/reportes/compras/rptComprobanteContableCompraProveeduria.jasper";
    public static final String REPORTE_ENTREGA_PROVEEDURIA = "/reportes/compras/rptEntregaProveeduria.jasper";

    public static final String REPORTE_CHEQUE_PICHINCHA = "/reportes/compras/rptChequePichincha.jasper";
    public static final String REPORTE_FACTURA_VENTA = "/reportes/compras/rptFactura.jasper";
    public static final String REPORTE_FACTURA_VENTA_CLIENTE = "/reportes/compras/rptFacturaCliente.jasper";
    public static final String REPORTE_NOTA_CREDITO = "/reportes/compras/rptNotaCredito.jasper";
    public static final String REPORTE_NOTA_DEBITO = "/reportes/compras/rptNotaDebito.jasper";
    public static final String REPORTE_FORMULARIO_103 = "/reportes/compras/rptFormulario103.jasper";
    public static final String REPORTE_FORMULARIO_104 = "/reportes/compras/rptFormulario104.jasper";

    public static final String PATH_COMPROBANTES_GENERADOS = "comprobantes/generados";
    public static final String PATH_COMPROBANTES_FIRMADOS = "comprobantes/firmados";
    public static final String PATH_COMPROBANTES_AUTORIZADOS = "comprobantes/autorizados";
    public static final String PATH_COMPROBANTES_NO_AUTORIZADOS = "comprobantes/no_autorizados";
    public static final String PATH_COMPROBANTES_RECHAZADOS = "comprobantes/rechazados";
    public static final String PATH_XSD = "/resources/xsd/";

    /*public static final ComdEstadoDocumento ESTADO_DOC_SOLICITUD_PROFORMA = new ComdEstadoDocumento(1);
    public static final ComdEstadoDocumento ESTADO_DOC_ORDEN_PEDIDO = new ComdEstadoDocumento(2);
    public static final ComdEstadoDocumento ESTADO_DOC_BORRADOR = new ComdEstadoDocumento(3);
    public static final ComdEstadoDocumento ESTADO_DOC_VALIDADO = new ComdEstadoDocumento(4);
    public static final ComdEstadoDocumento ESTADO_DOC_ANULADO = new ComdEstadoDocumento(5);*/

    public static final String DOCUMENTO_RETENCION = "DOCUMENTO DE RETENCIÓN";
    public static final String DOCUMENTO_FACTURA_VENTA = "FACTURA DE VENTA";
    public static final String DOCUMENTO_NOTA_CREDITO = "NOTA DE CREDITO";
    public static final String DOCUMENTO_NOTA_DEBITO = "NOTA DE DEBITO";

    public static final String NOMBRE_SECUENCIA_FACTURA = "seq_factura_";
    public static final String NOMBRE_SECUENCIA_RETENCION = "seq_retencion_";
    public static final String NOMBRE_SECUENCIA_NOTA_DEBITO = "seq_nota_debito_";

    public static final String NOMBRE_TABLA_FACTURAS = "coma_factura_cab";
    public static final String NOMBRE_TABLA_RETENCIONES = "coma_retencion";

    public static final String CODIGO_TIPO_DOC_NOTA_DEBITO = "05";
    public static final String CODIGO_TIPO_DOC_RETENCION = "07";
    public static final String CODIGO_TIPO_DOC_DOCUMENTO_VENTA = "18";

    public static final String TIPO_CATEGORIA_PRODUCTO_VISTA = "vista";
    public static final String TIPO_CATEGORIA_PRODUCTO_NORMAL = "normal";

    public static final String SOLICITUD_COMPRA = "compra";
    public static final String ORDEN_COMPRA = "compra";
    public static final String PROFORMA_VENTA = "venta";
    public static final String PEDIDO_VENTA = "venta";

    public static final String FACTURA_COMPRA = "compra";
    public static final String NOTA_DEBITO = "debito";
    public static final String FACTURA_VENTA = "venta";
    public static final String NOTA_CREDITO = "credito";

    public static final String TIPO_PRODUCTO_ALMACENABLE = "almacenable";
    public static final String TIPO_PRODUCTO_CONSUMIBLE = "consumible";
    public static final String TIPO_PRODUCTO_SERVICIO = "servicio";

    public static final String TIPO_RETENCION_AUTOMATICA = "automatica";
    public static final String TIPO_RETENCION_MANUAL = "manual";

    public static final String TIPO_MOVIMIENTO_INGRESO = "ingreso";
    public static final String TIPO_MOVIMIENTO_EGRESO = "egreso";
    
    public static final String TIPO_ITEM_BIEN = "BIEN";
    public static final String TIPO_ITEM_SERVICIO = "SERVICIO";

    public static final String ESTADO_SOLICITUD_COMPRA = "solicitud_compra";
    public static final String ESTADO_PEDIDO_COMPRA = "pedido_compra";

    public static final String CODIGO_ORDEN_COMPRA = "OC";
    public static final String CODIGO_PEDIDO_VENTA = "PV";
    public static final String CODIGO_FACTURA_COMPRA = "FC";
    public static final String CODIGO_FACTURA_VENTA = "FV";
    public static final String CODIGO_NOTA_DEBITO = "ND";
    public static final String CODIGO_NOTA_CREDITO = "NC";
    public static final String CODIGO_PAGO_FACTURA_COMPRA = "PFC";
    public static final String CODIGO_PAGO_FACTURA_VENTA = "PFV";
    public static final String CODIGO_PROVEEDURIA = "PR";

    /*public static final String CODIGO_DIARIO_GENERAL = "CC";
    public static final String CODIGO_DIARIO_COMPRA = "FC";
    public static final String CODIGO_DIARIO_VENTA = "FV";
    public static final String CODIGO_DIARIO_NOTA_DEBITO = "ND";
    public static final String CODIGO_DIARIO_NOTA_CREDITO = "NC";
    public static final String CODIGO_DIARIO_PROVEEDURIA = "PR";*/

    public static final String CODIGO_GRUPO_IVA = "iva";
    public static final String CODIGO_GRUPO_IVA_CERO = "iva0";
    public static final String CODIGO_GRUPO_NO_IVA = "no_iva";
    public static final String CODIGO_GRUPO_EXENTO_IVA = "exento_iva";
    public static final String CODIGO_GRUPO_RETENCION_IVA_BIENES_10 = "ret_iva_bienes10";
    public static final String CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_20 = "ret_iva_servicios20";
    public static final String CODIGO_GRUPO_RETENCION_IVA_BIENES = "ret_iva_bienes";
    public static final String CODIGO_GRUPO_RETENCION_IVA_SERVICIOS = "ret_iva_servicios";
    public static final String CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_100 = "ret_iva_servicios100";
    public static final String CODIGO_GRUPO_RETENCION_IMPUESTO_RENTA = "ret_ir";
    public static final String CODIGO_GRUPO_NO_RETENCION_IMPUESTO_RENTA = "no_ret_ir";
    public static final String CODIGO_GRUPO_IMPUESTO_ADUANA = "imp_aduana";
    public static final String CODIGO_GRUPO_ICE = "ice";
    public static final String CODIGO_GRUPO_IRBPNR = "irbpnr";
    public static final String CODIGO_GRUPO_IMPUESTO_SALIDA_DIVISAS = "isd";
    public static final String CODIGO_GRUPO_OTRO_IMPUESTO = "otro";

    public static final String TIPO_RETENCION_VENTA = "retencion_venta";
    public static final String TIPO_RETENCION_COMPRA = "retencion_compra";

    public static final String SECUENCIA_NUMERO_PROVEEDURIA = "seq_proveeduria";
    
    public static final String IMAGEN_PROVEEDOR = "ImagenProveedor.png";
    
    // constantes para consumir ws sri
    
    /*public static final String RECEPCION_COMPROBANTES_SERVICE ="RecepcionComprobantesService";
    public static final String RECEPCION_COMPROBANTES="RecepcionComprobantes";
    public static final String AUTORIZACION_COMPROBANTES="AutorizacionComprobantes";
    public static final String AUTORIZACION_COMPROBANTES_SERVICE="AutorizacionComprobantesService";*/
    
    public static final String RECEPCION_COMPROBANTES_SERVICE ="RecepcionComprobantesOfflineService";
    public static final String RECEPCION_COMPROBANTES="RecepcionComprobantesOffline";
    public static final String AUTORIZACION_COMPROBANTES="AutorizacionComprobantesOffline";
    public static final String AUTORIZACION_COMPROBANTES_SERVICE="AutorizacionComprobantesOfflineService";
}
