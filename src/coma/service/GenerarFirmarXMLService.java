/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.service;

import coma.util.ConstantesCompras;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import sri.util.ArchivoUtils;
import sri.util.TipoComprobanteEnum;
import util.AbstractService;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class GenerarFirmarXMLService extends AbstractService {

    

    public GenerarFirmarXMLService() {
        
    }

    public RespuestaFuncion generarFirmaXML(Object comprobante, String path,String nombreArchivo) throws IOException{
        RespuestaFuncion r =new RespuestaFuncion();
        try{
            String archivoACrear = path +"/" +ConstantesCompras.PATH_COMPROBANTES_GENERADOS + File.separator + nombreArchivo;
            String dirFirmados = path + "/" +ConstantesCompras.PATH_COMPROBANTES_FIRMADOS + "/";

            String respuestaCrear = ArchivoUtils.crearArchivoXml2(archivoACrear, comprobante, TipoComprobanteEnum.FACTURA.getCode());

            if (respuestaCrear == null) {            
                ArchivoUtils.firmarArchivo(path, archivoACrear, dirFirmados, nombreArchivo);

                String ubicacionXML = path + "/"+ConstantesCompras.PATH_COMPROBANTES_FIRMADOS
                        + File.separator + nombreArchivo;
                Path ficheroXML = Paths.get(ubicacionXML);

                byte[] xml=Files.readAllBytes(ficheroXML);
                r.setEjecuto(true);
                r.setMensaje("firmado");
                r.setObjeto(xml);
            }else{
                r.setEjecuto(false);
                r.setMensaje("no firmado");
                r.setObjeto(null);
            }
        }catch (Exception ex) {
            Logger.getLogger(GenerarFirmarXMLService.class.getName()).log(Level.FATAL, null, ex.getCause());
        }
        
        return r;
    }
    
}
