/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.service;

import coma.dao.CorreoElectronicoDao;
import util.AbstractService;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class CorreoElectronicoService extends AbstractService{

    private CorreoElectronicoDao emailDao;

    public CorreoElectronicoService() {

    }

    public RespuestaFuncion getCorreoElectronicoPlantilla() {
        emailDao = new CorreoElectronicoDao();
        return validaRespuestaDao(emailDao.obtenerPlantillaCorreoElectronico());
    }

    public RespuestaFuncion getCorreoElectronicoServidor() {
        emailDao = new CorreoElectronicoDao();
        return validaRespuestaDao(emailDao.obtenerServidorCorreoElectronico());
    }
}
