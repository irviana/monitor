/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.service;

import coma.dao.ComprobanteObjetoDao;
import coma.dao.FacturaDao;
import coma.dao.FacturaImpuestoDao;
import coma.dao.LogComprobanteObjetoDao;
import coma.dao.NotaCreditoDao;
import coma.dao.NotaDebitoDao;
import coma.dao.RetencionDao;
import coma.modelo.ComaComprobanteObjeto;
import coma.man.Comprobante;
import coma.man.ImpuestosRetencion;
import coma.modelo.PlantillaCorreoElectronico;
import coma.modelo.ServidorCorreoElectronico;
import coma.util.ConstantesCompras;
import doca.dao.DocaCorreoElectronicoDao;
import doca.modelo.DocaCorreoElectronico;
import ec.gob.sri.comprobantes.ws.RespuestaSolicitud;
import ec.gob.sri.comprobantes.ws.aut.Autorizacion;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import sri.util.ArchivoUtils;
import sri.util.TipoComprobanteEnum;
import util.AbstractService;
import util.Constantes;
import util.EnvioCorreo;
import util.ImpresionDocPdf;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class MonitorService extends AbstractService {

    private final FacturaDao facturaDao;
    private final RetencionDao retencionDao;
    private final ComprobanteObjetoDao objetoDao;
    private final NotaCreditoDao notaCreditoDao;
    private final NotaDebitoDao notaDebitoDao;
    private final LogComprobanteObjetoDao logComprobanteObjetoDao;
    private final FacturaImpuestoDao facturaImpuestoDao;
    private final DocaCorreoElectronicoDao docaCorreoElectronicoDao;

    public MonitorService() {
        facturaDao = new FacturaDao();
        retencionDao = new RetencionDao();
        objetoDao = new ComprobanteObjetoDao();
        notaCreditoDao =  new NotaCreditoDao();
        notaDebitoDao = new NotaDebitoDao();
        logComprobanteObjetoDao = new LogComprobanteObjetoDao();
        facturaImpuestoDao = new FacturaImpuestoDao();
        docaCorreoElectronicoDao = new DocaCorreoElectronicoDao();
    }

    
    public RespuestaFuncion getComprobateObjeto(int numeroComprobantes) {
        RespuestaFuncion r = new RespuestaFuncion();

        List<Comprobante> listaFacturas = new ArrayList<>();
        List<Comprobante> listaRetencionesFacturaCompra = new ArrayList<>();
        List<Comprobante> listaRetencionesInversion = new ArrayList<>();
        List<Comprobante> listaComprobantes = new ArrayList<>();
        try{
            r = validaRespuestaDao(facturaDao.getComprobantesFacturaVenta(numeroComprobantes));
            listaFacturas = (List<Comprobante>) r.getObjeto();
            
            r = validaRespuestaDao(retencionDao.getComprobantesRetencionFacturaCompra(numeroComprobantes));
            listaRetencionesFacturaCompra = (List<Comprobante>) r.getObjeto();
            
            r = validaRespuestaDao(retencionDao.getComprobantesRetencionInversion(numeroComprobantes));
            listaRetencionesInversion = (List<Comprobante>) r.getObjeto();

            listaComprobantes.addAll(listaFacturas);
            listaComprobantes.addAll(listaRetencionesFacturaCompra);
            listaComprobantes.addAll(listaRetencionesInversion);

            if (!listaComprobantes.isEmpty()) {
                for (Comprobante con : listaComprobantes) {
                    r = validaRespuestaDao(objetoDao.getComprobantesObjetos(con.getEntidadId(), con.getId()));

                    List<ComaComprobanteObjeto> compObjetos = (List<ComaComprobanteObjeto>) r.getObjeto();
                    if(!compObjetos.isEmpty()){
                        ComaComprobanteObjeto compObjeto = compObjetos.get(0);

                        con.setXml(compObjeto.getXml());
                        con.setNombre_xml(compObjeto.getNombre());
                    }
                }

            } else {
                System.out.println("No existen comprobantes");
            }
            r = new RespuestaFuncion();
            r.setEjecuto(true);
            r.setObjeto(listaComprobantes);
            r.setMensaje("ok");
            
        }catch (Exception ex) {
            r.setEjecuto(false);
            r.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, "Catch: " + ex.getCause());
        }
        return r;
    }
    
    public RespuestaFuncion enviarAutorizar(String path,
            List<Comprobante> listaComprobantesEnviar, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) throws SQLException {
        Timestamp ts;
        RespuestaFuncion re,resp = new RespuestaFuncion();
        RespuestaSolicitud respuestaRecepcion;
        try {

            for (Comprobante comp : listaComprobantesEnviar) {

                if (!comp.isEnviadoSri()) {
                    if (!comp.isRecibidoSri()) {
                        
                        RespuestaFuncion r=ArchivoUtils.enviarRecibir(path, comp);
                        respuestaRecepcion=(RespuestaSolicitud) r.getObjeto();
                        
                        if (respuestaRecepcion.getEstado().equals("RECIBIDA")) {
                            comp.setRecibidoSri(true);
                            comp.setEnviadoSri(true);
                            
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.FACTURA.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_DEBITO.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true
                                        , false, comp.getId(), Constantes.TABLA_COMA_RETENCION, r.getMensaje()));
                                if(comp.getIdFacturaRetencion()!=null){
                                    re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true
                                            , false, comp.getIdFacturaRetencion(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                                }
                            }
                        } else if (respuestaRecepcion.getEstado().equals("DEVUELTA")) {
                            String tipo = null;
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.FACTURA.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, false
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                                tipo = Constantes.TIPO_COMPROBANTE_FACTURA_VENTA;
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, false
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                                tipo = Constantes.TIPO_COMPROBANTE_NOTA_CREDITO;
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_DEBITO.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, false
                                        , false, comp.getId(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                                tipo = Constantes.TIPO_COMPROBANTE_NOTA_DEBITO;
                            }
                            if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode()) == 0) {
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, false
                                        , false, comp.getId(), Constantes.TABLA_COMA_RETENCION, r.getMensaje()));
                                if(comp.getIdFacturaRetencion()!=null){
                                    re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, false
                                            , false, comp.getIdFacturaRetencion(), Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                                }
                                tipo = Constantes.TIPO_COMPROBANTE_RETENCION;
                            }
                            re = validaRespuestaDao(logComprobanteObjetoDao.insertarLogComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_LOG_OBJETO, 
                                    comp.getXml(), comp, tipo, respuestaRecepcion.getEstado(), 
                                    r.getMensaje(), ""));
                        }
                    }
                }
                if (!comp.isAutorizacionSri() && comp.isRecibidoSri() && comp.isEnviadoSri()) {
                    
                    RespuestaFuncion r=ArchivoUtils.enviarAutorizar(path, comp);
                    Autorizacion autorizacion=(Autorizacion) r.getObjeto();
                    
                    if (r.isEjecuto()) {
                        ts = convertirFecha(autorizacion.getFechaAutorizacion().toGregorianCalendar().getTime());
                        
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.FACTURA.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaDatosAutorizacionFacturaComprobante(true, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, ts, autorizacion.getNumeroAutorizacion()
                                    , r.getMensaje(),comp));
                            
                            guardarComprobanteObjetoFacturaVenta(path, autorizacion, ts
                                    ,  comp, servidorCorreoElectronico, plantillaCorreoElectronico);
                        }
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaDatosAutorizacionFacturaComprobante(true, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, ts, autorizacion.getNumeroAutorizacion()
                                    , r.getMensaje(),comp));
                            
                            guardarComprobanteObjetoNotaCredito(path, autorizacion, ts
                                    ,  comp, servidorCorreoElectronico, plantillaCorreoElectronico);
                        }
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_DEBITO.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaDatosAutorizacionFacturaComprobante(true, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, ts, autorizacion.getNumeroAutorizacion()
                                    , r.getMensaje(),comp));
                            
                            guardarComprobanteObjetoNotaDebito(path, autorizacion, ts
                                    ,  comp, servidorCorreoElectronico, plantillaCorreoElectronico);
                        }
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaDatosAutorizacionFacturaComprobante(true, comp.getId()
                                    , Constantes.TABLA_COMA_RETENCION, ts, autorizacion.getNumeroAutorizacion()
                                    , r.getMensaje(),comp));
                            if(comp.getIdFacturaRetencion()!=null){
                                re=validaRespuestaDao(facturaDao.actualizaDatosAutorizacionFacturaComprobante(true, comp.getIdFacturaRetencion()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, ts, autorizacion.getNumeroAutorizacion()
                                    , r.getMensaje(),comp));
                                
                                guardarComprobanteObjetoRetencion(path, autorizacion, ts
                                    ,  comp, servidorCorreoElectronico, plantillaCorreoElectronico);
                            }else{
                                guardarComprobanteObjetoRetencionInversion(path, autorizacion, ts
                                    ,  comp, servidorCorreoElectronico, plantillaCorreoElectronico);
                            }
                        }
                        comp.setAutorizacionSri(true);
                        String nombreArchivo = comp.getNombre_xml() + ".xml";
                        String dirAutorizados = path +"/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS + "/";
                        File archivoAutorizado = new File(dirAutorizados + File.separator + nombreArchivo);
                        archivoAutorizado.delete();
                    } else{                         
                        String tipo = null;
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.FACTURA.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true, false, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            tipo = Constantes.TIPO_COMPROBANTE_FACTURA_VENTA;
                        }
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true, false, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            tipo = Constantes.TIPO_COMPROBANTE_NOTA_CREDITO;
                        }                        
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.NOTA_DE_DEBITO.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true, false, comp.getId()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            tipo = Constantes.TIPO_COMPROBANTE_NOTA_DEBITO;
                        }                        
                        if (comp.getCodigoDocumento().compareTo(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode()) == 0) {
                            re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante( true, true, false, comp.getId()
                                    , Constantes.TABLA_COMA_RETENCION, r.getMensaje()));
                            if(comp.getIdFacturaRetencion()!=null){
                                re=validaRespuestaDao(facturaDao.actualizaEstadosFacturaComprobante(true, true, false, comp.getIdFacturaRetencion()
                                    , Constantes.TABLA_COMA_FACTURA_CAB, r.getMensaje()));
                            }
                            tipo = Constantes.TIPO_COMPROBANTE_RETENCION;
                        }
                        String estado = " ";
                        if(r.getObjeto()!=null){
                            estado = autorizacion.getEstado();
                        }else{
                            estado = Constantes.COMPROBANTE_NO_AUTORIZADO;
                        }                        
                        re = validaRespuestaDao(logComprobanteObjetoDao.insertarLogComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_LOG_OBJETO, 
                                    comp.getXml(), comp, tipo, estado, 
                                    r.getMensaje(), ""));
                    }
                }
            }
            resp.setEjecuto(true);
            resp.setObjeto(null);
            resp.setMensaje("ok");
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Finaliza Proceso: " + "\n");
            
        } catch (SQLException ex) {
            resp.setEjecuto(false);
            resp.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, "Catch: " + ex.getCause());
        }
        return resp;
    }
    
    public Timestamp convertirFecha(Date d) {
        Timestamp ts = new Timestamp(d.getTime());

        return ts;
    }
    
    private void guardarComprobanteObjetoFacturaVenta(String path, Autorizacion autorizacion,
            Timestamp ts,  Comprobante comp,
            ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) {
        try {
                // Objeto XML
                String ubicacionXML = "/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS
                        + File.separator + comp.getNombre_xml()
                        + Constantes.EXTENSION_XML;
                String tmp = path + ubicacionXML;
                Path ficheroXML = Paths.get(tmp);

                // Objeto PDF
                ImpresionDocPdf impPdf = new ImpresionDocPdf();
                String img = path + Constantes.PATH_LOGO_REPORTE;
                Map<String, Object> mapParams = new HashMap<>();
                mapParams.put("PATH_LOGO", img);
                mapParams.put("FACTURA_ID", comp.getId());
                mapParams.put("CLAVE_ACCESO", autorizacion.getNumeroAutorizacion());
                mapParams.put("FECHA_AUTORIZACION", ts);
                mapParams.put("NUMERO_AUTORIZACION", autorizacion.getNumeroAutorizacion());
                mapParams.put("EMISION", comp.getNombreEmision());
                mapParams.put("AMBIENTE", comp.getNombreAmbiente());
                mapParams.put("titulo", "facturaVenta");
                impPdf.fileIreportDownload(path + ConstantesCompras.REPORTE_FACTURA_VENTA_CLIENTE, mapParams);                       

                byte[] ride = IOUtils.toByteArray(impPdf.getFile().getStream());
                byte[] xml = Files.readAllBytes(ficheroXML);

                RespuestaFuncion re=validaRespuestaDao(objetoDao.insertarComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_OBJETO
                        , xml, ride,comp));
                if(re.isEjecuto()){
                    Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Objeto facturaVenta guardado",null);
                    if(validarCorreo(comp.getCorreoElectronicoCliente())){
                            enviarCorreoElectronicoFacturaVenta(comp, servidorCorreoElectronico
                                , plantillaCorreoElectronico, autorizacion, ride, xml);
                    }else{
                        Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al enviar por correo, dirección de correo electronico invalida",null);
                    }
                }                
            } catch (Exception ex) {
                System.out.println("ex: " + ex.getMessage());
                Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
            }
    }

    private void guardarComprobanteObjetoNotaCredito(String path, Autorizacion autorizacion,
            Timestamp ts,  Comprobante comp,
            ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) {
        try {            
            // Objeto XML
            String ubicacionXML = "/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS
                    + File.separator + comp.getNombre_xml()
                    + Constantes.EXTENSION_XML;
            String tmp = path + ubicacionXML;
            Path ficheroXML = Paths.get(tmp);

            // Objeto PDF
            ImpresionDocPdf impPdf = new ImpresionDocPdf();
            String img = path + Constantes.PATH_LOGO_REPORTE;
            Map<String, Object> mapParams = new HashMap<>();
            mapParams.put("PATH_LOGO", img);
            mapParams.put("FACTURA_ID", comp.getId());
            mapParams.put("CLAVE_ACCESO", autorizacion.getNumeroAutorizacion());
            mapParams.put("FECHA_AUTORIZACION", ts);
            mapParams.put("NUMERO_AUTORIZACION", autorizacion.getNumeroAutorizacion());
            mapParams.put("EMISION", comp.getNombreEmision());
            mapParams.put("AMBIENTE", comp.getNombreAmbiente());
            mapParams.put("titulo", "notaCredito");
            impPdf.fileIreportDownload(path + ConstantesCompras.REPORTE_NOTA_CREDITO, mapParams);
            byte[] ride = IOUtils.toByteArray(impPdf.getFile().getStream());
            byte[] xml = Files.readAllBytes(ficheroXML);

            RespuestaFuncion re=validaRespuestaDao(objetoDao.insertarComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_OBJETO
                    , xml, ride,comp));
            if(re.isEjecuto()){
                Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Objeto notaCredito guardado",null);
                if(validarCorreo(comp.getCorreoElectronicoCliente())){
                    enviarCorreoElectronicoNotaCredito(comp, servidorCorreoElectronico
                            , plantillaCorreoElectronico, autorizacion, ride, xml);
                }else{
                    Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al enviar por correo, dirección de correo electronico invalida",null);
                }
            }            
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    private void guardarComprobanteObjetoNotaDebito(String path, Autorizacion autorizacion,
            Timestamp ts,  Comprobante comp,
            ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) {
        try {            
            // Objeto XML
            String ubicacionXML = "/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS
                    + File.separator + comp.getNombre_xml()
                    + Constantes.EXTENSION_XML;
            String tmp = path + ubicacionXML;
            Path ficheroXML = Paths.get(tmp);

            // Objeto PDF
            ImpresionDocPdf impPdf = new ImpresionDocPdf();
            String img = path + Constantes.PATH_LOGO_REPORTE;
            Map<String, Object> mapParams = new HashMap<>();
            mapParams.put("PATH_LOGO", img);
            mapParams.put("FACTURA_ID", comp.getId());
            mapParams.put("CLAVE_ACCESO", autorizacion.getNumeroAutorizacion());
            mapParams.put("FECHA_AUTORIZACION", ts);
            mapParams.put("NUMERO_AUTORIZACION", autorizacion.getNumeroAutorizacion());
            mapParams.put("EMISION", comp.getNombreEmision());
            mapParams.put("AMBIENTE", comp.getNombreAmbiente());
            mapParams.put("titulo", "notaDebito");
            impPdf.fileIreportDownload(path + ConstantesCompras.REPORTE_NOTA_DEBITO, mapParams);
            byte[] ride = IOUtils.toByteArray(impPdf.getFile().getStream());
            byte[] xml = Files.readAllBytes(ficheroXML);

            RespuestaFuncion re=validaRespuestaDao(objetoDao.insertarComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_OBJETO
                    , xml, ride,comp));
            if(re.isEjecuto()){
                Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Objeto notaDebito guardado",null);
                if(validarCorreo(comp.getCorreoElectronicoCliente())){
                    enviarCorreoElectronicoNotaDebito(comp, servidorCorreoElectronico
                            , plantillaCorreoElectronico, autorizacion, ride, xml);
                }else{
                    Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al enviar por correo, dirección de correo electronico invalida",null);
                }
            }            
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    public void guardarComprobanteObjetoRetencion(String path, Autorizacion autorizacion,
            Timestamp ts,  Comprobante comp,
            ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) {
        try {            
            // Objeto XML
            String ubicacionXML = "/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS
                    + File.separator + comp.getNombre_xml()
                    + Constantes.EXTENSION_XML;
            String tmp = path + ubicacionXML;
            Path ficheroXML = Paths.get(tmp);

            // Objeto PDF
            ImpresionDocPdf impPdf = new ImpresionDocPdf();
            String img = path + Constantes.PATH_LOGO_REPORTE;
            Map<String, Object> mapParams = new HashMap<>();
            mapParams.put("PATH_LOGO", img);
            mapParams.put("RETENCION_ID", comp.getId());
            mapParams.put("CLAVE_ACCESO", autorizacion.getNumeroAutorizacion());
            mapParams.put("FECHA_AUTORIZACION", ts);
            mapParams.put("NUMERO_AUTORIZACION", autorizacion.getNumeroAutorizacion());
            mapParams.put("EMISION", comp.getNombreEmision());
            mapParams.put("AMBIENTE", comp.getNombreAmbiente());
            mapParams.put("titulo", "comprobanteRetencion");
            impPdf.fileIreportDownload(path + ConstantesCompras.REPORTE_COMPROBANTE_RETENCION_CLIENTE, mapParams);
            byte[] ride = IOUtils.toByteArray(impPdf.getFile().getStream());
            byte[] xml = Files.readAllBytes(ficheroXML);

            RespuestaFuncion re=validaRespuestaDao(objetoDao.insertarComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_OBJETO
                    , xml, ride,comp));
            if(re.isEjecuto()){
                Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Objeto comprobanteRetencion guardado",null);
                if(validarCorreo(comp.getCorreoElectronicoProveedor())){
                        enviarCorreoElectronicoRetencion(comp, servidorCorreoElectronico
                                , plantillaCorreoElectronico, autorizacion, ride, xml);
                }else{
                    Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al enviar por correo, dirección de correo electronico invalida",null);
                }
            }
            
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    public void guardarComprobanteObjetoRetencionInversion(String path, Autorizacion autorizacion,
            Timestamp ts,  Comprobante comp,
            ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico) {
        try {            
            // Objeto XML
            String ubicacionXML = "/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS
                    + File.separator + comp.getNombre_xml()
                    + Constantes.EXTENSION_XML;
            String tmp = path + ubicacionXML;
            Path ficheroXML = Paths.get(tmp);

            // Objeto PDF
            ImpresionDocPdf impPdf = new ImpresionDocPdf();
            String img = path + Constantes.PATH_LOGO_REPORTE;
            Map<String, Object> mapParams = new HashMap<>();
            mapParams.put("PATH_LOGO", img);
            mapParams.put("RETENCION_ID", comp.getId());
            mapParams.put("CLAVE_ACCESO", autorizacion.getNumeroAutorizacion());
            mapParams.put("FECHA_AUTORIZACION", ts);
            mapParams.put("NUMERO_AUTORIZACION", autorizacion.getNumeroAutorizacion());
            mapParams.put("EMISION", comp.getNombreEmision());
            mapParams.put("AMBIENTE", comp.getNombreAmbiente());
            
            mapParams.put("TRNX_INTERES", Constantes.TIPO_TRNX_INTERES);
            mapParams.put("TRNX_IMPUESTO", Constantes.TIPO_TRNX_IMPUESTO);
            mapParams.put("TIPO_DIRECCION", Constantes.TIPO_DIRECCION);
            mapParams.put("NUM_COMP", Constantes.NUMERO_DOC_SUSTENTO_INVERSION);
            mapParams.put("IMPUESTO", Constantes.TIPO_IMPUESTO);
            
            mapParams.put("titulo", "comprobanteRetencion");
            impPdf.fileIreportDownload(path + ConstantesCompras.REPORTE_COMPROBANTE_RETENCION_CLIENTE_INVERSION, mapParams);
            byte[] ride = IOUtils.toByteArray(impPdf.getFile().getStream());
            byte[] xml = Files.readAllBytes(ficheroXML);

            RespuestaFuncion re=validaRespuestaDao(objetoDao.insertarComprobanteObjeto(Constantes.TABLA_COMA_COMPROBANTE_OBJETO
                    , xml, ride,comp));
            List<ImpuestosRetencion> listaImpuestoRetencion = comp.getListaImpuestoRetencion();
            if(!listaImpuestoRetencion.isEmpty()){
                for(ImpuestosRetencion imp : listaImpuestoRetencion){
                    RespuestaFuncion res = validaRespuestaDao(retencionDao.insertarDetalleRetencionInversion(Constantes.TABLA_COMA_RETENCION_DET,
                    imp.getRetencioId(),imp.getNombreImpuesto(),imp.getBase(),imp.getImpuesto()));
                    if(res.isEjecuto()){
                        Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Impuesto detalle retención gurdado correctamente",null);
                    }else{
                        Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al guardar detalle impuesto retención",null);
                    }
                }
            }
            
            
            if(re.isEjecuto()){
                Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Objeto comprobanteRetencion Inversión guardado",null);
                if(validarCorreo(comp.getCorreoElectronicoCliente())){
                    enviarCorreoElectronicoRetencionInversion(comp, servidorCorreoElectronico
                            , plantillaCorreoElectronico, autorizacion, ride, xml);
                }else{
                    Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Error al enviar por correo, dirección de correo electronico invalida",null);
                }
            }            
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    public void enviarCorreoElectronicoRetencion(Comprobante comp, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, Autorizacion autorizacion,
            byte[] ride, byte[] xml) {
        RespuestaFuncion r = new RespuestaFuncion();
        try {
//            Date fecha = procedimientoDao.getFechaServidorBD();
            long t1 = Calendar.getInstance().getTimeInMillis();
            String mensajeCuerpo = plantillaCorreoElectronico.getBodyHtml();
            String asunto = plantillaCorreoElectronico.getAsunto();

            mensajeCuerpo = mensajeCuerpo.replace("$P{razon_social_cliente_proveedor}",
                    comp.getRazonSocial());
            mensajeCuerpo = mensajeCuerpo.replace("$P{tipo_documento}", ConstantesCompras.DOCUMENTO_RETENCION);
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_documento}", comp.getNumeroComprobante());
            mensajeCuerpo = mensajeCuerpo.replace("$P{clave_acceso}", comp.getNombre_xml());
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_autorizacion}", autorizacion.getNumeroAutorizacion());
            mensajeCuerpo = mensajeCuerpo.replace("$P{empresa}", comp.getNombreEmpresa());
            
//            mensajeCuerpo = new String(mensajeCuerpo.getBytes("UTF-8"),"ISO-8859-15");
            long t2 = Calendar.getInstance().getTimeInMillis();

            long t3 = Calendar.getInstance().getTimeInMillis();
//            EnvioCorreo.enviarCorreoDesdeBd(servidorCorreoElectronico, plantillaCorreoElectronico
//                    , autorizacion.getNumeroAutorizacion(), ride, xml
//                    , comp.getCorreoElectronicoProveedor(), asunto, mensajeCuerpo);

            //codigo para almacenar en la tabla doca_correo_electronico            
            DocaCorreoElectronico correoElectronico = new DocaCorreoElectronico();
            correoElectronico.setEntidadId(comp.getEntidadId());
            correoElectronico.setRegistroId(comp.getId());
            correoElectronico.setEnviado(false);
            correoElectronico.setEntregado(false);
            correoElectronico.setCorreoElectronicoPlantillaId(6);
            correoElectronico.setAsunto(plantillaCorreoElectronico.getAsunto());
            correoElectronico.setBodyHtml(mensajeCuerpo);
            correoElectronico.setEmailTo(comp.getCorreoElectronicoProveedor());
            correoElectronico.setIdAuditoria(".");
            r =  docaCorreoElectronicoDao.insertarCorreoElectronico(correoElectronico);
            
            long t4 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Tiempo Plantilla:" + (t2 - t1) + "ms");
            System.out.println("Obtiene Bytes:" + (t3 - t2) + "ms");
            System.out.println("Envio Mail:" + (t4 - t3) + "ms");
            System.out.println("Tiempo Total:" + (t4 - t1) + "ms");
            Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Comprobante Retencion listo para ser enviado por e-mail"+
                    " "+comp.getNombre_xml()+"\n",null);            
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage()+ r.getMensaje());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage() + r.getMensaje(), ex.getCause());
        }
    }
    
    public void enviarCorreoElectronicoRetencionInversion(Comprobante comp, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, Autorizacion autorizacion,
            byte[] ride, byte[] xml) {
        try {

            long t1 = Calendar.getInstance().getTimeInMillis();
            String mensajeCuerpo = plantillaCorreoElectronico.getBodyHtml();
            String asunto = plantillaCorreoElectronico.getAsunto();

            mensajeCuerpo = mensajeCuerpo.replace("$P{razon_social_cliente_proveedor}",
                    comp.getRazonSocial());
            mensajeCuerpo = mensajeCuerpo.replace("$P{tipo_documento}", ConstantesCompras.DOCUMENTO_RETENCION);
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_documento}", comp.getNumeroComprobante());
            mensajeCuerpo = mensajeCuerpo.replace("$P{clave_acceso}", comp.getNombre_xml());
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_autorizacion}", autorizacion.getNumeroAutorizacion());
            mensajeCuerpo = mensajeCuerpo.replace("$P{empresa}", comp.getNombreEmpresa());
            
//            mensajeCuerpo = new String(mensajeCuerpo.getBytes("UTF-8"),"ISO-8859-15");
            long t2 = Calendar.getInstance().getTimeInMillis();

            long t3 = Calendar.getInstance().getTimeInMillis();
//            EnvioCorreo.enviarCorreoDesdeBd(servidorCorreoElectronico, plantillaCorreoElectronico
//                    , autorizacion.getNumeroAutorizacion(), ride, xml
//                    , comp.getCorreoElectronicoCliente(), asunto, mensajeCuerpo);

            //codigo para almacenar en la tabla doca_correo_electronico            
            DocaCorreoElectronico correoElectronico = new DocaCorreoElectronico();
            correoElectronico.setEntidadId(comp.getEntidadId());
            correoElectronico.setRegistroId(comp.getId());
            correoElectronico.setEnviado(false);
            correoElectronico.setEntregado(false);
            correoElectronico.setCorreoElectronicoPlantillaId(6);
            correoElectronico.setAsunto(plantillaCorreoElectronico.getAsunto());
            correoElectronico.setBodyHtml(mensajeCuerpo);
            correoElectronico.setEmailTo(comp.getCorreoElectronicoCliente());
            correoElectronico.setIdAuditoria(".");
            docaCorreoElectronicoDao.insertarCorreoElectronico(correoElectronico);
            
            long t4 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Tiempo Plantilla:" + (t2 - t1) + "ms");
            System.out.println("Obtiene Bytes:" + (t3 - t2) + "ms");
            System.out.println("Envio Mail:" + (t4 - t3) + "ms");
            System.out.println("Tiempo Total:" + (t4 - t1) + "ms");
            Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Comprobante Retencion listo para ser enviado por e-mail"+
                    " "+comp.getNombre_xml()+"\n",null);
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }

    public void enviarCorreoElectronicoFacturaVenta(Comprobante comp, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, Autorizacion autorizacion,
            byte[] ride, byte[] xml) {

        try {
            long t1 = Calendar.getInstance().getTimeInMillis();
            String mensajeCuerpo = plantillaCorreoElectronico.getBodyHtml();
            String asunto = plantillaCorreoElectronico.getAsunto();

            mensajeCuerpo = mensajeCuerpo.replace("$P{razon_social_cliente_proveedor}",
                    comp.getRazonSocial());
            mensajeCuerpo = mensajeCuerpo.replace("$P{tipo_documento}", ConstantesCompras.DOCUMENTO_FACTURA_VENTA);
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_documento}", comp.getNumeroComprobante());
            mensajeCuerpo = mensajeCuerpo.replace("$P{clave_acceso}", comp.getNombre_xml());
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_autorizacion}", autorizacion.getNumeroAutorizacion());
            mensajeCuerpo = mensajeCuerpo.replace("$P{empresa}", comp.getNombreEmpresa());
            
//            mensajeCuerpo = new String(mensajeCuerpo.getBytes("UTF-8"),"ISO-8859-15");
            long t2 = Calendar.getInstance().getTimeInMillis();

            long t3 = Calendar.getInstance().getTimeInMillis();
//            EnvioCorreo.enviarCorreoDesdeBd(servidorCorreoElectronico, plantillaCorreoElectronico
//                    , autorizacion.getNumeroAutorizacion(), ride, xml
//                    , comp.getCorreoElectronicoCliente(), asunto, mensajeCuerpo);

            //codigo para almacenar en la tabla doca_correo_electronico            
            DocaCorreoElectronico correoElectronico = new DocaCorreoElectronico();
            correoElectronico.setEntidadId(comp.getEntidadId());
            correoElectronico.setRegistroId(comp.getId());
            correoElectronico.setEnviado(false);
            correoElectronico.setEntregado(false);
            correoElectronico.setCorreoElectronicoPlantillaId(6);
            correoElectronico.setAsunto(plantillaCorreoElectronico.getAsunto());
            correoElectronico.setBodyHtml(mensajeCuerpo);
            correoElectronico.setEmailTo(comp.getCorreoElectronicoCliente());
            correoElectronico.setIdAuditoria(".");
            docaCorreoElectronicoDao.insertarCorreoElectronico(correoElectronico);
            
            long t4 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Tiempo Plantilla:" + (t2 - t1) + "ms");
            System.out.println("Obtiene Bytes:" + (t3 - t2) + "ms");
            System.out.println("Envio Mail:" + (t4 - t3) + "ms");
            System.out.println("Tiempo Total:" + (t4 - t1) + "ms");
            Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Comprobante Factura Venta listo para ser enviado por e-mail"+
                    " "+comp.getNombre_xml()+"\n",null);
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }

    public void enviarCorreoElectronicoNotaCredito(Comprobante comp, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, Autorizacion autorizacion,
            byte[] ride, byte[] xml) {

        try {
            long t1 = Calendar.getInstance().getTimeInMillis();
            String mensajeCuerpo = plantillaCorreoElectronico.getBodyHtml();
            String asunto = plantillaCorreoElectronico.getAsunto();

            mensajeCuerpo = mensajeCuerpo.replace("$P{razon_social_cliente_proveedor}",
                    comp.getRazonSocial());
            mensajeCuerpo = mensajeCuerpo.replace("$P{tipo_documento}", ConstantesCompras.DOCUMENTO_NOTA_CREDITO);
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_documento}", comp.getNumeroComprobante());
            mensajeCuerpo = mensajeCuerpo.replace("$P{clave_acceso}", comp.getNombre_xml());
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_autorizacion}", autorizacion.getNumeroAutorizacion());
            mensajeCuerpo = mensajeCuerpo.replace("$P{empresa}", comp.getNombreEmpresa());
            
//            mensajeCuerpo = new String(mensajeCuerpo.getBytes("UTF-8"),"ISO-8859-15");
            long t2 = Calendar.getInstance().getTimeInMillis();

            long t3 = Calendar.getInstance().getTimeInMillis();
//            EnvioCorreo.enviarCorreoDesdeBd(servidorCorreoElectronico, plantillaCorreoElectronico
//                    , autorizacion.getNumeroAutorizacion(), ride, xml
//                    , comp.getCorreoElectronicoCliente(), asunto, mensajeCuerpo);

            //codigo para almacenar en la tabla doca_correo_electronico            
            DocaCorreoElectronico correoElectronico = new DocaCorreoElectronico();
            correoElectronico.setEntidadId(comp.getEntidadId());
            correoElectronico.setRegistroId(comp.getId());
            correoElectronico.setEnviado(false);
            correoElectronico.setEntregado(false);
            correoElectronico.setCorreoElectronicoPlantillaId(6);
            correoElectronico.setAsunto(plantillaCorreoElectronico.getAsunto());
            correoElectronico.setBodyHtml(mensajeCuerpo);
            correoElectronico.setEmailTo(comp.getCorreoElectronicoCliente());
            correoElectronico.setIdAuditoria(".");
            docaCorreoElectronicoDao.insertarCorreoElectronico(correoElectronico);
            
            long t4 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Tiempo Plantilla:" + (t2 - t1) + "ms");
            System.out.println("Obtiene Bytes:" + (t3 - t2) + "ms");
            System.out.println("Envio Mail:" + (t4 - t3) + "ms");
            System.out.println("Tiempo Total:" + (t4 - t1) + "ms");
            Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Comprobante Nota Crédito listo para ser enviado por e-mail"+
                    " "+" "+comp.getNombre_xml()+"\n",null);
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    public void enviarCorreoElectronicoNotaDebito(Comprobante comp, ServidorCorreoElectronico servidorCorreoElectronico,
            PlantillaCorreoElectronico plantillaCorreoElectronico, Autorizacion autorizacion,
            byte[] ride, byte[] xml) {

        try {            
            long t1 = Calendar.getInstance().getTimeInMillis();
            String mensajeCuerpo = plantillaCorreoElectronico.getBodyHtml();
            String asunto = plantillaCorreoElectronico.getAsunto();

            mensajeCuerpo = mensajeCuerpo.replace("$P{razon_social_cliente_proveedor}",
                    comp.getRazonSocial());
            mensajeCuerpo = mensajeCuerpo.replace("$P{tipo_documento}", ConstantesCompras.DOCUMENTO_NOTA_DEBITO);
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_documento}", comp.getNumeroComprobante());
            mensajeCuerpo = mensajeCuerpo.replace("$P{clave_acceso}", comp.getNombre_xml());
            mensajeCuerpo = mensajeCuerpo.replace("$P{numero_autorizacion}", autorizacion.getNumeroAutorizacion());
            mensajeCuerpo = mensajeCuerpo.replace("$P{empresa}", comp.getNombreEmpresa());
            
//            mensajeCuerpo = new String(mensajeCuerpo.getBytes("UTF-8"),"ISO-8859-15");
            long t2 = Calendar.getInstance().getTimeInMillis();

            long t3 = Calendar.getInstance().getTimeInMillis();
//            EnvioCorreo.enviarCorreoDesdeBd(servidorCorreoElectronico, plantillaCorreoElectronico
//                    , autorizacion.getNumeroAutorizacion(), ride, xml
//                    , comp.getCorreoElectronicoCliente(), asunto, mensajeCuerpo);

            //codigo para almacenar en la tabla doca_correo_electronico            
            DocaCorreoElectronico correoElectronico = new DocaCorreoElectronico();
            correoElectronico.setEntidadId(comp.getEntidadId());
            correoElectronico.setRegistroId(comp.getId());
            correoElectronico.setEnviado(false);
            correoElectronico.setEntregado(false);
            correoElectronico.setCorreoElectronicoPlantillaId(6);
            correoElectronico.setAsunto(plantillaCorreoElectronico.getAsunto());
            correoElectronico.setBodyHtml(mensajeCuerpo);
            correoElectronico.setEmailTo(comp.getCorreoElectronicoCliente());
            correoElectronico.setIdAuditoria(".");
            docaCorreoElectronicoDao.insertarCorreoElectronico(correoElectronico);
            
            long t4 = Calendar.getInstance().getTimeInMillis();

            System.out.println("Tiempo Plantilla:" + (t2 - t1) + "ms");
            System.out.println("Obtiene Bytes:" + (t3 - t2) + "ms");
            System.out.println("Envio Mail:" + (t4 - t3) + "ms");
            System.out.println("Tiempo Total:" + (t4 - t1) + "ms");
            Logger.getLogger(MonitorService.class.getName()).log(Level.INFO, "Comprobante Nota Débito listo para ser enviado por e-mail"+
                                        " "+comp.getNombre_xml()+"\n",null);
        } catch (Exception ex) {
            System.out.println("ex: " + ex.getMessage());
            Logger.getLogger(MonitorService.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
    }
    
    public boolean validarCorreo(String email) {
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(email);

        if (mather.find() == true) {
            return true;
        }
        return false;
    }
    
    public RespuestaFuncion generarComprobanteFacturaVenta(Long id) {
        RespuestaFuncion r=facturaDao.getComprobantesFacturaVentaFirmar(id); 
        return r;
    }
    
    public RespuestaFuncion generarComprobanteRetencion(Long id) {
        RespuestaFuncion r=retencionDao.getComprobantesRetencionFirmar(id); 
        return r;
    }
    
    public RespuestaFuncion generarComprobanteRetencionInversiones(Long id) {
        RespuestaFuncion r=retencionDao.getComprobantesRetencionInversionesFirmar(id); 
        return r;
    }
    
    public RespuestaFuncion getRetencionNombreImpuestoInversion(String codigoApp,
            Integer ejercicio) {
        RespuestaFuncion r=facturaImpuestoDao.getRetencionNombreImpuestoInversion(codigoApp,
                ejercicio); 
        return r;
    }
    
    public RespuestaFuncion generarComprobanteNotaCredito(Long id) {
        RespuestaFuncion r=notaCreditoDao.getComprobantesNotaCreditoFirmar(id); 
        return r;
    }
    
    public RespuestaFuncion generarComprobanteNotaDebito(Long id) {
        RespuestaFuncion r=notaDebitoDao.getComprobantesNotaDebitoFirmar(id); 
        return r;
    }
    
}
