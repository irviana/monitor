/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.man;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ivan
 */
public class Comprobante implements Serializable {

    private Long id;
    private String numeroComprobante;
    private String nombre_xml;
    private byte[] xml;
    private boolean enviadoSri;
    private boolean recibidoSri;
    private boolean autorizacionSri;
    private Date fechaRegistro;
    private Date fechaAutorizacion;
    private String estadoAutorizacion;
    private String observacion;
    private String identificacion;
    private String codigoDocumento;
    private Long idFacturaRetencion;
    private String correoElectronicoCliente;
    private String correoElectronicoProveedor;
    private long idCliente;
    private String razonSocial;
    private String nombreEmpresa;
    private Integer entidadId;
    private Integer tipoAmbienteId;
    private Integer tipoEmisionId;
    private String codigoAmbiente;
    private String codigoEmision;
    private String nombreAmbiente;
    private String nombreEmision;
    private Long idInversion;
    private Integer ejercicio;
    private List<ImpuestosRetencion> listaImpuestoRetencion;

    public Comprobante(Long id, String numeroComprobante, String nombre_xml, byte[] xml, boolean enviadoSri,
            boolean recibidoSri, boolean autorizacionSri, Date fechaRegistro, Date fechaAutorizacion,
            String estadoAutorizacion, String observacion, String identificacion,
            String codigoDocumento, Long idFacturaRetencion,
            String correoElectronicoCliente, String correoElectronicoProveedor, long idCliente,
            String razonSocial, String nombreEmpresa, Integer entidadId,Integer tipoAmbienteId
            ,Integer tipoEmisionId,String codigoAmbiente,String codigoEmision,
            String nombreAmbiente,String nombreEmision,Long idInversion, Integer ejercicio) {
        this.id = id;
        this.numeroComprobante = numeroComprobante;
        this.nombre_xml = nombre_xml;
        this.xml = xml;
        this.enviadoSri = enviadoSri;
        this.recibidoSri = recibidoSri;
        this.autorizacionSri = autorizacionSri;
        this.fechaRegistro = fechaRegistro;
        this.fechaAutorizacion = fechaAutorizacion;
        this.estadoAutorizacion = estadoAutorizacion;
        this.observacion = observacion;
        this.identificacion = identificacion;
        this.codigoDocumento = codigoDocumento;
        this.idFacturaRetencion = idFacturaRetencion;
        this.correoElectronicoCliente = correoElectronicoCliente;
        this.correoElectronicoProveedor = correoElectronicoProveedor;
        this.idCliente = idCliente;
        this.razonSocial = razonSocial;
        this.nombreEmpresa = nombreEmpresa;
        this.tipoAmbienteId=tipoAmbienteId;
        this.tipoEmisionId=tipoEmisionId;
        this.codigoAmbiente=codigoAmbiente;
        this.codigoEmision=codigoEmision;
        this.nombreAmbiente=nombreAmbiente;
        this.nombreEmision=nombreEmision;
        this.idInversion = idInversion;
        this.ejercicio = ejercicio;
    }

    public Comprobante() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public String getNombre_xml() {
        return nombre_xml;
    }

    public void setNombre_xml(String nombre_xml) {
        this.nombre_xml = nombre_xml;
    }

    public byte[] getXml() {
        return xml;
    }

    public void setXml(byte[] xml) {
        this.xml = xml;
    }

    public boolean isEnviadoSri() {
        return enviadoSri;
    }

    public void setEnviadoSri(boolean enviadoSri) {
        this.enviadoSri = enviadoSri;
    }

    public boolean isRecibidoSri() {
        return recibidoSri;
    }

    public void setRecibidoSri(boolean recibidoSri) {
        this.recibidoSri = recibidoSri;
    }

    public boolean isAutorizacionSri() {
        return autorizacionSri;
    }

    public void setAutorizacionSri(boolean autorizacionSri) {
        this.autorizacionSri = autorizacionSri;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getEstadoAutorizacion() {
        return estadoAutorizacion;
    }

    public void setEstadoAutorizacion(String estadoAutorizacion) {
        this.estadoAutorizacion = estadoAutorizacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCodigoDocumento() {
        return codigoDocumento;
    }

    public void setCodigoDocumento(String codigoDocumento) {
        this.codigoDocumento = codigoDocumento;
    }

    public Long getIdFacturaRetencion() {
        return idFacturaRetencion;
    }

    public void setIdFacturaRetencion(Long idFacturaRetencion) {
        this.idFacturaRetencion = idFacturaRetencion;
    }

    public String getCorreoElectronicoCliente() {
        return correoElectronicoCliente;
    }

    public void setCorreoElectronicoCliente(String correoElectronicoCliente) {
        this.correoElectronicoCliente = correoElectronicoCliente;
    }

    public String getCorreoElectronicoProveedor() {
        return correoElectronicoProveedor;
    }

    public void setCorreoElectronicoProveedor(String correoElectronicoProveedor) {
        this.correoElectronicoProveedor = correoElectronicoProveedor;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Integer getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Integer entidadId) {
        this.entidadId = entidadId;
    }

    public Integer getTipoAmbienteId() {
        return tipoAmbienteId;
    }

    public void setTipoAmbienteId(Integer tipoAmbienteId) {
        this.tipoAmbienteId = tipoAmbienteId;
    }

    public Integer getTipoEmisionId() {
        return tipoEmisionId;
    }

    public void setTipoEmisionId(Integer tipoEmisionId) {
        this.tipoEmisionId = tipoEmisionId;
    }    

    public String getCodigoAmbiente() {
        return codigoAmbiente;
    }

    public void setCodigoAmbiente(String codigoAmbiente) {
        this.codigoAmbiente = codigoAmbiente;
    }

    public String getCodigoEmision() {
        return codigoEmision;
    }

    public void setCodigoEmision(String codigoEmision) {
        this.codigoEmision = codigoEmision;
    }

    public String getNombreAmbiente() {
        return nombreAmbiente;
    }

    public void setNombreAmbiente(String nombreAmbiente) {
        this.nombreAmbiente = nombreAmbiente;
    }

    public String getNombreEmision() {
        return nombreEmision;
    }

    public void setNombreEmision(String nombreEmision) {
        this.nombreEmision = nombreEmision;
    }

    public Long getIdInversion() {
        return idInversion;
    }

    public void setIdInversion(Long idInversion) {
        this.idInversion = idInversion;
    }

    public List<ImpuestosRetencion> getListaImpuestoRetencion() {
        return listaImpuestoRetencion;
    }

    public void setListaImpuestoRetencion(List<ImpuestosRetencion> listaImpuestoRetencion) {
        this.listaImpuestoRetencion = listaImpuestoRetencion;
    }

    public Integer getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio) {
        this.ejercicio = ejercicio;
    }    
}
