/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.man;

/**
 *
 * @author ivan
 */
public class DatosTabla {

    private String numeroComprobante;
    private String nombre_xml;
    private String enviadoSri;
    private String recibidoSri;
    private String autorizacionSri;

//    public DatosTabla(String numeroComprobante, String nombre_xml, String enviadoSri, String recibidoSri, String autorizacionSri) {
//        this.numeroComprobante = numeroComprobante;
//        this.nombre_xml = nombre_xml;
//        this.enviadoSri = enviadoSri;
//        this.recibidoSri = recibidoSri;
//        this.autorizacionSri = autorizacionSri;
//    }

    public DatosTabla() {
        numeroComprobante = "..";
        nombre_xml = "..";
        enviadoSri = "..";
        recibidoSri = "..";
        autorizacionSri = "..";
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public String getNombre_xml() {
        return nombre_xml;
    }

    public void setNombre_xml(String nombre_xml) {
        this.nombre_xml = nombre_xml;
    }

    public String getEnviadoSri() {
        return enviadoSri;
    }

    public void setEnviadoSri(String enviadoSri) {
        this.enviadoSri = enviadoSri;
    }

    public String getRecibidoSri() {
        return recibidoSri;
    }

    public void setRecibidoSri(String recibidoSri) {
        this.recibidoSri = recibidoSri;
    }

    public String getAutorizacionSri() {
        return autorizacionSri;
    }

    public void setAutorizacionSri(String autorizacionSri) {
        this.autorizacionSri = autorizacionSri;
    }

}
