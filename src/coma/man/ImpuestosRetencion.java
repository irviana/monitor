/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.man;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author ivan
 */
public class ImpuestosRetencion implements Serializable{
    private Long retencioId;
    private String nombreImpuesto;
    private BigDecimal base;
    private BigDecimal impuesto;

    public ImpuestosRetencion() {
    }

    public ImpuestosRetencion(Long retencioId, String nombreImpuesto, BigDecimal base, BigDecimal impuesto) {
        this.retencioId = retencioId;
        this.nombreImpuesto = nombreImpuesto;
        this.base = base;
        this.impuesto = impuesto;
    }

    public Long getRetencioId() {
        return retencioId;
    }

    public void setRetencioId(Long retencioId) {
        this.retencioId = retencioId;
    }

    public String getNombreImpuesto() {
        return nombreImpuesto;
    }

    public void setNombreImpuesto(String nombreImpuesto) {
        this.nombreImpuesto = nombreImpuesto;
    }

    public BigDecimal getBase() {
        return base;
    }

    public void setBase(BigDecimal base) {
        this.base = base;
    }

    public BigDecimal getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(BigDecimal impuesto) {
        this.impuesto = impuesto;
    }
    
}
