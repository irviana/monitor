/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.man;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import util.Constantes;

/**
 *
 * @author ivan
 */
public class ModeloDatosTablaComprobantes extends AbstractTableModel{
    
    private final Class[] tipoColumnas;
    private final String[] titleColumnas;
    private List<DatosTabla> datosTabla;

    public List<DatosTabla> getDatosTabla() {
        return datosTabla;
    }

    public void setDatosTabla(List<DatosTabla> datosTabla) {
        this.datosTabla = datosTabla;
    }

    public ModeloDatosTablaComprobantes() {
        datosTabla=new ArrayList<>();
        this.titleColumnas = new String[]{Constantes.NUMERO_COMPROBANTE_CAB,Constantes.COMPROBANTE_CAB,
            Constantes.ENVIADO_CAB,Constantes.RECIBIDO_CAB,Constantes.AUTORIZACION_SRI_CAB};
        this.tipoColumnas = new Class[]{String.class, String.class, String.class, String.class,String.class};
    }
    
    @Override
    public String getColumnName(int column) {
        return titleColumnas[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return tipoColumnas[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public int getRowCount() {
        return datosTabla.size();
    }

    @Override
    public int getColumnCount() {
        return titleColumnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return datosTabla.get(rowIndex).getNumeroComprobante();
            case 1:
                return datosTabla.get(rowIndex).getNombre_xml();
            case 2:
                return datosTabla.get(rowIndex).getEnviadoSri();
            case 3: 
                return datosTabla.get(rowIndex).getRecibidoSri();
            case 4: 
                return datosTabla.get(rowIndex).getAutorizacionSri();
            default:
                return null;
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                datosTabla.get(rowIndex).setNumeroComprobante(aValue.toString());
                break;
            case 1:
                datosTabla.get(rowIndex).setNombre_xml(aValue.toString());
                break;
            case 2:
                datosTabla.get(rowIndex).setEnviadoSri(aValue.toString());
                break;
            case 3:
                datosTabla.get(rowIndex).setRecibidoSri(aValue.toString());
                break;
            case 4:
                datosTabla.get(rowIndex).setAutorizacionSri(aValue.toString());
            default: ;
        }
        this.fireTableCellUpdated(rowIndex, columnIndex);
        this.fireTableRowsUpdated(rowIndex, rowIndex);        
    }
}
