/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.util.ConstantesCompras;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import sri.modelo.factura.Impuesto;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class FacturaDetalleImpuestoDao {

    public FacturaDetalleImpuestoDao() {
        
    } 
    
    public RespuestaFuncion getFacturaDetalleImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.subtotal"
                        + "        , d.factura_detalle_id"
                        + "        , g.codigo_ce codigo_grupo"
                        + "        , i.codigo_ce codigo_impuesto"
                        + "        , i.valor"
                        + "        , i.porcentaje"
                        + "     FROM coma_factura_detalle_impuesto d"
                        + "        , coma_factura_det f"
                        + "        , comc_grupo_impuesto g"
                        + "        , comc_impuesto i"
                        + "    WHERE d.factura_detalle_id   = ? "
                        + "      AND f.visible              = ?"
                        + "      AND d.visible              = ?"
                        + "      AND d.factura_detalle_id   = f.id"
                        + "      AND d.impuesto_id          = i.id"
                        + "      AND i.grupo_impuesto_id    = g.id ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.ps.setBoolean(3, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<Impuesto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    Impuesto i = new Impuesto();
                    i.setCodigo(conexion.rs.getString("codigo_grupo"));
                    i.setCodigoPorcentaje(conexion.rs.getString("codigo_impuesto"));
                    
                    BigDecimal baseImponible;
                    BigDecimal valor;
                    if (ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_BIENES_10.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_20.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_BIENES.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_100.equals(i.getCodigo())) {
                    baseImponible = conexion.rs.getBigDecimal("subtotal").multiply(new BigDecimal(0.14));
                    } else {
                        baseImponible = conexion.rs.getBigDecimal("subtotal");
                    }
                    valor = conexion.rs.getBigDecimal("subtotal").multiply(conexion.rs.getBigDecimal("valor"));

                    i.setTarifa(conexion.rs.getBigDecimal("porcentaje").setScale(0, RoundingMode.HALF_UP));
                    i.setBaseImponible(baseImponible.setScale(2, RoundingMode.HALF_UP));
                    i.setValor(valor.setScale(2, RoundingMode.HALF_UP));                    
                    
                    lista.add(i);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getNotaCreditoDetalleImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.subtotal"
                        + "        , d.factura_detalle_id"
                        + "        , g.codigo_ce codigo_grupo"
                        + "        , i.codigo_ce codigo_impuesto"
                        + "        , i.valor"
                        + "        , i.porcentaje"
                        + "     FROM coma_factura_detalle_impuesto d"
                        + "        , coma_factura_det f"
                        + "        , comc_grupo_impuesto g"
                        + "        , comc_impuesto i"
                        + "    WHERE d.factura_detalle_id   = ? "
                        + "      AND f.visible              = ?"
                        + "      AND d.visible              = ?"
                        + "      AND d.factura_detalle_id   = f.id"
                        + "      AND d.impuesto_id          = i.id"
                        + "      AND i.grupo_impuesto_id    = g.id ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.ps.setBoolean(3, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<sri.modelo.notacredito.Impuesto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    sri.modelo.notacredito.Impuesto i = new sri.modelo.notacredito.Impuesto();
                    i.setCodigo(conexion.rs.getString("codigo_grupo"));
                    i.setCodigoPorcentaje(conexion.rs.getString("codigo_impuesto"));
                    
                    BigDecimal baseImponible;
                    BigDecimal valor;
                    if (ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_BIENES_10.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_20.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_BIENES.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS.equals(i.getCodigo())
                    || ConstantesCompras.CODIGO_GRUPO_RETENCION_IVA_SERVICIOS_100.equals(i.getCodigo())) {
                    baseImponible = conexion.rs.getBigDecimal("subtotal").multiply(new BigDecimal(0.14));
                    } else {
                        baseImponible = conexion.rs.getBigDecimal("subtotal");
                    }
                    valor = conexion.rs.getBigDecimal("subtotal").multiply(conexion.rs.getBigDecimal("valor"));

                    i.setTarifa(conexion.rs.getBigDecimal("porcentaje").setScale(0, RoundingMode.HALF_UP));
                    i.setBaseImponible(baseImponible.setScale(2, RoundingMode.HALF_UP));
                    i.setValor(valor.setScale(2, RoundingMode.HALF_UP));                    
                    
                    lista.add(i);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

}
