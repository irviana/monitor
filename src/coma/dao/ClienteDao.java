/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.modelo.DatosCliente;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class ClienteDao {

    public ClienteDao() {
        
    } 
    
    public RespuestaFuncion getDatosCliente(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT c.identificacion"
                        + "         , c.apellidos_nombres_razon_social"
                        + "         , d.correo_electronico"
                        + "         , d.calle_principal"
                        + "         , d.calle_secundaria"
                        + "         , d.telefono_convencional"
                        + "         , t.codigo_ce"
                        + "     FROM clia_cliente c"
                        + "        , clia_direccion d"
                        + "        , clid_tipo_identificacion t"
                        + "    WHERE c.id                       = ? "
                        + "      AND c.id                       = d.cliente_id"
                        + "      AND d.tipo_direccion_id        = ?"
                        + "      AND c.tipo_identificacion_id   =t.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setLong(2, Constantes.TIPO_DIRECCION);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<DatosCliente> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    DatosCliente c = new DatosCliente();
                    c.setIdentificacion(conexion.rs.getString("identificacion"));
                    c.setRazonSocial(conexion.rs.getString("apellidos_nombres_razon_social"));
                    c.setEmail(conexion.rs.getString("correo_electronico"));
                    c.setTipoIdentificacion(conexion.rs.getString("codigo_ce"));
                    c.setCallePrincipal(conexion.rs.getString("calle_principal"));
                    c.setCalleSecundaria(conexion.rs.getString("calle_secundaria"));
                    c.setTelefonoConvencional(conexion.rs.getString("telefono_convencional"));
                    lista.add(c);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

}
