/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.modelo.DatosRetencionInversion;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class InversionDao {

    public InversionDao() {

    }

    public RespuestaFuncion getRetencioInversion(Long id, Date fechaContable) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "select sum(case when k.id = ? then t.valor_efectivo else 0 end) as base_imponible " 
                            +"     , sum(case when k.id = ? then t.valor_efectivo else 0 end) as impuesto_renta " 
                            +"  from inva_inversion i " 
                            +"     , inva_transaccion_inversion t " 
                            +"     , invc_transaccion c " 
                            +"     , capc_transaccion_tipo k " 
                            +" where i.id             = ? " 
                            +"   and i.id             = t.inversion_id " 
                            +"   and t.fecha_contable = ? " 
                            +"   and t.fila_activa    = ? " 
                            +"   and t.concepto_id    = c.id " 
                            +"   and c.tipo_id        = k.id " 
                            +"   and k.id in(?, ?) ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setInt(1, Constantes.TIPO_TRNX_INTERES);
                conexion.ps.setInt(2, Constantes.TIPO_TRNX_IMPUESTO);
                conexion.ps.setLong(3, id);
                conexion.ps.setDate(4, fechaContable);
                conexion.ps.setBoolean(5, true);
                conexion.ps.setInt(6, Constantes.TIPO_TRNX_INTERES);
                conexion.ps.setInt(7, Constantes.TIPO_TRNX_IMPUESTO);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<DatosRetencionInversion> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    DatosRetencionInversion retencionInversion = new DatosRetencionInversion();
                    retencionInversion.setBaseImponible(conexion.rs.getBigDecimal("base_imponible"));
                    retencionInversion.setImpuestoRenta(conexion.rs.getBigDecimal("impuesto_renta"));
                    lista.add(retencionInversion);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
