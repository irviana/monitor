/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.modelo.DatosProveedor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class ProveedorDao {

    public ProveedorDao() {
        
    }

    public RespuestaFuncion getDatosProveedor(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT p.identificacion "
                        + "        , p.razon_social "
                        + "        , p.correo_electronico "
                        + "        , p.direccion "
                        + "        , p.telefono_convencional "
                        + "        , t.codigo_ce "
                        + "     FROM coma_proveedor p "
                        + "        , clid_tipo_identificacion t "
                        + "    WHERE p.id                       = ? "
                        + "      and p.tipo_identificacion_id   = t.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<DatosProveedor> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    DatosProveedor c = new DatosProveedor();
                    c.setIdentificacion(conexion.rs.getString("identificacion"));
                    c.setRazonSocial(conexion.rs.getString("razon_social"));
                    c.setEmail(conexion.rs.getString("correo_electronico"));
                    c.setDireccion(conexion.rs.getString("direccion"));
                    c.setTelefonoConvencional(conexion.rs.getString("telefono_convencional"));
                    c.setTipoIdentificacion(conexion.rs.getString("codigo_ce"));
                    lista.add(c);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
