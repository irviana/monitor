/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import sri.modelo.factura.FacturaXML;
import sri.modelo.factura.Impuesto;
import sri.modelo.factura.ObjectFactory;
import sri.modelo.notacredito.NotaCredito;
import sri.util.StringUtil;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class FacturaDetalleDao {

    private final FacturaDetalleImpuestoDao facturaDetalleImpuestoDao;
    
    private final ObjectFactory facturaFactory;
    private final sri.modelo.notacredito.ObjectFactory factoryNotaCredito;
    
    public FacturaDetalleDao() {
        facturaDetalleImpuestoDao=new FacturaDetalleImpuestoDao();
        facturaFactory=new ObjectFactory();
        factoryNotaCredito=new sri.modelo.notacredito.ObjectFactory();
    } 
    
    public RespuestaFuncion getFacturaDetalle(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.id"
                        + "        , f.nombre"
                        + "        , f.cantidad"
                        + "        , f.precio_unitario"
                        + "        , f.valor_descuento"
                        + "        , f.subtotal"
                        + "        , i.codigo"
                        + "     FROM coma_factura_det f"
                        + "        , comc_item i"
                        + "    WHERE f.factura_id   = ? "
                        + "      AND f.visible      = ?"
                        + "      AND f.item_id      = i.id ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<FacturaXML.Detalles.Detalle> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    
                    FacturaXML.Detalles.Detalle d=new FacturaXML.Detalles.Detalle();
                    d.setCodigoPrincipal(StringUtil.reemplazarCaracteresEspeciales(conexion.rs.getString("codigo")));
                    d.setDescripcion(StringUtil.reemplazarCaracteresEspeciales(conexion.rs.getString("nombre")));
                    d.setCantidad(conexion.rs.getBigDecimal("cantidad"));
                    d.setPrecioUnitario(conexion.rs.getBigDecimal("precio_unitario"));
                    d.setDescuento(conexion.rs.getBigDecimal("valor_descuento"));
                    d.setPrecioTotalSinImpuesto(conexion.rs.getBigDecimal("subtotal"));
                    
                    
                    RespuestaFuncion resp =facturaDetalleImpuestoDao.getFacturaDetalleImpuesto(conexion.rs.getLong("id"));
                    List<Impuesto> listaImpuesto=(List<Impuesto>) resp.getObjeto();
                    
                    FacturaXML.Detalles.Detalle.Impuestos result = facturaFactory.createFacturaDetallesDetalleImpuestos();
                    
                    for(Impuesto i:listaImpuesto){
                         result.getImpuesto().add(i);
                    }
                    d.setImpuestos(result);
                    
                    lista.add(d);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getNotaCreditoDetalle(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.id"
                        + "        , f.nombre"
                        + "        , f.cantidad"
                        + "        , f.precio_unitario"
                        + "        , f.valor_descuento"
                        + "        , f.subtotal"
                        + "        , i.codigo"
                        + "     FROM coma_factura_det f"
                        + "        , comc_item i"
                        + "    WHERE f.factura_id   = ? "
                        + "      AND f.visible      = ?"
                        + "      AND f.item_id      = i.id ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<NotaCredito.Detalles.Detalle> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    
                    NotaCredito.Detalles.Detalle d=new NotaCredito.Detalles.Detalle();
                    d.setCodigoInterno(StringUtil.reemplazarCaracteresEspeciales(conexion.rs.getString("codigo")));
                    d.setDescripcion(StringUtil.reemplazarCaracteresEspeciales(conexion.rs.getString("nombre")));
                    d.setCantidad(conexion.rs.getBigDecimal("cantidad"));
                    d.setPrecioUnitario(conexion.rs.getBigDecimal("precio_unitario"));
                    d.setDescuento(conexion.rs.getBigDecimal("valor_descuento"));
                    d.setPrecioTotalSinImpuesto(conexion.rs.getBigDecimal("subtotal"));
                    
                    
                    RespuestaFuncion resp =facturaDetalleImpuestoDao.getNotaCreditoDetalleImpuesto(conexion.rs.getLong("id"));
                    List<sri.modelo.notacredito.Impuesto> listaImpuesto=(List<sri.modelo.notacredito.Impuesto>) resp.getObjeto();
                    
                    NotaCredito.Detalles.Detalle.Impuestos result = factoryNotaCredito.createNotaCreditoDetallesDetalleImpuestos();
                    
                    for(sri.modelo.notacredito.Impuesto i:listaImpuesto){
                         result.getImpuesto().add(i);
                    }
                    d.setImpuestos(result);
                    
                    lista.add(d);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

}
