/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.man.Comprobante;
import coma.modelo.DatosCliente;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import jdbc.Conexion;
import sri.modelo.InfoTributaria;
import sri.modelo.factura.FacturaXML;
import sri.modelo.factura.ObjectFactory;
import sri.util.FormGenerales;
import sri.util.StringUtil;
import sri.util.TipoComprobanteEnum;
import util.Constantes;
import util.ConstantesCatalogo;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class FacturaDao {

    private final ClienteDao clienteDao;
    private final EntidadDao entidadDao;
    private final FacturaDetalleDao facturaDetalleDao;
    private final FacturaImpuestoDao facturaImpuestoDao;
    private final ObjectFactory facturaFactory;
    private InfoTributaria infoTributaria;
    private FacturaXML.InfoFactura infoFactura;

    public FacturaDao() {
        clienteDao = new ClienteDao();
        entidadDao = new EntidadDao();
        facturaDetalleDao = new FacturaDetalleDao();
        facturaImpuestoDao = new FacturaImpuestoDao();
        facturaFactory = new ObjectFactory();
    }

    public RespuestaFuncion getComprobantesFacturaVenta(int numeroComprobantes) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();
            
            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.id"
                        + "        , f.numero_factura"
                        + "        , f.fecha_contable"
                        + "        , f.fecha_autorizacion"
                        + "        , f.autorizacion_sri"
                        + "        , f.identificacion"
                        + "        , f.codigo_documento"
                        + "        , f.clave_acceso"
                        + "        , f.enviado_sri"
                        + "        , f.recibido_sri"
                        + "        , f.cliente_id"
                        + "        , o.nombre nombre_empresa"
                        + "        , a.id ambiente_id"
                        + "        , e.id emision_id"
                        + "        , a.codigo codigo_ambiente"
                        + "        , e.codigo codigo_emision"
                        + "        , a.nombre nombre_ambiente"
                        + "        , e.nombre nombre_emision"
                        + "        , (SELECT nombre razon_social FROM adm_oficina WHERE id=f.empresa_id)"
                        + "     FROM coma_factura_cab f "
                        + "        , adm_oficina o "
                        + "        , comd_estado_documento d"
                        + "        , comd_tipo_ambiente_ce a"
                        + "        , comd_tipo_emision_ce e "
                        + "    WHERE (f.tipo = ? or f.tipo = ? or f.tipo = ?)"
                        + "      and f.firmado              = ?"
                        + "      and (f.autorizacion_sri    = ?"
                        + "       or f.enviado_sri          = ?"
                        + "       or f.recibido_sri         = ?)"
                        + "      and f.oficina_id           = o.id "
                        + "      and f.estado_documento_id  = d.id"
                        + "      and d.nombre               = ? "
                        + "      and o.tipo_ambiente_id     = a.id"
                        + "      and o.tipo_emision_id      = e.id "
                        + " order by f.id";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setString(1, "venta");
                conexion.ps.setString(2, "credito");
                conexion.ps.setString(3, "debito");
                conexion.ps.setBoolean(4, Constantes.FIRMADO);
                conexion.ps.setBoolean(5, Constantes.SRI_AUTORIZACION);
                conexion.ps.setBoolean(6, Constantes.SRI_ENVIADO);
                conexion.ps.setBoolean(7, Constantes.SRI_RECIBIDO);
                conexion.ps.setString(8, Constantes.ESTADO_VALIDADO);
                conexion.ps.setMaxRows(numeroComprobantes);
                conexion.rs = conexion.ps.executeQuery();

                List<Comprobante> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    Comprobante c = new Comprobante();
                    c.setId(conexion.rs.getLong("id"));
                    c.setNumeroComprobante(conexion.rs.getString("numero_factura"));
                    c.setFechaRegistro(conexion.rs.getDate("fecha_contable"));
                    c.setFechaAutorizacion(conexion.rs.getDate("fecha_autorizacion"));
                    c.setAutorizacionSri(conexion.rs.getBoolean("autorizacion_sri"));
                    c.setIdentificacion(conexion.rs.getString("identificacion"));
                    c.setCodigoDocumento(conexion.rs.getString("codigo_documento"));
                    c.setNombre_xml(conexion.rs.getString("clave_acceso"));    //Es la clave de acceso
                    c.setEnviadoSri(conexion.rs.getBoolean("enviado_sri"));
                    c.setRecibidoSri(conexion.rs.getBoolean("recibido_sri"));
                    c.setTipoAmbienteId(conexion.rs.getInt("ambiente_id"));
                    c.setTipoEmisionId(conexion.rs.getInt("emision_id"));
                    c.setCodigoAmbiente(conexion.rs.getString("codigo_ambiente"));
                    c.setCodigoEmision(conexion.rs.getString("codigo_emision"));
                    c.setNombreAmbiente(conexion.rs.getString("nombre_ambiente"));
                    c.setNombreEmision(conexion.rs.getString("nombre_emision"));

                    c.setIdCliente(conexion.rs.getLong("cliente_id"));
                    r = clienteDao.getDatosCliente(conexion.rs.getLong("cliente_id"));
                    DatosCliente dc = new DatosCliente();
                    if (r.isEjecuto()) {
                        List<DatosCliente> listaCliente = (List<DatosCliente>) r.getObjeto();
                        if (!listaCliente.isEmpty()) {
                            dc = listaCliente.get(0);
                            if(dc.getEmail()==null){
                                dc.setEmail(".");
                            }
                        }
                    }
                    c.setIdentificacion(dc.getIdentificacion());
                    c.setRazonSocial(dc.getRazonSocial());
                    c.setCorreoElectronicoCliente(dc.getEmail());

                    c.setNombreEmpresa(conexion.rs.getString("razon_social"));

                    r = entidadDao.getEntidadId(ConstantesCatalogo.ENTIDAD_FACTURA);
                    Integer entidadId = null;
                    if (r.isEjecuto()) {
                        List<Integer> listaEntidadesIds = (List<Integer>) r.getObjeto();
                        if (!listaEntidadesIds.isEmpty()) {
                            entidadId = listaEntidadesIds.get(0);
                        }
                    }

                    c.setEntidadId(entidadId);

                    lista.add(c);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion getComprobantesFacturaVentaFirmar(Long id) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.id"
                        + "        , f.fecha "
                        + "        , f.numero_factura"
                        + "        , f.numero"
                        + "        , f.fecha_contable"
                        + "        , f.fecha_autorizacion"
                        + "        , f.autorizacion_sri"
                        + "        , f.identificacion"
                        + "        , f.codigo_documento"
                        + "        , f.clave_acceso"
                        + "        , f.enviado_sri"
                        + "        , f.recibido_sri"
                        + "        , f.cliente_id"
                        + "        , f.subtotal"
                        + "        , f.total_descuento"
                        + "        , f.propina"
                        + "        , f.total"
                        + "        , a.codigo codigo_ambiente"
                        + "        , e.codigo codigo_emision"
                        + "        , o.nombre nombre_empresa"
                        + "        , o.nombre_comercial"
                        + "        , o.ruc"
                        + "        , o.codigo_establecimiento"
                        + "        , o.codigo_punto_emision"
                        + "        , o.direccion"
                        + "        , o.contribuyente_especial"
                        + "        , p.codigo_ce forma_pago"
                        + "        , (SELECT nombre razon_social FROM adm_oficina WHERE id=f.empresa_id)"
                        + "     FROM coma_factura_cab f"
                        + "        , adm_oficina o"
                        + "        , comd_tipo_ambiente_ce a"
                        + "        , comd_tipo_emision_ce e"
                        + "        , comd_forma_pago p"
                        + "    WHERE f.id               = ?"
                        + "      and f.oficina_id       = o.id"
                        + "      and o.tipo_ambiente_id = a.id"
                        + "      and o.tipo_emision_id  = e.id"
                        + "      and f.forma_pago_id    = p.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.rs = conexion.ps.executeQuery();

                List<FacturaXML> lista = new ArrayList<>();

                while (conexion.rs.next()) {
                    FacturaXML facturaXML = new FacturaXML();
                    infoTributaria = new InfoTributaria();
                    infoTributaria.setSecuencial(String.format("%09d", conexion.rs.getInt("numero")));
                    infoTributaria.setAmbiente(conexion.rs.getString("codigo_ambiente"));
                    infoTributaria.setTipoEmision(conexion.rs.getString("codigo_emision"));
                    infoTributaria.setRazonSocial(conexion.rs.getString("razon_social"));
                    infoTributaria.setNombreComercial(conexion.rs.getString("nombre_comercial"));
                    infoTributaria.setRuc(conexion.rs.getString("ruc"));
                    infoTributaria.setCodDoc(TipoComprobanteEnum.FACTURA.getCode());
                    infoTributaria.setEstab(conexion.rs.getString("codigo_establecimiento"));
                    infoTributaria.setPtoEmi(conexion.rs.getString("codigo_punto_emision"));
                    infoTributaria.setDirMatriz(conexion.rs.getString("direccion"));

                    String claveacceso = new FormGenerales().obtieneClaveDeAcceso(String.format("%09d", conexion.rs.getInt("numero")),
                             conexion.rs.getString("codigo_establecimiento"),
                             conexion.rs.getString("codigo_punto_emision"),
                             conexion.rs.getString("codigo_emision"),
                             conexion.rs.getString("codigo_ambiente"),
                             conexion.rs.getString("ruc"),
                             conexion.rs.getDate("fecha"),
                             String.format("%08d", 12345678),
                             TipoComprobanteEnum.FACTURA.getCode());

                    infoTributaria.setClaveAcceso(claveacceso);

                    facturaXML.setInfoTributaria(infoTributaria);

                    r = clienteDao.getDatosCliente(conexion.rs.getLong("cliente_id"));
                    DatosCliente dc = new DatosCliente();
                    if (r.isEjecuto()) {
                        List<DatosCliente> listaCliente = (List<DatosCliente>) r.getObjeto();
                        if (!listaCliente.isEmpty()) {
                            dc = listaCliente.get(0);
                            if(dc.getEmail()==null){
                                dc.setEmail(".");
                            }
                        }
                    }
                    infoFactura = facturaFactory.createFacturaInfoFactura();

                    infoFactura.setFechaEmision(Constantes.dateFormat.format(conexion.rs.getDate("fecha")));
                    infoFactura.setDirEstablecimiento(conexion.rs.getString("direccion"));
                    infoFactura.setTipoIdentificacionComprador(dc.getTipoIdentificacion());
                    infoFactura.setIdentificacionComprador(dc.getIdentificacion());
                    infoFactura.setRazonSocialComprador(dc.getRazonSocial());

                    infoFactura.setTotalSinImpuestos(conexion.rs.getBigDecimal("subtotal").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoFactura.setTotalDescuento(conexion.rs.getBigDecimal("total_descuento").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoFactura.setPropina(conexion.rs.getBigDecimal("propina").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoFactura.setImporteTotal(conexion.rs.getBigDecimal("total").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoFactura.setMoneda("DOLAR");

                    RespuestaFuncion res = facturaImpuestoDao.getFacturaVentaImpuesto(conexion.rs.getLong("id"));
                    List<FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto> listaImpuestos = (List<FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto>) res.getObjeto();
                    FacturaXML.InfoFactura.TotalConImpuestos respuestaImpuesto = this.facturaFactory.createFacturaInfoFacturaTotalConImpuestos();

                    for (FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto i : listaImpuestos) {
                        respuestaImpuesto.getTotalImpuesto().add(i);
                    }
                    infoFactura.setTotalConImpuestos(respuestaImpuesto);

                    infoFactura.setContribuyenteEspecial(conexion.rs.getString("contribuyente_especial"));
                    infoFactura.setObligadoContabilidad("SI");

                    FacturaXML.Pago pago = this.facturaFactory.createFormaPago();
                    FacturaXML.Pago.DetallePago detallePago = new FacturaXML.Pago.DetallePago();
                    detallePago.setFormaPago(conexion.rs.getString("forma_pago"));
                    detallePago.setTotal(conexion.rs.getBigDecimal("total").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    pago.getPagos().add(detallePago);
                    infoFactura.setPagos(pago);

                    facturaXML.setInfoFactura(infoFactura);

                    FacturaXML.InfoAdicional info = this.facturaFactory.createFacturaInfoAdicional();

                    FacturaXML.InfoAdicional.CampoAdicional detalleDireccion = new FacturaXML.InfoAdicional.CampoAdicional();
                    detalleDireccion.setNombre("DIRECCION");
                    detalleDireccion.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getCallePrincipal() + " " + dc.getCalleSecundaria()));
                    info.getCampoAdicional().add(detalleDireccion);

                    FacturaXML.InfoAdicional.CampoAdicional detalleTelefono = new FacturaXML.InfoAdicional.CampoAdicional();
                    detalleTelefono.setNombre("TELEFONO");
                    detalleTelefono.setValue(dc.getTelefonoConvencional());
                    info.getCampoAdicional().add(detalleTelefono);

                    FacturaXML.InfoAdicional.CampoAdicional detalleEmail = new FacturaXML.InfoAdicional.CampoAdicional();
                    detalleEmail.setNombre("CORREO ELECTRONICO");
                    detalleEmail.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getEmail()));
                    info.getCampoAdicional().add(detalleEmail);

                    if (info.getCampoAdicional().size() > 0) {
                        facturaXML.setInfoAdicional(info);
                    }
                    facturaXML.setVersion("1.0.0");
                    facturaXML.setId("comprobante");

                    RespuestaFuncion re = facturaDetalleDao.getFacturaDetalle(conexion.rs.getLong("id"));
                    List<FacturaXML.Detalles.Detalle> listaDetalleFactura = (List<FacturaXML.Detalles.Detalle>) re.getObjeto();

                    FacturaXML.Detalles resultado = facturaFactory.createFacturaDetalles();

                    for (FacturaXML.Detalles.Detalle det : listaDetalleFactura) {
                        resultado.getDetalle().add(det);
                    }
                    facturaXML.setDetalles(resultado);

                    lista.add(facturaXML);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion actualizaEstadosFacturaComprobante(boolean enviadoSri,
            boolean recibidoSri, boolean autorizacionSri, Long id, String tabla, String resultado) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "UPDATE " + tabla + " "
                        + "     SET enviado_sri = ?"
                        + "         ,recibido_sri = ?"
                        + "         ,autorizacion_sri = ?"
                        + "         ,observacion = ? "
                        + "         ,firmado = ? "
                        + "   WHERE id = ?;";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setBoolean(1, enviadoSri);
                conexion.ps.setBoolean(2, recibidoSri);
                conexion.ps.setBoolean(3, autorizacionSri);
                conexion.ps.setString(4, resultado);
                conexion.ps.setBoolean(5, Boolean.TRUE);
                conexion.ps.setLong(6, id);
                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion actualizaDatosAutorizacionFacturaComprobante(boolean autorizacionSri, Long id, String tabla,
            Timestamp ts, String numeroAutorizacion, String resultado, Comprobante comp) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "UPDATE " + tabla + " "
                        + "     SET autorizacion_sri = ?"
                        + "         ,fecha_autorizacion = ?"
                        + "         ,numero_autorizacion = ?"
                        + "         ,estado_autorizacion = ?"
                        + "         ,observacion = ? "
                        + "         ,clave_acceso = ? "
                        + "         ,codigo_seguridad = ? "
                        + "         ,tipo_emision_id = ? "
                        + "         ,tipo_ambiente_id = ? "
                        + "         ,identificacion = ? "
                        + "         ,codigo_documento = ? "
                        + "         ,firmado = ? "
                        + "   WHERE id = ?;";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setBoolean(1, autorizacionSri);
                conexion.ps.setTimestamp(2, ts);
                conexion.ps.setString(3, numeroAutorizacion);
                conexion.ps.setString(4, Constantes.COMPROBANTE_AUTORIZADO);
                conexion.ps.setString(5, resultado);
                conexion.ps.setString(6, comp.getNombre_xml());
                conexion.ps.setString(7, Constantes.CODIGO_SEGURIDAD);
                conexion.ps.setInt(8, comp.getTipoEmisionId());
                conexion.ps.setInt(9, comp.getTipoAmbienteId());
                conexion.ps.setString(10, comp.getIdentificacion());
                conexion.ps.setString(11, comp.getCodigoDocumento());
                conexion.ps.setBoolean(12, Boolean.TRUE);
                conexion.ps.setLong(13, id);

                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

}
