/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.modelo.DatosCliente;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import jdbc.Conexion;
import sri.modelo.InfoTributaria;
import sri.modelo.notacredito.ObjectFactory;
import sri.modelo.notacredito.NotaCredito;
import sri.util.FormGenerales;
import sri.util.StringUtil;
import sri.util.TipoComprobanteEnum;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class NotaCreditoDao {

    private final ClienteDao clienteDao;
    private final FacturaDetalleDao facturaDetalleDao;
    private final FacturaImpuestoDao facturaImpuestoDao;
    private final ObjectFactory notaCreditoFactory;
    private InfoTributaria infoTributaria;
    private NotaCredito.InfoNotaCredito infoNotaCredito;

    public NotaCreditoDao() {
        clienteDao = new ClienteDao();
        facturaDetalleDao=new FacturaDetalleDao();
        facturaImpuestoDao=new FacturaImpuestoDao();
        notaCreditoFactory=new ObjectFactory();
    }

   public RespuestaFuncion getComprobantesNotaCreditoFirmar(Long id) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.id"
                        + "        , f.fecha "
                        + "        , f.numero_factura "
                        + "        , f.numero "
                        + "        , f.origen "
                        + "        , f.fecha_contable "
                        + "        , f.fecha_autorizacion "
                        + "        , f.autorizacion_sri "
                        + "        , f.identificacion "
                        + "        , f.codigo_documento "
                        + "        , f.clave_acceso "
                        + "        , f.enviado_sri "
                        + "        , f.recibido_sri "
                        + "        , f.cliente_id "
                        + "        , f.subtotal "
                        + "        , f.total_descuento "
                        + "        , f.propina "
                        + "        , f.total "
                        + "        , f.motivo "
                        + "        , a.codigo codigo_ambiente "
                        + "        , e.codigo codigo_emision "
                        + "        , o.nombre nombre_empresa "
                        + "        , o.nombre_comercial "
                        + "        , o.ruc "
                        + "        , o.codigo_establecimiento "
                        + "        , o.codigo_punto_emision "
                        + "        , o.direccion "
                        + "        , o.contribuyente_especial "
                        + "        , p.codigo_ce forma_pago "
                        + "        , (SELECT nombre razon_social FROM adm_oficina WHERE id=f.empresa_id)"
                        + "     FROM coma_factura_cab f"
                        + "        , adm_oficina o"
                        + "        , comd_tipo_ambiente_ce a"
                        + "        , comd_tipo_emision_ce e"
                        + "        , comd_forma_pago p"
                        + "    WHERE f.id               = ?"
//                        + "      and f.firmado= ?"
//                        + "      and (f.autorizacion_sri = ?"
//                        + "           or f.enviado_sri = ?"
//                        + "           or f.recibido_sri = ?)"
                        + "      and f.oficina_id       = o.id"
                        + "      and o.tipo_ambiente_id = a.id"
                        + "      and o.tipo_emision_id  = e.id"
                        + "      and f.forma_pago_id    = p.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
//                conexion.ps.setBoolean(2, Constantes.FIRMADO);
//                conexion.ps.setBoolean(3, Constantes.SRI_AUTORIZACION);
//                conexion.ps.setBoolean(4, Constantes.SRI_ENVIADO);
//                conexion.ps.setBoolean(5, Constantes.SRI_RECIBIDO);
                conexion.rs = conexion.ps.executeQuery();

                List<NotaCredito> lista = new ArrayList<>();
                
                while (conexion.rs.next()) {
                    NotaCredito notaCreditoXML=new NotaCredito();
                    infoTributaria = new InfoTributaria();
                    infoTributaria.setSecuencial(String.format("%09d", conexion.rs.getInt("numero")));
                    infoTributaria.setAmbiente(conexion.rs.getString("codigo_ambiente"));
                    infoTributaria.setTipoEmision(conexion.rs.getString("codigo_emision"));
                    infoTributaria.setRazonSocial(conexion.rs.getString("razon_social"));
                    infoTributaria.setNombreComercial(conexion.rs.getString("nombre_comercial"));
                    infoTributaria.setRuc(conexion.rs.getString("ruc"));
                    infoTributaria.setCodDoc(TipoComprobanteEnum.NOTA_DE_CREDITO.getCode());
                    infoTributaria.setEstab(conexion.rs.getString("codigo_establecimiento"));
                    infoTributaria.setPtoEmi(conexion.rs.getString("codigo_punto_emision"));
                    infoTributaria.setDirMatriz(conexion.rs.getString("direccion"));
            
                    String claveacceso=new FormGenerales().obtieneClaveDeAcceso(String.format("%09d",conexion.rs.getInt("numero"))
                                            ,conexion.rs.getString("codigo_establecimiento")
                                            ,conexion.rs.getString("codigo_punto_emision")
                                            ,conexion.rs.getString("codigo_emision")
                                            ,conexion.rs.getString("codigo_ambiente")
                                            ,conexion.rs.getString("ruc")
                                            ,conexion.rs.getDate("fecha") 
                                            ,String.format("%08d", 12345678)
                                            ,TipoComprobanteEnum.NOTA_DE_CREDITO.getCode());
                    
                    infoTributaria.setClaveAcceso(claveacceso);
                    
                    notaCreditoXML.setInfoTributaria(infoTributaria);
                    
                    
                    r = clienteDao.getDatosCliente(conexion.rs.getLong("cliente_id"));
                    DatosCliente dc = new DatosCliente();
                    if (r.isEjecuto()) {
                        List<DatosCliente> listaCliente = (List<DatosCliente>) r.getObjeto();
                        if (!listaCliente.isEmpty()) {
                            dc = listaCliente.get(0);
                            if(dc.getEmail()==null){
                                dc.setEmail(".");
                            }
                        }
                    }
                    infoNotaCredito = notaCreditoFactory.createNotaCreditoInfoNotaCredito();
                    
                    infoNotaCredito.setFechaEmision(Constantes.dateFormat.format(conexion.rs.getDate("fecha")));
                    infoNotaCredito.setDirEstablecimiento(conexion.rs.getString("direccion"));
                    infoNotaCredito.setTipoIdentificacionComprador(dc.getTipoIdentificacion());
                    infoNotaCredito.setIdentificacionComprador(dc.getIdentificacion());
                    infoNotaCredito.setRazonSocialComprador(dc.getRazonSocial());
                    
                    infoNotaCredito.setTotalSinImpuestos(conexion.rs.getBigDecimal("subtotal").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
//                    infoNotaCredito.setTotalDescuento(conexion.rs.getBigDecimal("total_descuento").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
//                    infoNotaCredito.setPropina(conexion.rs.getBigDecimal("propina").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoNotaCredito.setCodDocModificado(TipoComprobanteEnum.FACTURA.getCode());
                    infoNotaCredito.setNumDocModificado(conexion.rs.getString("origen"));
                    infoNotaCredito.setFechaEmisionDocSustento(Constantes.dateFormat.format(conexion.rs.getDate("fecha_contable")));
                    
                    infoNotaCredito.setValorModificacion(conexion.rs.getBigDecimal("total").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    infoNotaCredito.setMoneda("DOLAR");
                    infoNotaCredito.setMotivo(conexion.rs.getString("motivo"));
           
                    RespuestaFuncion res = facturaImpuestoDao.getNotaCreditoImpuesto(conexion.rs.getLong("id"));
                    List<NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto> listaImpuestos = (List<NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto>) res.getObjeto();
                    NotaCredito.InfoNotaCredito.TotalConImpuestos respuestaImpuesto = notaCreditoFactory.createNotaCreditoInfoNotaCreditoTotalConImpuestos();
                    
                    for(NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto i:listaImpuestos){
                        respuestaImpuesto.getTotalImpuesto().add(i);
                    }
                    infoNotaCredito.setTotalConImpuestos(respuestaImpuesto);                    
                    
                    infoNotaCredito.setContribuyenteEspecial(conexion.rs.getString("contribuyente_especial"));
                    infoNotaCredito.setObligadoContabilidad("SI");
                    
//                    NotaCredito.Pago pago = this.notaCreditoFactory.createFormaPago();
//                    NotaCredito.Pago.DetallePago detallePago=new NotaCredito.Pago.DetallePago();
//                    detallePago.setFormaPago(conexion.rs.getString("forma_pago"));
//                    detallePago.setTotal(conexion.rs.getBigDecimal("total").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
//                    pago.getPagos().add(detallePago);
//                    infoNotaCredito.setPagos(pago);
                    
                    notaCreditoXML.setInfoNotaCredito(infoNotaCredito);
                    
                    NotaCredito.InfoAdicional info = this.notaCreditoFactory.createNotaCreditoInfoAdicional();
                    
                    NotaCredito.InfoAdicional.CampoAdicional detalleDireccion = new NotaCredito.InfoAdicional.CampoAdicional();
                    detalleDireccion.setNombre("DIRECCION");
                    detalleDireccion.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getCallePrincipal() + " " + dc.getCalleSecundaria()));                    
                    info.getCampoAdicional().add(detalleDireccion);
                    
                    NotaCredito.InfoAdicional.CampoAdicional detalleTelefono = new NotaCredito.InfoAdicional.CampoAdicional();
                    detalleTelefono.setNombre("TELEFONO");
                    detalleTelefono.setValue(dc.getTelefonoConvencional());
                    info.getCampoAdicional().add(detalleTelefono);
                    
                    NotaCredito.InfoAdicional.CampoAdicional detalleEmail = new NotaCredito.InfoAdicional.CampoAdicional();
                    detalleEmail.setNombre("CORREO ELECTRONICO");
                    detalleEmail.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getEmail()));
                    info.getCampoAdicional().add(detalleEmail);
                    
                    if (info.getCampoAdicional().size() > 0) {
                        notaCreditoXML.setInfoAdicional(info);
                    }
                    notaCreditoXML.setVersion("1.0.0");
                    notaCreditoXML.setId("comprobante");
                    
                    RespuestaFuncion re = facturaDetalleDao.getNotaCreditoDetalle(conexion.rs.getLong("id"));
                    List<NotaCredito.Detalles.Detalle> listaDetalleFactura = (List<NotaCredito.Detalles.Detalle>) re.getObjeto();
                    
                    NotaCredito.Detalles resultado = notaCreditoFactory.createNotaCreditoDetalles();
                    
                    for(NotaCredito.Detalles.Detalle det:listaDetalleFactura){
                        resultado.getDetalle().add(det);
                    }
                    notaCreditoXML.setDetalles(resultado);
                    
                    lista.add(notaCreditoXML);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
}
