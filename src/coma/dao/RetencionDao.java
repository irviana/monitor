/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.man.Comprobante;
import coma.modelo.DatosCliente;
import coma.modelo.DatosProveedor;
import coma.modelo.DatosRetencionInversion;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import jdbc.Conexion;
import sri.modelo.InfoTributaria;
import sri.modelo.retencion.ComprobanteRetencionXML;
import sri.modelo.retencion.ObjectFactory;
import sri.util.FormGenerales;
import sri.util.StringUtil;
import sri.util.TipoComprobanteEnum;
import util.Constantes;
import util.ConstantesCatalogo;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class RetencionDao {

    private final ProveedorDao proveedorDao;
    private final EntidadDao entidadDao;
    private InfoTributaria infoTributaria;
    private ComprobanteRetencionXML.InfoCompRetencion infoCompRetencion;
    private final ObjectFactory factory;
    private final FacturaImpuestoDao facturaImpuestoDao;
    private final ClienteDao clienteDao;
    private final InversionDao inversionDao;

    public RetencionDao() {
        proveedorDao = new ProveedorDao();
        entidadDao = new EntidadDao();
        factory = new ObjectFactory();
        facturaImpuestoDao = new FacturaImpuestoDao();
        clienteDao = new ClienteDao();
        inversionDao = new InversionDao();
    }

    public RespuestaFuncion getComprobantesRetencionFacturaCompra(int numeroComprobantes) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();
        try {
            respuesta = conexion.conectarBaseDatos();            

            if (respuesta.isEjecuto()) {
                String sql = "SELECT r.id"
                        + "        , r.numero_retencion"
                        + "        , r.fecha"
                        + "        , r.fecha_autorizacion"
                        + "        , r.autorizacion_sri"
                        + "        , r.identificacion"
                        + "        , r.codigo_documento"
                        + "        , r.clave_acceso"
                        + "        , r.enviado_sri"
                        + "        , r.recibido_sri"
                        + "        , r.factura_id "
                        + "        , f.proveedor_id "
                        + "        , o.nombre nombre_empresa"
                        + "        , a.id ambiente_id"
                        + "        , e.id emision_id"
                        + "        , a.codigo codigo_ambiente"
                        + "        , e.codigo codigo_emision"
                        + "        , a.nombre nombre_ambiente"
                        + "        , e.nombre nombre_emision"
                        + "        ,(SELECT nombre razon_social FROM adm_oficina WHERE id=f.empresa_id)"
                        + "     FROM coma_retencion r "
                        + "        , coma_factura_cab f "
                        + "        , adm_oficina o "
                        + "        , comd_tipo_ambiente_ce a "
                        + "        , comd_tipo_emision_ce e "
                        + "        , comd_estado_documento d "
                        + "    WHERE r.firmado              = ? "
                        + "      and (r.autorizacion_sri    = ? "
                        + "       or r.enviado_sri          = ? "
                        + "       or r.recibido_sri         = ?) "
                        + "      and r.factura_id           = f.id "
                        + "      and f.oficina_id           = o.id "
                        + "      and o.tipo_ambiente_id     = a.id "
                        + "      and o.tipo_emision_id      = e.id "
                        + "      and f.estado_documento_id  = d.id "
                        + "      and d.nombre               = ? "
                        + "    order by r.id";
                
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setBoolean(1, Constantes.FIRMADO);
                conexion.ps.setBoolean(2, Constantes.SRI_AUTORIZACION);
                conexion.ps.setBoolean(3, Constantes.SRI_ENVIADO);
                conexion.ps.setBoolean(4, Constantes.SRI_RECIBIDO);
                conexion.ps.setString(5, Constantes.ESTADO_VALIDADO);
                conexion.ps.setMaxRows(numeroComprobantes);
                conexion.rs = conexion.ps.executeQuery();
                
                List<Comprobante> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    Comprobante c = new Comprobante();
                    c.setId(conexion.rs.getLong("id"));
                    c.setNumeroComprobante(conexion.rs.getString("numero_retencion"));
                    c.setFechaRegistro(conexion.rs.getDate("fecha"));
                    c.setFechaAutorizacion(conexion.rs.getDate("fecha_autorizacion"));
                    c.setAutorizacionSri(conexion.rs.getBoolean("autorizacion_sri"));
                    c.setIdentificacion(conexion.rs.getString("identificacion"));
                    c.setCodigoDocumento(conexion.rs.getString("codigo_documento"));
                    c.setNombre_xml(conexion.rs.getString("clave_acceso"));    //Es la clave de acceso
                    c.setEnviadoSri(conexion.rs.getBoolean("enviado_sri"));
                    c.setRecibidoSri(conexion.rs.getBoolean("recibido_sri"));
                    c.setIdFacturaRetencion(conexion.rs.getLong("factura_id"));
                    c.setTipoAmbienteId(conexion.rs.getInt("ambiente_id"));
                    c.setTipoEmisionId(conexion.rs.getInt("emision_id"));
                    c.setCodigoAmbiente(conexion.rs.getString("codigo_ambiente"));
                    c.setCodigoEmision(conexion.rs.getString("codigo_emision"));
                    c.setNombreAmbiente(conexion.rs.getString("nombre_ambiente"));
                    c.setNombreEmision(conexion.rs.getString("nombre_emision"));
                    
                    r = proveedorDao.getDatosProveedor(conexion.rs.getLong("proveedor_id"));
                    DatosProveedor dp = new DatosProveedor();
                    if (r.isEjecuto()) {
                        List<DatosProveedor> listaProveedor = (List<DatosProveedor>) r.getObjeto();
                        if (!listaProveedor.isEmpty()) {
                            dp = listaProveedor.get(0);
                            if(dp.getEmail()==null){
                                dp.setEmail(".");
                            }
                        }
                    }
                    c.setIdentificacion(dp.getIdentificacion());
                    c.setRazonSocial(dp.getRazonSocial());
                    c.setCorreoElectronicoProveedor(dp.getEmail());
                    
                    c.setNombreEmpresa(conexion.rs.getString("razon_social"));

                    r = entidadDao.getEntidadId(ConstantesCatalogo.ENTIDAD_RETENCION);
                    Integer entidadId = null;
                    if (r.isEjecuto()) {
                        List<Integer> listaEntidadesIds = (List<Integer>) r.getObjeto();
                        if (!listaEntidadesIds.isEmpty()) {
                            entidadId = listaEntidadesIds.get(0);
                        }
                    }

                    c.setEntidadId(entidadId);
                    lista.add(c);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion getComprobantesRetencionInversion(int numeroComprobantes) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT r.id "
                        + "        , r.numero_retencion "
                        + "        , r.fecha "
                        + "        , r.fecha_autorizacion "
                        + "        , r.autorizacion_sri"
                        + "        , r.identificacion "
                        + "        , r.codigo_documento "
                        + "        , r.clave_acceso "
                        + "        , r.enviado_sri "
                        + "        , r.recibido_sri "
                        + "        , r.inversion_id "
                        + "        , i.cliente_id "
                        + "        , o.nombre nombre_empresa "
                        + "        , a.id ambiente_id "
                        + "        , e.id emision_id "
                        + "        , a.codigo codigo_ambiente "
                        + "        , e.codigo codigo_emision "
                        + "        , a.nombre nombre_ambiente "
                        + "        , e.nombre nombre_emision "
                        + "        , (SELECT nombre razon_social FROM adm_oficina WHERE id=r.empresa_id)"
                        + "        , p.ejercicio_contable_id ejercicio "
                        + "     FROM coma_retencion r "
                        + "        , inva_inversion i "
                        + "        , adm_oficina o "
                        + "        , comd_tipo_ambiente_ce a "
                        + "        , comd_tipo_emision_ce e "
                        + "        , conc_periodo p"
                        + "    WHERE r.firmado               = ? "
                        + "      and (r.autorizacion_sri     = ? "
                        + "       or r.enviado_sri           = ? "
                        + "       or r.recibido_sri          = ?) "
                        + "      and r.inversion_id          = i.id "
                        + "      and i.oficina_id            = o.id "
                        + "      and o.tipo_ambiente_id      = a.id "
                        + "      and o.tipo_emision_id       = e.id "
                        + "      and r.periodo_id            = p.id "
                        + " order by r.id";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setBoolean(1, Constantes.FIRMADO);
                conexion.ps.setBoolean(2, Constantes.SRI_AUTORIZACION);
                conexion.ps.setBoolean(3, Constantes.SRI_ENVIADO);
                conexion.ps.setBoolean(4, Constantes.SRI_RECIBIDO);
                conexion.ps.setMaxRows(numeroComprobantes);
                conexion.rs = conexion.ps.executeQuery();

                List<Comprobante> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    Comprobante c = new Comprobante();
                    c.setId(conexion.rs.getLong("id"));
                    c.setNumeroComprobante(conexion.rs.getString("numero_retencion"));
                    c.setFechaRegistro(conexion.rs.getDate("fecha"));
                    c.setFechaAutorizacion(conexion.rs.getDate("fecha_autorizacion"));
                    c.setAutorizacionSri(conexion.rs.getBoolean("autorizacion_sri"));
                    c.setIdentificacion(conexion.rs.getString("identificacion"));
                    c.setCodigoDocumento(conexion.rs.getString("codigo_documento"));
                    c.setNombre_xml(conexion.rs.getString("clave_acceso"));    //Es la clave de acceso
                    c.setEnviadoSri(conexion.rs.getBoolean("enviado_sri"));
                    c.setRecibidoSri(conexion.rs.getBoolean("recibido_sri"));
                    c.setTipoAmbienteId(conexion.rs.getInt("ambiente_id"));
                    c.setTipoEmisionId(conexion.rs.getInt("emision_id"));
                    c.setCodigoAmbiente(conexion.rs.getString("codigo_ambiente"));
                    c.setCodigoEmision(conexion.rs.getString("codigo_emision"));
                    c.setNombreAmbiente(conexion.rs.getString("nombre_ambiente"));
                    c.setNombreEmision(conexion.rs.getString("nombre_emision"));
                    c.setIdInversion(conexion.rs.getLong("inversion_id"));
                    c.setEjercicio(conexion.rs.getInt("ejercicio"));
                    r = clienteDao.getDatosCliente(conexion.rs.getLong("cliente_id"));
                    DatosCliente dc = new DatosCliente();
                    if (r.isEjecuto()) {
                        List<DatosCliente> listaCliente = (List<DatosCliente>) r.getObjeto();
                        if (!listaCliente.isEmpty()) {
                            dc = listaCliente.get(0);
                            if(dc.getEmail()==null){
                                dc.setEmail(".");
                            }
                        }
                    }
                    c.setIdentificacion(dc.getIdentificacion());
                    c.setRazonSocial(dc.getRazonSocial());
                    c.setCorreoElectronicoCliente(dc.getEmail());
                     
                    c.setNombreEmpresa(conexion.rs.getString("razon_social"));

                    r = entidadDao.getEntidadId(ConstantesCatalogo.ENTIDAD_RETENCION);
                    Integer entidadId = null;
                    if (r.isEjecuto()) {
                        List<Integer> listaEntidadesIds = (List<Integer>) r.getObjeto();
                        if (!listaEntidadesIds.isEmpty()) {
                            entidadId = listaEntidadesIds.get(0);
                        }
                    }

                    c.setEntidadId(entidadId);
                    lista.add(c);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion getComprobantesRetencionFirmar(Long id) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT r.id"
                        + "        , r.numero_retencion"
                        + "        , r.numero"
                        + "        , r.fecha"
                        + "        , r.fecha_autorizacion"
                        + "        , r.autorizacion_sri"
                        + "        , r.identificacion"
                        + "        , r.codigo_documento"
                        + "        , r.clave_acceso"
                        + "        , r.enviado_sri"
                        + "        , r.recibido_sri"
                        + "        , r.factura_id "
                        + "        , f.proveedor_id "
                        + "        , a.codigo codigo_ambiente"
                        + "        , e.codigo codigo_emision"
                        + "        , o.nombre nombre_empresa"
                        + "        , o.nombre_comercial"
                        + "        , o.ruc"
                        + "        , o.codigo_establecimiento"
                        + "        , o.codigo_punto_emision"
                        + "        , o.direccion"
                        + "        , o.contribuyente_especial"
                        + "        , (select nombre razon_social from adm_oficina where id=f.empresa_id)"
                        + "        , p.codigo codigo_periodo"
                        + "     FROM coma_retencion r "
                        + "        , coma_factura_cab f "
                        + "        , adm_oficina o "
                        + "        , comd_tipo_ambiente_ce a "
                        + "        , comd_tipo_emision_ce e "
                        + "        , conc_periodo p"
                        + "    WHERE r.id               = ? "
//                        + "      and r.firmado = ?"
//                        + "      and (r.autorizacion_sri = ?"
//                        + "           or r.enviado_sri = ?"
//                        + "           or r.recibido_sri = ?)"
                        + "      and r.factura_id       = f.id "
                        + "      and f.oficina_id       = o.id"
                        + "      and o.tipo_ambiente_id = a.id"
                        + "      and o.tipo_emision_id  = e.id"
                        + "      and r.periodo_id       = p.id";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
//                conexion.ps.setBoolean(2, Constantes.FIRMADO);
//                conexion.ps.setBoolean(3, Constantes.SRI_AUTORIZACION);
//                conexion.ps.setBoolean(4, Constantes.SRI_ENVIADO);
//                conexion.ps.setBoolean(5, Constantes.SRI_RECIBIDO);
                conexion.rs = conexion.ps.executeQuery();

                List<ComprobanteRetencionXML> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    ComprobanteRetencionXML retencionXML = new ComprobanteRetencionXML();
                    infoTributaria = new InfoTributaria();
                    infoTributaria.setSecuencial(String.format("%09d", conexion.rs.getInt("numero")));
                    infoTributaria.setAmbiente(conexion.rs.getString("codigo_ambiente"));
                    infoTributaria.setTipoEmision(conexion.rs.getString("codigo_emision"));
                    infoTributaria.setRazonSocial(conexion.rs.getString("razon_social"));
                    infoTributaria.setNombreComercial(conexion.rs.getString("nombre_comercial"));
                    infoTributaria.setRuc(conexion.rs.getString("ruc"));
                    infoTributaria.setCodDoc(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode());
                    infoTributaria.setEstab(conexion.rs.getString("codigo_establecimiento"));
                    infoTributaria.setPtoEmi(conexion.rs.getString("codigo_punto_emision"));
                    infoTributaria.setDirMatriz(conexion.rs.getString("direccion"));

                    String claveacceso = new FormGenerales().obtieneClaveDeAcceso(String.format("%09d", conexion.rs.getInt("numero")),
                             conexion.rs.getString("codigo_establecimiento"),
                             conexion.rs.getString("codigo_punto_emision"),
                             conexion.rs.getString("codigo_emision"),
                             conexion.rs.getString("codigo_ambiente"),
                             conexion.rs.getString("ruc"),
                             conexion.rs.getDate("fecha"),
                             String.format("%08d", 12345678),
                             TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode());

                    infoTributaria.setClaveAcceso(claveacceso);

                    retencionXML.setInfoTributaria(infoTributaria);

                    r = proveedorDao.getDatosProveedor(conexion.rs.getLong("proveedor_id"));
                    DatosProveedor dp = new DatosProveedor();
                    if (r.isEjecuto()) {
                        List<DatosProveedor> listaProveedor = (List<DatosProveedor>) r.getObjeto();
                        if (!listaProveedor.isEmpty()) {
                            dp = listaProveedor.get(0);
                            if(dp.getEmail()==null){
                                dp.setEmail(".");
                            }
                        }
                    }

                    infoCompRetencion = factory.createComprobanteRetencionInfoCompRetencion();
                    infoCompRetencion.setFechaEmision(Constantes.dateFormat.format(conexion.rs.getDate("fecha")));
                    infoCompRetencion.setDirEstablecimiento(conexion.rs.getString("direccion"));
                    infoCompRetencion.setTipoIdentificacionSujetoRetenido(dp.getTipoIdentificacion());
                    infoCompRetencion.setIdentificacionSujetoRetenido(dp.getIdentificacion());
                    infoCompRetencion.setRazonSocialSujetoRetenido(dp.getRazonSocial());
                    infoCompRetencion.setPeriodoFiscal(conexion.rs.getString("codigo_periodo"));
                    infoCompRetencion.setContribuyenteEspecial(conexion.rs.getString("contribuyente_especial"));
                    infoCompRetencion.setObligadoContabilidad("SI");
                    retencionXML.setInfoCompRetencion(infoCompRetencion);

                    ComprobanteRetencionXML.InfoAdicional info = this.factory.createComprobanteRetencionInfoAdicional();

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleDireccion = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleDireccion.setNombre("DIRECCION");
                    detalleDireccion.setValue(StringUtil.reemplazarCaracteresEspeciales(dp.getDireccion()));
                    info.getCampoAdicional().add(detalleDireccion);

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleTelefono = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleTelefono.setNombre("TELEFONO");
                    detalleTelefono.setValue(dp.getTelefonoConvencional());
                    info.getCampoAdicional().add(detalleTelefono);

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleEmail = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleEmail.setNombre("CORREO ELECTRONICO");
                    detalleEmail.setValue(StringUtil.reemplazarCaracteresEspeciales(dp.getEmail()));
                    info.getCampoAdicional().add(detalleEmail);

                    if (info.getCampoAdicional().size() > 0) {
                        retencionXML.setInfoAdicional(info);
                    }

                    RespuestaFuncion res = facturaImpuestoDao.getRetencionImpuesto(conexion.rs.getLong("factura_id"));
                    List<sri.modelo.retencion.Impuesto> listaImpuestos = (List<sri.modelo.retencion.Impuesto>) res.getObjeto();

                    ComprobanteRetencionXML.Impuestos resultado = this.factory.createComprobanteRetencionImpuestos();

                    for (sri.modelo.retencion.Impuesto i : listaImpuestos) {
                        resultado.getImpuesto().add(i);
                    }
                    retencionXML.setImpuestos(resultado);

                    retencionXML.setVersion("1.0.0");
                    retencionXML.setId("comprobante");

                    lista.add(retencionXML);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion getComprobantesRetencionInversionesFirmar(Long id) {
        RespuestaFuncion respuesta;
        RespuestaFuncion r;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                 String sql = "SELECT r.id "
                            + "     , r.numero_retencion "
                            + "     , r.numero "
                            + "     , r.fecha "
                            + "     , r.fecha_autorizacion "
                            + "     , r.autorizacion_sri "
                            + "     , r.identificacion "
                            + "     , r.codigo_documento "
                            + "     , r.clave_acceso "
                            + "     , r.enviado_sri "
                            + "     , r.recibido_sri "
                            + "     , r.inversion_id "
                            + "     , i.cliente_id "
                            + "     , ip.codigo_impuesto_app "
                            + "     , ip.codigo_doc_sustento_ce"
                            + "     , a.codigo codigo_ambiente "
                            + "     , e.codigo codigo_emision "
                            + "     , o.nombre nombre_empresa "
                            + "     , o.nombre_comercial "
                            + "     , o.ruc "
                            + "     , o.codigo_establecimiento "
                            + "     , o.codigo_punto_emision "
                            + "     , o.direccion "
                            + "     , o.contribuyente_especial "
                            + "     , (select nombre razon_social from adm_oficina where id=i.empresa_id) "
                            + "     , p.codigo codigo_periodo "
                            + "     , p.ejercicio_contable_id ejercicio "
                            + "  FROM coma_retencion r "
                            + "     , inva_inversion i "
                            + "     , invc_producto ip "
                            + "     , adm_oficina o "
                            + "     , comd_tipo_ambiente_ce a "
                            + "     , comd_tipo_emision_ce e "
                            + "     , conc_periodo p "
                            + " WHERE r.id                 = ? "
//                            + "   and r.firmado            = ? "
//                            + "   and (r.autorizacion_sri  = ? "
//                            + "    or r.enviado_sri        = ? "
//                            + "    or r.recibido_sri       = ?) "
                            + "   and r.inversion_id       = i.id "
                            + "   and i.oficina_id         = o.id "
                            + "   and i.producto_id        = ip.id "
                            + "   and ip.fila_activa       = ? "
                            + "   and o.tipo_ambiente_id   = a.id "
                            + "   and o.tipo_emision_id    = e.id "
                            + "   and r.periodo_id         = p.id ";

                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
//                conexion.ps.setBoolean(2, Constantes.FIRMADO);
//                conexion.ps.setBoolean(3, Constantes.SRI_AUTORIZACION);
//                conexion.ps.setBoolean(4, Constantes.SRI_ENVIADO);
//                conexion.ps.setBoolean(5, Constantes.SRI_RECIBIDO);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();

                List<ComprobanteRetencionXML> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    ComprobanteRetencionXML retencionXML = new ComprobanteRetencionXML();
                    infoTributaria = new InfoTributaria();
                    infoTributaria.setSecuencial(String.format("%09d", conexion.rs.getInt("numero")));
                    infoTributaria.setAmbiente(conexion.rs.getString("codigo_ambiente"));
                    infoTributaria.setTipoEmision(conexion.rs.getString("codigo_emision"));
                    infoTributaria.setRazonSocial(conexion.rs.getString("razon_social"));
                    infoTributaria.setNombreComercial(conexion.rs.getString("nombre_comercial"));
                    infoTributaria.setRuc(conexion.rs.getString("ruc"));
                    infoTributaria.setCodDoc(TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode());
                    infoTributaria.setEstab(conexion.rs.getString("codigo_establecimiento"));
                    infoTributaria.setPtoEmi(conexion.rs.getString("codigo_punto_emision"));
                    infoTributaria.setDirMatriz(conexion.rs.getString("direccion"));

                    String claveacceso = new FormGenerales().obtieneClaveDeAcceso(String.format("%09d", conexion.rs.getInt("numero")),
                             conexion.rs.getString("codigo_establecimiento"),
                             conexion.rs.getString("codigo_punto_emision"),
                             conexion.rs.getString("codigo_emision"),
                             conexion.rs.getString("codigo_ambiente"),
                             conexion.rs.getString("ruc"),
                             conexion.rs.getDate("fecha"),
                             String.format("%08d", 12345678),
                             TipoComprobanteEnum.COMPROBANTE_DE_RETENCION.getCode());

                    infoTributaria.setClaveAcceso(claveacceso);

                    retencionXML.setInfoTributaria(infoTributaria);

                    r = clienteDao.getDatosCliente(conexion.rs.getLong("cliente_id"));
                    DatosCliente dc = new DatosCliente();
                    if (r.isEjecuto()) {
                        List<DatosCliente> listaCliente = (List<DatosCliente>) r.getObjeto();
                        if (!listaCliente.isEmpty()) {
                            dc = listaCliente.get(0);
                            if(dc.getEmail()==null){
                                dc.setEmail(".");
                            }
                        }
                    }

                    infoCompRetencion = factory.createComprobanteRetencionInfoCompRetencion();
                    infoCompRetencion.setFechaEmision(Constantes.dateFormat.format(conexion.rs.getDate("fecha")));
                    infoCompRetencion.setDirEstablecimiento(conexion.rs.getString("direccion"));
                    infoCompRetencion.setTipoIdentificacionSujetoRetenido(dc.getTipoIdentificacion());
                    infoCompRetencion.setIdentificacionSujetoRetenido(dc.getIdentificacion());
                    infoCompRetencion.setRazonSocialSujetoRetenido(dc.getRazonSocial());
                    infoCompRetencion.setPeriodoFiscal(conexion.rs.getString("codigo_periodo"));
                    infoCompRetencion.setContribuyenteEspecial(conexion.rs.getString("contribuyente_especial"));
                    infoCompRetencion.setObligadoContabilidad("SI");
                    retencionXML.setInfoCompRetencion(infoCompRetencion);

                    ComprobanteRetencionXML.InfoAdicional info = this.factory.createComprobanteRetencionInfoAdicional();

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleDireccion = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleDireccion.setNombre("DIRECCION");
                    detalleDireccion.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getCallePrincipal() + " " + dc.getCalleSecundaria()));
                    info.getCampoAdicional().add(detalleDireccion);

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleTelefono = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleTelefono.setNombre("TELEFONO");
                    detalleTelefono.setValue(dc.getTelefonoConvencional());
                    info.getCampoAdicional().add(detalleTelefono);

                    ComprobanteRetencionXML.InfoAdicional.CampoAdicional detalleEmail = new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
                    detalleEmail.setNombre("CORREO ELECTRONICO");
                    detalleEmail.setValue(StringUtil.reemplazarCaracteresEspeciales(dc.getEmail()));
                    info.getCampoAdicional().add(detalleEmail);

                    if (info.getCampoAdicional().size() > 0) {
                        retencionXML.setInfoAdicional(info);
                    }

                    r = inversionDao.getRetencioInversion(conexion.rs.getLong("inversion_id"), conexion.rs.getDate("fecha"));
                    DatosRetencionInversion inv = new DatosRetencionInversion();
                    if (r.isEjecuto()) {
                        List<DatosRetencionInversion> listaRetencionInversion = (List<DatosRetencionInversion>) r.getObjeto();
                        if (!listaRetencionInversion.isEmpty()) {
                            inv = listaRetencionInversion.get(0);

                            RespuestaFuncion res = facturaImpuestoDao.getRetencionImpuestoInversion(conexion.rs.getString("codigo_impuesto_app"),
                                    inv.getBaseImponible(), inv.getImpuestoRenta(),
                                    conexion.rs.getString("codigo_doc_sustento_ce"),
                                    conexion.rs.getDate("fecha"),
                                    conexion.rs.getInt("ejercicio"));

                            List<sri.modelo.retencion.Impuesto> listaImpuestos = (List<sri.modelo.retencion.Impuesto>) res.getObjeto();

                            ComprobanteRetencionXML.Impuestos resultado = this.factory.createComprobanteRetencionImpuestos();

                            for (sri.modelo.retencion.Impuesto i : listaImpuestos) {
                                resultado.getImpuesto().add(i);
                            }
                            retencionXML.setImpuestos(resultado);
                        }
                    }
                    retencionXML.setVersion("1.0.0");
                    retencionXML.setId("comprobante");

                    lista.add(retencionXML);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            Logger.getLogger(this.getClass().getName()).log(Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion insertarDetalleRetencionInversion(String tabla, Long retencionId,
            String nombreImpuesto, BigDecimal base, BigDecimal impuesto) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "INSERT INTO " + tabla + "("
                        + "         retencion_id, "
                        + "         nombre_impuesto,"
                        + "         base, "
                        + "         impuesto, "
                        + "         fecha_registro) "
                        + "   VALUES (?,?,?,?,localtimestamp)";                
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, retencionId);
                conexion.ps.setString(2, nombreImpuesto);
                conexion.ps.setBigDecimal(3, base);
                conexion.ps.setBigDecimal(4, impuesto);
                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");
                
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
