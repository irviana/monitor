/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.man.Comprobante;
import java.sql.SQLException;
import jdbc.Conexion;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class LogComprobanteObjetoDao {

    public LogComprobanteObjetoDao() {
        
    }
    
    public RespuestaFuncion insertarLogComprobanteObjeto(String tabla, byte[] xml,Comprobante comp, 
            String tipo, String estado, String mensajeSri, String observacion) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseObjetos();

            if (respuesta.isEjecuto()) {
                String sql = "INSERT INTO " + tabla + "("
                        + "         entidad_id, "
                        + "         registro_id,"
                        + "         nombre, "
                        + "         numero, "
                        + "         tipo, "
                        + "         xml, "
                        + "         estado, "
                        + "         mensaje_sri, "
                        + "         observacion,"
                        + "         fecha_registro) "
                        + "   VALUES (?,?,?,?,?,?,?,?,?,localtimestamp)";                
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setInt(1, comp.getEntidadId());
                conexion.ps.setLong(2, comp.getId());
                conexion.ps.setString(3, comp.getNombre_xml());
                conexion.ps.setString(4, comp.getNumeroComprobante());
                conexion.ps.setString(5, tipo);
                conexion.ps.setBytes(6, xml);
                conexion.ps.setString(7, estado);
                conexion.ps.setString(8, mensajeSri);
                conexion.ps.setString(9, observacion);
                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");
                
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
