/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.util.ConstantesCompras;
import coma.modelo.PlantillaCorreoElectronico;
import coma.modelo.ServidorCorreoElectronico;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class CorreoElectronicoDao {

    public CorreoElectronicoDao() {
        
    }

    public RespuestaFuncion obtenerPlantillaCorreoElectronico() {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT nombre"
                        + "        , body_html"
                        + "        , email_to"
                        + "        , asunto  "
                        + "     FROM docc_correo_electronico_plantilla"
                        + "    WHERE nombre = ? ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setString(1, ConstantesCompras.PLANTILLA_EMAIL_COMPROBANTE_ELECTRONICO);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<PlantillaCorreoElectronico> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    PlantillaCorreoElectronico pce = new PlantillaCorreoElectronico();
                    pce.setNombre(conexion.rs.getString("nombre"));
                    pce.setBodyHtml(conexion.rs.getString("body_html"));
                    pce.setEmailTo(conexion.rs.getString("email_to"));
                    pce.setAsunto(conexion.rs.getString("asunto"));
                    lista.add(pce);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion obtenerServidorCorreoElectronico() {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT nombre"
                        + "        , smtp_puerto"
                        + "        , smtp_host"
                        + "        , smtp_usuario"
                        + "        , smtp_clave"
                        + "        , smtp_encriptacion"
                        + "     FROM docc_correo_electronico_servidor"
                        + "    WHERE nombre = ? ";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setString(1, Constantes.SERVIDOR_EMAIL);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<ServidorCorreoElectronico> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    ServidorCorreoElectronico sce = new ServidorCorreoElectronico();
                    sce.setNombre(conexion.rs.getString("nombre"));
                    sce.setSmtpPuerto(conexion.rs.getInt("smtp_puerto"));
                    sce.setSmtpHost(conexion.rs.getString("smtp_host"));
                    sce.setSmtpUsuario(conexion.rs.getString("smtp_usuario"));
                    sce.setSmtpClave(conexion.rs.getString("smtp_clave"));
                    sce.setSmtpEncriptacion(conexion.rs.getString("smtp_encriptacion"));
                    lista.add(sce);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
