/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.man.Comprobante;
import coma.modelo.ComaComprobanteObjeto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class ComprobanteObjetoDao {

    public ComprobanteObjetoDao() {
        
    }

    public RespuestaFuncion getComprobantesObjetos(Integer entidadId, Long registroId) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseObjetos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT id"
                        + "        , nombre"
                        + "        , xml"
                        + "     FROM coma_comprobante_objeto"
                        + "    WHERE entidad_id     = ?"
                        + "      AND registro_id    = ?";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setInt(1, entidadId);
                conexion.ps.setLong(2, registroId);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<ComaComprobanteObjeto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    ComaComprobanteObjeto co = new ComaComprobanteObjeto();
                    co.setId(conexion.rs.getLong("id"));
                    co.setNombre(conexion.rs.getString("nombre"));
                    co.setXml(conexion.rs.getBytes("xml"));
                    lista.add(co);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion insertarComprobanteObjeto(String tabla, byte[] xml, byte[] ride,Comprobante comp) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseObjetos();

            if (respuesta.isEjecuto()) {
                String sql = "INSERT INTO " + tabla + "("
                        + "         entidad_id, "
                        + "         registro_id,"
                        + "         nombre, "
                        + "         xml, "
                        + "         ride, "
                        + "         observacion,"
                        + "         fecha_registro) "
                        + "   VALUES (?,?,?,?,?,?,localtimestamp)";                
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setInt(1, comp.getEntidadId());
                conexion.ps.setLong(2, comp.getId());
                conexion.ps.setString(3, comp.getNombre_xml());
                conexion.ps.setBytes(4, xml);
                conexion.ps.setBytes(5, ride);
                conexion.ps.setString(6, Constantes.OBSERVACION_COMPROBANTE__OBJETO);
                conexion.ps.executeUpdate();

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setMensaje("");
                
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
