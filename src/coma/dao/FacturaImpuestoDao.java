/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import coma.man.ImpuestosRetencion;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jdbc.Conexion;
import sri.modelo.factura.FacturaXML;
import sri.modelo.factura.ObjectFactory;
import sri.modelo.notacredito.NotaCredito;
import util.Constantes;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class FacturaImpuestoDao {

    
    private final ObjectFactory facturaFactory;
    private final sri.modelo.retencion.ObjectFactory factory;
    private final sri.modelo.notacredito.ObjectFactory factoryNotaCredito;
    
    public FacturaImpuestoDao() {
        facturaFactory=new ObjectFactory();
        factory=new sri.modelo.retencion.ObjectFactory();
        factoryNotaCredito= new sri.modelo.notacredito.ObjectFactory();
    } 
    
    public RespuestaFuncion getFacturaVentaImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.nombre"
                        + "        , f.base"
                        + "        , f.impuesto"
                        + "        , g.codigo_ce codigo_grupo"
                        + "        , i.codigo_ce codigo_impuesto"
                        + "     FROM coma_factura_impuesto f, comc_grupo_impuesto g, comc_impuesto i"
                        + "    WHERE f.factura_id        = ? "
                        + "      AND f.visible           = ?"
                        + "      AND f.impuesto_id       = i.id"
                        + "      AND f.grupo_impuesto_id = g.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);
                
                List<FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    
                    FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto impuesto = this.facturaFactory.createFacturaInfoFacturaTotalConImpuestosTotalImpuesto();
                    
                    impuesto.setCodigo(conexion.rs.getString("codigo_grupo"));
                    impuesto.setCodigoPorcentaje(conexion.rs.getString("codigo_impuesto"));
                    impuesto.setBaseImponible(conexion.rs.getBigDecimal("base").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    impuesto.setValor(conexion.rs.getBigDecimal("impuesto").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    
                    lista.add(impuesto);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getNotaCreditoImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.nombre"
                        + "        , f.base"
                        + "        , f.impuesto"
                        + "        , g.codigo_ce codigo_grupo"
                        + "        , i.codigo_ce codigo_impuesto"
                        + "     FROM coma_factura_impuesto f, comc_grupo_impuesto g, comc_impuesto i"
                        + "    WHERE f.factura_id        = ? "
                        + "      AND f.visible           = ?"
                        + "      AND f.impuesto_id       = i.id"
                        + "      AND f.grupo_impuesto_id = g.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);
                
                List<NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    
                    NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto impuesto = this.factoryNotaCredito.createNotaCreditoInfoNotacCreditoTotalConImpuestosTotalImpuesto();
                    
                    impuesto.setCodigo(conexion.rs.getString("codigo_grupo"));
                    impuesto.setCodigoPorcentaje(conexion.rs.getString("codigo_impuesto"));
                    impuesto.setBaseImponible(conexion.rs.getBigDecimal("base").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    impuesto.setValor(conexion.rs.getBigDecimal("impuesto").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    
                    lista.add(impuesto);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getNotaDebitoImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT f.nombre "
                        + "        , f.base "
                        + "        , f.impuesto "
                        + "        , g.codigo_ce codigo_grupo "
                        + "        , i.codigo_ce codigo_impuesto "
                        + "        , i.porcentaje "
                        + "     FROM coma_factura_impuesto f, comc_grupo_impuesto g, comc_impuesto i"
                        + "    WHERE f.factura_id           = ? "
                        + "      AND f.visible              = ?"
                        + "      AND f.impuesto_id          = i.id"
                        + "      AND f.grupo_impuesto_id    = g.id";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setLong(1, id);
                conexion.ps.setBoolean(2, true);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);
                
                List<sri.modelo.notadebito.Impuesto> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    
                    sri.modelo.notadebito.Impuesto impuesto = new sri.modelo.notadebito.Impuesto();
                                        
                    impuesto.setCodigo(conexion.rs.getString("codigo_grupo"));
                    impuesto.setCodigoPorcentaje(conexion.rs.getString("codigo_impuesto"));
                    impuesto.setBaseImponible(conexion.rs.getBigDecimal("base").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    impuesto.setValor(conexion.rs.getBigDecimal("impuesto").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    impuesto.setTarifa(conexion.rs.getBigDecimal("porcentaje").setScale(Constantes.NUMERO_DIGITOS_ESTANDAR, RoundingMode.HALF_UP));
                    lista.add(impuesto);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }

    public RespuestaFuncion getRetencionImpuesto(Long id) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
                respuesta = conexion.conectarBaseDatos();

                if (respuesta.isEjecuto()) {
                    String sql = "SELECT f.nombre"
                            + "        , f.base"
                            + "        , f.impuesto"
                            + "        , f.porcentaje"
                            + "        , c.numero"
                            + "        , c.fecha"
                            + "        , g.codigo_ce codigo_grupo"
                            + "        , i.codigo_ce codigo_impuesto"
                            + "        , a.serie_entidad"
                            + "        , a.serie_emision"
                            + "        , t.codigo_ce"
                            + "     FROM coma_factura_impuesto f"
                            + "        , comc_grupo_impuesto g"
                            + "        , comc_impuesto i"
                            + "        , coma_factura_cab c"
                            + "        , coma_autorizacion_sri a"
                            + "        , comd_tipo_comprobante t"
                            + "    WHERE f.factura_id           = ? "
                            + "      AND f.visible              = ?"
                            + "      AND f.impuesto_id          = i.id"
                            + "      AND f.grupo_impuesto_id    = g.id"
                            + "      AND f.retencion_id is not null"
                            + "      AND f.factura_id           = c.id"
                            + "      AND c.autorizacion_sri_id  = a.id"
                            + "      AND a.tipo_comprobante_id  = t.id";
                    conexion.ps = conexion.cx.prepareStatement(sql);
                    conexion.ps.setLong(1, id);
                    conexion.ps.setBoolean(2, true);
                    conexion.rs = conexion.ps.executeQuery();
                    conexion.rs.setFetchSize(1024);

                    List<sri.modelo.retencion.Impuesto> lista=new ArrayList<>();

                    while (conexion.rs.next()) {

                        sri.modelo.retencion.Impuesto ir = this.factory.createImpuesto();

                        ir.setCodigo(conexion.rs.getString("codigo_grupo"));                
                        ir.setCodigoRetencion(conexion.rs.getString("codigo_impuesto"));
                        ir.setPorcentajeRetener(conexion.rs.getBigDecimal("porcentaje"));
                        ir.setBaseImponible(conexion.rs.getBigDecimal("base"));
                        ir.setValorRetenido(conexion.rs.getBigDecimal("impuesto"));
                        ir.setCodDocSustento(conexion.rs.getString("codigo_ce"));
                        String docSustento = conexion.rs.getString("serie_entidad")
                                + conexion.rs.getString("serie_emision")
                                + String.format("%09d", conexion.rs.getInt("serie_emision"));
                        ir.setNumDocSustento(docSustento);
                        ir.setFechaEmisionDocSustento(Constantes.dateFormat.format(conexion.rs.getDate("fecha")));
                    
                        lista.add(ir);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getRetencionImpuestoInversion(String codigoApp
            , BigDecimal base, BigDecimal impuesto, String codigoDocSustento
            , Date fecha, Integer ejercicio) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
                respuesta = conexion.conectarBaseDatos();

                if (respuesta.isEjecuto()) {
                    String sql = "select i.codigo_ce codigo_impuesto " 
                                +"     , i.porcentaje "
                                +"     , g.codigo_ce codigo_grupo " 
                                +"  from comc_impuesto i " 
                                +"     , comc_grupo_impuesto g " 
                                +" where codigo_app              = ? " 
                                +"   and i.ejercicio_contable_id = ? " 
                                +"   and i.visible               = ? " 
                                +"   and i.grupo_impuesto_id     = g.id " 
                                +"   and g.visible               = ? ";
                    conexion.ps = conexion.cx.prepareStatement(sql);
                    conexion.ps.setString(1, codigoApp);
                    conexion.ps.setInt(2, ejercicio);
                    conexion.ps.setBoolean(3, true);
                    conexion.ps.setBoolean(4, true);
                    conexion.rs = conexion.ps.executeQuery();
                    conexion.rs.setFetchSize(1024);

                    List<sri.modelo.retencion.Impuesto> lista=new ArrayList<>();
                    while (conexion.rs.next()) {

                        sri.modelo.retencion.Impuesto ir = this.factory.createImpuesto();

                        ir.setCodigo(conexion.rs.getString("codigo_grupo"));                
                        ir.setCodigoRetencion(conexion.rs.getString("codigo_impuesto"));
                        ir.setPorcentajeRetener(conexion.rs.getBigDecimal("porcentaje"));
                        ir.setBaseImponible(base);
                        ir.setValorRetenido(impuesto);
                        ir.setCodDocSustento(codigoDocSustento);
                        ir.setNumDocSustento(Constantes.NUMERO_DOC_SUSTENTO_INVERSION);
                        ir.setFechaEmisionDocSustento(Constantes.dateFormat.format(fecha));
                        lista.add(ir);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
    public RespuestaFuncion getRetencionNombreImpuestoInversion(String codigoApp,
            Integer ejercicio) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();
        
        try {
                respuesta = conexion.conectarBaseDatos();

                if (respuesta.isEjecuto()) {
                    String sql = "select i.codigo_ce codigo_impuesto " 
                                +"     , i.porcentaje "
                                + "    , i.nombre " 
                                +"     , g.codigo_ce codigo_grupo " 
                                +"  from comc_impuesto i " 
                                +"     , comc_grupo_impuesto g " 
                                +" where codigo_app              = ? " 
                                +"   and i.ejercicio_contable_id = ? " 
                                +"   and i.visible               = ? " 
                                +"   and i.grupo_impuesto_id     = g.id " 
                                +"   and g.visible               = ? ";
                    conexion.ps = conexion.cx.prepareStatement(sql);
                    conexion.ps.setString(1, codigoApp);
                    conexion.ps.setInt(2, ejercicio);
                    conexion.ps.setBoolean(3, true);
                    conexion.ps.setBoolean(4, true);
                    conexion.rs = conexion.ps.executeQuery();

                    List<ImpuestosRetencion> lista=new ArrayList<>();
                    while (conexion.rs.next()) {
                        ImpuestosRetencion ir = new ImpuestosRetencion();
                        ir.setNombreImpuesto(conexion.rs.getString("nombre"));
                        lista.add(ir);
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
    
}
