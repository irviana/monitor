/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Conexion;
import util.RespuestaFuncion;

/**
 *
 * @author ivan
 */
public class EntidadDao {

    public EntidadDao() {

    }

    public RespuestaFuncion getEntidadId(String nombreEntidad) {
        RespuestaFuncion respuesta;
        Conexion conexion = new Conexion();

        try {
            respuesta = conexion.conectarBaseDatos();

            if (respuesta.isEjecuto()) {
                String sql = "SELECT c.id"
                        + "     FROM cat_entidad c"
                        + "    WHERE c.nombre = ?";
                conexion.ps = conexion.cx.prepareStatement(sql);
                conexion.ps.setString(1, nombreEntidad);
                conexion.rs = conexion.ps.executeQuery();
                conexion.rs.setFetchSize(1024);

                List<Integer> lista = new ArrayList<>();
                while (conexion.rs.next()) {
                    lista.add(conexion.rs.getInt("id"));
                }

                respuesta = new RespuestaFuncion();
                respuesta.setEjecuto(true);
                respuesta.setObjeto(lista);

            } else {
                org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, respuesta.getMensaje());
            }
        } catch (SQLException ex) {
            respuesta = new RespuestaFuncion();
            respuesta.setEjecuto(false);
            respuesta.setMensaje(ex.getMessage());
            org.apache.log4j.Logger.getLogger(this.getClass().getName()).log(org.apache.log4j.Level.FATAL, ex.getMessage());
        }

        conexion.desconectar();

        return respuesta;
    }
}
