/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.modelo;

/**
 *
 * @author ivan
 */
public class ServidorCorreoElectronico {
    
    private String nombre;
    private int smtpPuerto;
    private String smtpHost;
    private String smtpUsuario;
    private String smtpClave;
    private String smtpEncriptacion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSmtpPuerto() {
        return smtpPuerto;
    }

    public void setSmtpPuerto(int smtpPuerto) {
        this.smtpPuerto = smtpPuerto;
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public String getSmtpUsuario() {
        return smtpUsuario;
    }

    public void setSmtpUsuario(String smtpUsuario) {
        this.smtpUsuario = smtpUsuario;
    }

    public String getSmtpClave() {
        return smtpClave;
    }

    public void setSmtpClave(String smtpClave) {
        this.smtpClave = smtpClave;
    }

    public String getSmtpEncriptacion() {
        return smtpEncriptacion;
    }

    public void setSmtpEncriptacion(String smtpEncriptacion) {
        this.smtpEncriptacion = smtpEncriptacion;
    }
    
    
}
