/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coma.modelo;

import java.math.BigDecimal;

/**
 *
 * @author ivan
 */
public class DatosRetencionInversion {

    private BigDecimal baseImponible;
    private BigDecimal impuestoRenta;

    public DatosRetencionInversion() {
        baseImponible = BigDecimal.ZERO;
        impuestoRenta = BigDecimal.ZERO;
    }

    public BigDecimal getBaseImponible() {
        return baseImponible;
    }

    public void setBaseImponible(BigDecimal baseImponible) {
        this.baseImponible = baseImponible;
    }

    public BigDecimal getImpuestoRenta() {
        return impuestoRenta;
    }

    public void setImpuestoRenta(BigDecimal impuestoRenta) {
        this.impuestoRenta = impuestoRenta;
    }

    
}
