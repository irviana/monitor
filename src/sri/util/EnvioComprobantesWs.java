 /*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;

import coma.util.ConstantesCompras;
import sri.modelo.Respuesta;
import ec.gob.sri.comprobantes.ws.Comprobante;
import ec.gob.sri.comprobantes.ws.Mensaje;
//import ec.gob.sri.comprobantes.ws.RecepcionComprobantes;
//import ec.gob.sri.comprobantes.ws.RecepcionComprobantesService;
import ec.gob.sri.comprobantes.ws.RespuestaSolicitud;
import ec.gob.sri.comprobantes.ws.RecepcionComprobantesOfflineService;
import ec.gob.sri.comprobantes.ws.RecepcionComprobantesOffline;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class EnvioComprobantesWs {

    //private static RecepcionComprobantesService service;
    private static RecepcionComprobantesOfflineService service;
    private static final String VERSION = "1.0.0";
    public static final String ESTADO_RECIBIDA = "RECIBIDA";
    public static final String ESTADO_DEVUELTA = "DEVUELTA";

    public EnvioComprobantesWs(String wsdlLocation)
            throws MalformedURLException, WebServiceException {
        URL url = new URL(wsdlLocation);
        System.out.println("en envio comprobante Ws: "+wsdlLocation);
        //QName qname = new QName("http://ec.gob.sri.ws.recepcion", ConstantesCompras.RECEPCION_COMPROBANTES_SERVICE);
        QName qname = new QName("http://ec.gob.sri.ws.recepcion", ConstantesCompras.RECEPCION_COMPROBANTES_SERVICE);
        //service = new RecepcionComprobantesService(url, qname);
        service = new RecepcionComprobantesOfflineService(url,qname);
    }

    public static final Object webService(String wsdlLocation) {
        try {
            QName qname = new QName("http://ec.gob.sri.ws.recepcion", ConstantesCompras.RECEPCION_COMPROBANTES_SERVICE);
            //QName qname = new QName("http://ec.gob.sri.ws.recepcion", ConstantesCompras.RECEPCION_COMPROBANTES_SERVICE);
            URL url = new URL(wsdlLocation);
            //service = new RecepcionComprobantesService(url, qname);
            service = new RecepcionComprobantesOfflineService(url, qname);
            return null;
        } catch (MalformedURLException | WebServiceException ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            return ex;
        }
    }

    public RespuestaSolicitud enviarComprobante(String ruc, byte[] archivoXml, String tipoComprobante, String versionXsd) {
        RespuestaSolicitud response;
        System.out.println("en enviarComprobante desde clase EnvioComprobantesWs");
        try {
            //RecepcionComprobantes port = service.getRecepcionComprobantesPort();
            RecepcionComprobantesOffline port = service.getRecepcionComprobantesOfflinePort();
            //response = port.validarComprobante(ArchivoUtils.archivoToByte(xmlFile));
            System.out.println("hubo respuesta");
            response = port.validarComprobante(archivoXml);
            System.out.println("validarComprobante");
        } catch (Exception ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            response = new RespuestaSolicitud();
            response.setEstado(ex.getMessage());
            return response;
        }
        return response;
    }
    /* //// ENVIAR COMPROBANTE DESDE LA UBICACION DE FIRMADOS
    public RespuestaSolicitud enviarComprobante(String ruc, File xmlFile, String tipoComprobante, String versionXsd) {
        RespuestaSolicitud response;
        System.out.println("en enviarComprobante ");
        try {
            //RecepcionComprobantes port = service.getRecepcionComprobantesPort();
            RecepcionComprobantesOffline port = service.getRecepcionComprobantesOfflinePort();
            response = port.validarComprobante(ArchivoUtils.archivoToByte(xmlFile));
        } catch (IOException ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            response = new RespuestaSolicitud();
            response.setEstado(ex.getMessage());
            return response;
        }
        return response;
    }*/
    
    public RespuestaSolicitud enviarComprobanteLotes(String ruc, byte[] xml, String tipoComprobante, String versionXsd) {
        RespuestaSolicitud response;
        try {
            //RecepcionComprobantes port = service.getRecepcionComprobantesPort();
            RecepcionComprobantesOffline port = service.getRecepcionComprobantesOfflinePort();
            response = port.validarComprobante(xml);
        } catch (Exception ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            response = new RespuestaSolicitud();
            response.setEstado(ex.getMessage());
            return response;
        }
        return response;
    }

    public RespuestaSolicitud enviarComprobanteLotes(String ruc, File xml, String tipoComprobante, String versionXsd) {
        RespuestaSolicitud response;
        try {
            //RecepcionComprobantes port = service.getRecepcionComprobantesPort();
            RecepcionComprobantesOffline port = service.getRecepcionComprobantesOfflinePort();
            response = port.validarComprobante(ArchivoUtils.archivoToByte(xml));
        } catch (IOException ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            response = new RespuestaSolicitud();
            response.setEstado(ex.getMessage());
            return response;
        }
        return response;
    }

    public static RespuestaSolicitud obtenerRespuestaEnvio(byte[] archivoXml, String ruc, String tipoComprobante, String claveDeAcceso, String urlWsdl) {
        RespuestaSolicitud respuesta = new RespuestaSolicitud();
        EnvioComprobantesWs cliente;
        System.out.println("en obtenerRespuestaEnvio archivo: "+archivoXml);
        System.out.println("en obtenerRespuestaEnvio urlWsdl: "+urlWsdl);        
        try {
            cliente = new EnvioComprobantesWs(urlWsdl);
        } catch (MalformedURLException | WebServiceException ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            respuesta.setEstado(ex.getMessage());
            return respuesta;
        }
        respuesta = cliente.enviarComprobante(ruc, archivoXml, tipoComprobante, "1.0.0");

        return respuesta;
    }
    /* /////ENVIO DE ARCHIVO DESDE LA UBICACION DE FIRMADOS
    public static RespuestaSolicitud obtenerRespuestaEnvio(File archivo, String ruc, String tipoComprobante, String claveDeAcceso, String urlWsdl) {
        RespuestaSolicitud respuesta = new RespuestaSolicitud();
        EnvioComprobantesWs cliente;
        System.out.println("en obtenerRespuestaEnvio archivo: "+archivo);
        System.out.println("en obtenerRespuestaEnvio urlWsdl: "+urlWsdl);        
        try {
            cliente = new EnvioComprobantesWs(urlWsdl);
        } catch (MalformedURLException | WebServiceException ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            respuesta.setEstado(ex.getMessage());
            return respuesta;
        }
        respuesta = cliente.enviarComprobante(ruc, archivo, tipoComprobante, "1.0.0");

        return respuesta;
    }*/

    public static void guardarRespuesta(String claveDeAcceso, String archivo, boolean estado, java.util.Date fecha) {
        try {
            java.sql.Date sqlDate = new java.sql.Date(fecha.getTime());
            Respuesta item = new Respuesta(null, claveDeAcceso, archivo, estado, sqlDate);
            /*RespuestaSQL resp = new RespuestaSQL();
             resp.insertarRespuesta(item);*/
        } catch (Exception ex) {
            Logger.getLogger(EnvioComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
        }
    }

    public static String obtenerMensajeRespuesta(RespuestaSolicitud respuesta) {
        StringBuilder mensajeDesplegable = new StringBuilder();
        if (respuesta.getEstado().equals("DEVUELTA") == true) {
            RespuestaSolicitud.Comprobantes comprobantes = respuesta.getComprobantes();
            for (Comprobante comp : comprobantes.getComprobante()) {
                mensajeDesplegable.append(comp.getClaveAcceso());
                mensajeDesplegable.append("\n");
                for (Mensaje m : comp.getMensajes().getMensaje()) {
                    mensajeDesplegable.append(m.getMensaje()).append(" :\n");
                    mensajeDesplegable.append(m.getInformacionAdicional() != null ? m.getInformacionAdicional() : "");
                    mensajeDesplegable.append("\n");
                }
                mensajeDesplegable.append("\n");
            }
        }

        return mensajeDesplegable.toString();
    }
}
