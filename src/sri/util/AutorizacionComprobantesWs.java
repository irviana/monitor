/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;

import com.thoughtworks.xstream.XStream;
import coma.util.ConstantesCompras;
import sri.util.xml.XStreamUtil;
import ec.gob.sri.comprobantes.ws.aut.Autorizacion;
//import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantes;
import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantesOffline;
import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantesOfflineService;
//import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantesService;
import ec.gob.sri.comprobantes.ws.aut.Mensaje;
import ec.gob.sri.comprobantes.ws.aut.RespuestaComprobante;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
//import javax.faces.context.FacesContext;
//import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
//import util.Constantes;


//@org.apache.cxf.annotations.EndpointProperty(key = "soap.no.validate.parts", value = "true")
public class AutorizacionComprobantesWs {

    private static Autorizacion autorizacion;
    //private AutorizacionComprobantesService service;
    private AutorizacionComprobantesOfflineService service;
    public static final String ESTADO_AUTORIZADO = "AUTORIZADO";
    public static final String ESTADO_NO_AUTORIZADO = "NO AUTORIZADO";

    public AutorizacionComprobantesWs(String wsdlLocation) {
        try {
            //this.service = new AutorizacionComprobantesService(new URL(wsdlLocation), new QName("http://ec.gob.sri.ws.autorizacion", ConstantesCompras.AUTORIZACION_COMPROBANTES_SERVICE));
            this.service = new AutorizacionComprobantesOfflineService(new URL(wsdlLocation), new QName("http://ec.gob.sri.ws.autorizacion", ConstantesCompras.AUTORIZACION_COMPROBANTES_SERVICE));
        } catch (MalformedURLException ex) {
            Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
        }
    }

    public static Autorizacion getAutorizacion() {
        return autorizacion;
    }

    public static void setAutorizacion(Autorizacion autorizacion) {
        AutorizacionComprobantesWs.autorizacion = autorizacion;
    }
        
    public RespuestaComprobante llamadaWSAutorizacionInd(String claveDeAcceso) {
        RespuestaComprobante response = null;
        try {
            //AutorizacionComprobantes port = this.service.getAutorizacionComprobantesPort();
            AutorizacionComprobantesOffline port = this.service.getAutorizacionComprobantesOfflinePort();
            response = port.autorizacionComprobante(claveDeAcceso);
        } catch (Exception ex) {
            Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            return response;
        }

        return response;
    }
/*
    public RespuestaLote llamadaWsAutorizacionLote(String claveDeAcceso) {
        RespuestaLote response = null;
        try {
            //AutorizacionComprobantes port = this.service.getAutorizacionComprobantesPort();
            AutorizacionComprobantesOffline port = this.service.getAutorizacionComprobantesOfflinePort();
            response = port.autorizacionComprobanteLote(claveDeAcceso);
        } catch (Exception ex) {
            Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
            return response;
        }
        return response;
    }
*/
    public static String autorizarComprobanteIndividual(String claveDeAcceso, String path, String nombreArchivo, String tipoAmbiente) {
        StringBuilder mensaje = new StringBuilder();
        System.out.println("en AutorizacionComprobantesWS -> autorizarComprobanteIndividual");
        try {
            String dirAutorizados = path +"/" + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS + "/";
            String dirNoAutorizados = path +"/"+ ConstantesCompras.PATH_COMPROBANTES_NO_AUTORIZADOS + "/";

            RespuestaComprobante respuesta = null;

            for (int i = 0; i < 5; i++) {
                AutorizacionComprobantesWs cl = new AutorizacionComprobantesWs(FormGenerales.devuelveUrlWs(tipoAmbiente
                        , ConstantesCompras.AUTORIZACION_COMPROBANTES));
                respuesta = cl.llamadaWSAutorizacionInd(claveDeAcceso);
                
                if (!respuesta.getAutorizaciones().getAutorizacion().isEmpty()) {
                    break;
                }
                Thread.currentThread();
                Thread.sleep(300L);
            }
            int i;
            if (respuesta != null) {
                i = 0;
                for (Autorizacion item : respuesta.getAutorizaciones().getAutorizacion()) {
                    mensaje.append(item.getEstado());
                    item.setComprobante("<![CDATA[" + item.getComprobante() + "]]>");
                    XStream xstream = XStreamUtil.getRespuestaXStream();
                    Writer writer = null;
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    writer = new OutputStreamWriter(outputStream, "UTF-8");
                    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    xstream.toXML(item, writer);
                    
                    String xmlAutorizacion = outputStream.toString("UTF-8");
                    //byte[] bytes = xmlAutorizacion.getBytes();
                    
                    if ((i == 0) && (item.getEstado().equals("AUTORIZADO"))) {
                        
                        ArchivoUtils.stringToArchivo(dirAutorizados + File.separator + nombreArchivo, xmlAutorizacion);
                        autorizacion = item;
//                        VisualizacionRideUtil.decodeArchivoBase64(dirAutorizados, dirAutorizados + nombreArchivo, item.getNumeroAutorizacion(), item.getFechaAutorizacion().toString());
                        break;
                    }
                    if (item.getEstado().equals("NO AUTORIZADO")) {
                        autorizacion = item;
                        ArchivoUtils.stringToArchivo(dirNoAutorizados + File.separator + nombreArchivo, xmlAutorizacion);
                        mensaje.append("|").append(obtieneMensajesAutorizacion(item));
                        verificarOCSP(item);
                        break;
                    }
                    i++;
                }
            }

            if ((respuesta == null) || (respuesta.getAutorizaciones().getAutorizacion().isEmpty() == true)) {
                mensaje.append("TRANSMITIDO SIN RESPUESTA|Ha ocurrido un error en el proceso de la Autorización, por lo que se traslado el archivo a la carpeta de: transmitidosSinRespuesta");

                String dirFirmados = path + File.separator + ConstantesCompras.PATH_COMPROBANTES_FIRMADOS + "/";
                String dirTransmitidos = dirFirmados + File.separator + "transmitidosSinRespuesta";

                File transmitidos = new File(dirTransmitidos);
                if (!transmitidos.exists()) {
                    new File(dirTransmitidos).mkdir();
                }

                File archivoFirmado = new File(new File(dirFirmados), nombreArchivo);
                if (!ArchivoUtils.copiarArchivo(archivoFirmado, transmitidos.getPath() + File.separator + nombreArchivo)) {
                    mensaje.append("\nError al mover el archivo a la carpeta de Transmitidos sin Respuesta");
                } else {
                    archivoFirmado.delete();
                }
            }
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
        }
        return mensaje.toString();
    }
/*
    public static List<Autorizacion> autorizarComprobanteLote(String claveDeAcceso, String path, String nombreArchivoLote, int timeout, int cantidadArchivos, String tipoAmbiente) {
        List autorizaciones = new ArrayList();
        RespuestaLote respuestaAutorizacion = null;
        try {
            String dirAutorizados = path + ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS + "/";
            String dirRechazados = path + ConstantesCompras.PATH_COMPROBANTES_RECHAZADOS + "/";
            String dirFirmados = path + ConstantesCompras.PATH_COMPROBANTES_FIRMADOS + "/";

            Thread.currentThread();
            Thread.sleep(timeout * cantidadArchivos * 1000);

            for (int i = 0; i < 5; i++) {
                respuestaAutorizacion = new AutorizacionComprobantesWs(FormGenerales.devuelveUrlWs(tipoAmbiente, ConstantesCompras.AUTORIZACION_COMPROBANTES)).llamadaWsAutorizacionLote(claveDeAcceso);

                if (!respuestaAutorizacion.getAutorizaciones().getAutorizacion().isEmpty()) {
                    break;
                }
                Thread.currentThread();
                Thread.sleep(500L);
            }

            String comprobantesProcesados = respuestaAutorizacion.getNumeroComprobantesLote();
            for (Autorizacion item : respuestaAutorizacion.getAutorizaciones().getAutorizacion()) {
                item.setComprobante("<![CDATA[" + item.getComprobante() + "]]>");
                String claveAcceso = ArchivoUtils.obtieneClaveAccesoAutorizacion(item);

                XStream xstream = XStreamUtil.getRespuestaLoteXStream();
                Writer writer = null;
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                writer = new OutputStreamWriter(outputStream, "UTF-8");
                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                xstream.toXML(item, writer);
                String nombreArchivoConRespuesta = claveAcceso + ".xml";
                String xmlAutorizacion = outputStream.toString("UTF-8");
                if (item.getEstado().equals("AUTORIZADO")) {

                    ArchivoUtils.stringToArchivo(dirAutorizados + File.separator + nombreArchivoConRespuesta, xmlAutorizacion);
                    //VisualizacionRideUtil.decodeArchivoBase64(dirAutorizados, dirAutorizados + File.separator + nombreArchivoConRespuesta, item.getNumeroAutorizacion(), item.getFechaAutorizacion().toString());
                } else {
                    ArchivoUtils.stringToArchivo(dirRechazados + File.separator + nombreArchivoConRespuesta, xmlAutorizacion);
                }

                File archivoABorrar = new File(dirFirmados + File.separator + nombreArchivoConRespuesta);
                File archivoABorrarContingencia = new File(dirFirmados + File.separator + nombreArchivoConRespuesta + "-cont.xml");
                if (archivoABorrar.exists()) {
                    archivoABorrar.delete();
                } else if (archivoABorrarContingencia.exists()) {
                    archivoABorrarContingencia.delete();
                }

                item.setEstado(nombreArchivoConRespuesta + "|" + item.getEstado() + "|" + comprobantesProcesados);
                autorizaciones.add(item);
            }
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.FATAL, null, ex);
        }
        return autorizaciones;
    }*/

    public static String obtieneMensajesAutorizacion(Autorizacion autorizacion) {
        StringBuilder mensaje = new StringBuilder();
        for (Mensaje m : autorizacion.getMensajes().getMensaje()) {
            if (m.getInformacionAdicional() != null) {
                mensaje.append("\n").append(m.getMensaje()).append(": ").append(m.getInformacionAdicional());
            } else {
                mensaje.append("\n").append(m.getMensaje());
            }
        }

        return mensaje.toString();
    }

    public static boolean verificarOCSP(Autorizacion autorizacion) {
        boolean respuesta = true;

        for (Mensaje m : autorizacion.getMensajes().getMensaje()) {
            if (m.getIdentificador().equals("61")) {
                Logger.getLogger(AutorizacionComprobantesWs.class.getName()).log(Level.INFO, "No se puede validar el certificado digital. Deberia emitir en contingencia");
                respuesta = false;
            }
        }
        return respuesta;
    }
}
