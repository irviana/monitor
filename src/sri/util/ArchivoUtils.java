/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;

import coma.man.Comprobante;
import coma.util.ConstantesCompras;
import sri.util.xml.Java2XML;
import sri.util.xml.LectorXPath;
import ec.gob.sri.comprobantes.ws.RespuestaSolicitud;
import ec.gob.sri.comprobantes.ws.aut.Autorizacion;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.sql.SQLException;
import java.util.Calendar;
//import javax.faces.context.FacesContext;
//import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import sri.modelo.factura.FacturaXML;
import sri.modelo.notacredito.NotaCredito;
import sri.modelo.notadebito.NotaDebito;
import sri.modelo.retencion.ComprobanteRetencionXML;
import org.apache.commons.codec.binary.Base64;
import sri.xades.XAdESBESSignature;
//import sri.modelo.ClaveContingencia;
//import sri.modelo.guia.GuiaRemision;
//import sri.modelo.notacredito.NotaCredito;
//import sri.modelo.notadebito.NotaDebito;
import util.RespuestaFuncion;

public class ArchivoUtils {

    private static String errorMensaje;

    public static String archivoToString(String rutaArchivo) {
        StringBuilder buffer = new StringBuilder();
        try {
            FileInputStream fis = new FileInputStream(rutaArchivo);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            try (Reader in = new BufferedReader(isr)) {
                int ch;
                while ((ch = in.read()) > -1) {
                    buffer.append((char) ch);
                }
            }
            return buffer.toString();
        } catch (IOException ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
        }
        return null;
    }

    public static File stringToArchivo(String rutaArchivo, String contenidoArchivo) {
        FileOutputStream fos = null;
        File archivoCreado = null;
        try {
            fos = new FileOutputStream(rutaArchivo);
            try (OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8")) {
                for (int i = 0; i < contenidoArchivo.length(); i++) {
                    out.write(contenidoArchivo.charAt(i));
                }
            }
            archivoCreado = new File(rutaArchivo);
        } catch (IOException ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
            return null;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
            }
        }
        return archivoCreado;
    }

    public static byte[] archivoToByte(File file)
            throws IOException {
        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
            }
        }

        return buffer;
    }

    public static boolean byteToFile(byte[] arrayBytes, String rutaArchivo) {
        boolean respuesta = false;
        try {
            File file = new File(rutaArchivo);
            file.createNewFile();
            OutputStream outputStream;
            try (FileInputStream fileInputStream = new FileInputStream(rutaArchivo)) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrayBytes);
                outputStream = new FileOutputStream(rutaArchivo);
                int data;
                while ((data = byteArrayInputStream.read()) != -1) {
                    outputStream.write(data);
                }
            }
            outputStream.close();
            respuesta = true;
        } catch (IOException ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
        }
        return respuesta;
    }

    public static String obtenerValorXML(File xmlDocument, String expression) {
        String valor = null;
        try {
            LectorXPath reader = new LectorXPath(xmlDocument.getPath());
            valor = (String) reader.leerArchivo(expression, XPathConstants.STRING);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        }

        return valor;
    }

    public static RespuestaFuncion enviarRecibir(String path, Comprobante comp) throws SQLException {
        RespuestaFuncion r = new RespuestaFuncion();
        RespuestaSolicitud respuestaRecepcion;

        try {
            String dirFirmados = path + "/" + ConstantesCompras.PATH_COMPROBANTES_FIRMADOS + "/";
            String nombreArchivo = comp.getNombre_xml() + ".xml";
            String ruc = comp.getIdentificacion();
            String codDoc = comp.getCodigoDocumento();
            String claveDeAcceso = comp.getNombre_xml();

            if (!comp.isEnviadoSri()) {

                File archivoFirmado = new File(dirFirmados + File.separator + nombreArchivo);
                if (!comp.isRecibidoSri()) {
                    long t1 = Calendar.getInstance().getTimeInMillis();
                    respuestaRecepcion = EnvioComprobantesWs.obtenerRespuestaEnvio(comp.getXml(),
                             ruc, codDoc, claveDeAcceso, FormGenerales.devuelveUrlWs(comp.getCodigoAmbiente(),
                                     ConstantesCompras.RECEPCION_COMPROBANTES));
                    long t2 = Calendar.getInstance().getTimeInMillis();
                    System.out.println("EnvioComprobantesWs.obtenerRespuestaEnvio:" + (t2 - t1) + "ms");
                    if (respuestaRecepcion.getEstado().equals("RECIBIDA")) {
                        System.out.println("si recibida");
                        String mensaje = "COMPROBANTE RECIBIDO";

                        r.setEjecuto(true);
                        r.setMensaje(mensaje);
                        r.setObjeto(respuestaRecepcion);

                    } else if (respuestaRecepcion.getEstado().equals("DEVUELTA")) {
                        String resultado = FormGenerales.insertarCaracteres(EnvioComprobantesWs.obtenerMensajeRespuesta(respuestaRecepcion), "\n", 160);
                        String iden = "CODIGO ERROR: " + respuestaRecepcion.getComprobantes().getComprobante().get(0).getMensajes().getMensaje().get(0).getIdentificador();
                        String msg = " MENSAJE: " + respuestaRecepcion.getComprobantes().getComprobante().get(0).getMensajes().getMensaje().get(0).getMensaje();

                        System.out.println("codigo: " + iden + " msg: " + msg);
                        String mensaje = iden + msg;

                        String dirRechazados = dirFirmados + "rechazados";

                        r.setEjecuto(false);
                        //r.setMensaje(mensaje);
                        r.setMensaje(iden + ": " + resultado);
                        r.setObjeto(respuestaRecepcion);

                        byteToFile(comp.getXml(), dirFirmados + File.separator + nombreArchivo);
                        anadirMotivosRechazo(archivoFirmado, respuestaRecepcion);

                        File rechazados = new File(dirRechazados);
                        if (!rechazados.exists()) {
                            new File(dirRechazados).mkdir();
                        }

                        if (!copiarArchivo(archivoFirmado, rechazados.getPath() + File.separator + nombreArchivo)) {
                            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, "Error al mover archivo a carpeta rechazados");
                        } else {
                            archivoFirmado.delete();
                        }
                        Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, "Error al tratar de enviar el comprobante hacia el SRI:\n" + resultado);
                        setErrorMensaje("Error al tratar de enviar el comprobante hacia el SRI:\n" + resultado);
                        comp.setEnviadoSri(true);
                    }
                }
            }
            Thread.currentThread();
            Thread.sleep(2000);
        } catch (Exception ex) {
            r.setEjecuto(false);
            r.setMensaje(ex.getMessage());
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        }
        return r;
    }

    public static RespuestaFuncion enviarAutorizar(String path, Comprobante comp) throws SQLException {
        RespuestaFuncion r = new RespuestaFuncion();
        String respAutorizacion;
        Autorizacion autorizacion = null;

        try {
            String nombreArchivo = comp.getNombre_xml() + ".xml";
            String claveDeAcceso = comp.getNombre_xml();

            String archivoACrear = path + "/" + ConstantesCompras.PATH_COMPROBANTES_GENERADOS + "/";
            File archivoCreado = new File(archivoACrear + File.separator + nombreArchivo);
            String dirFirmados = path + "/" + ConstantesCompras.PATH_COMPROBANTES_FIRMADOS + "/";
            File archivoFirmado = new File(dirFirmados + File.separator + nombreArchivo);

            if (!comp.isAutorizacionSri() && comp.isRecibidoSri() && comp.isEnviadoSri()) {
                Thread.currentThread();
                Thread.sleep(1000);
                respAutorizacion = AutorizacionComprobantesWs.autorizarComprobanteIndividual(claveDeAcceso, path, nombreArchivo, comp.getCodigoAmbiente());

                if (respAutorizacion.equals("AUTORIZADO")) {
                    autorizacion = AutorizacionComprobantesWs.getAutorizacion();
                    String resultado = "El comprobante " + claveDeAcceso.substring(24, 39) + " fue autorizado por el SRI.";

                    r.setEjecuto(true);
                    r.setMensaje(resultado);
                    r.setObjeto(autorizacion);

                    Logger.getLogger(ArchivoUtils.class.getName()).log(Level.INFO, resultado);

                    comp.setAutorizacionSri(true);
                    archivoFirmado.delete();
                    archivoCreado.delete();

                } else if (respAutorizacion != null) {
                    autorizacion = AutorizacionComprobantesWs.getAutorizacion();
                    String estado = respAutorizacion.substring(0, respAutorizacion.lastIndexOf("|"));
                    String resultado = respAutorizacion.substring(respAutorizacion.lastIndexOf("|") + 1, respAutorizacion.length());

                    r.setMensaje(resultado);
                    r.setEjecuto(false);
                    r.setObjeto(autorizacion);

                    if (autorizacion != null) {
                        String codigoError = "CODIGO ERROR: " + autorizacion.getMensajes().getMensaje().get(0).getIdentificador();
                        String descripcion = " MANSAJE: " + autorizacion.getMensajes().getMensaje().get(0).getMensaje();
                        String mensaje = codigoError + descripcion;

                        //r.setMensaje(mensaje);
                        r.setMensaje(codigoError + ": " + resultado);
                    }
                    Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, "El comprobante fue guardado, firmado y enviado exitósamente, pero no fue Autorizado\n" + estado + "\n" + FormGenerales.insertarCaracteres(resultado, "\n", 160));
                    setErrorMensaje("El comprobante fue guardado, firmado y enviado exitósamente, pero no fue Autorizado\n" + estado + "\n" + FormGenerales.insertarCaracteres(resultado, "\n", 160));
                }
            }
        } catch (Exception ex) {
            r.setEjecuto(false);
            r.setMensaje(ex.getMessage());
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        }
        return r;
    }

    public static String firmarArchivo(String path, String archivoACrear, String dirFirmados, String nombreArchivo) {
        String respuestaFirma = null;
        try {
            String archivoFirmado = dirFirmados + nombreArchivo;

//            String firma = Base64.encodeBase64String((path+"/certificados/liliana_elizabeth_benitez_chamorro.p12").getBytes());            
//            String clave = "U3JpYmNlMTk=";
            String firma = Base64.encodeBase64String((path + "/certificados/liliana_elizabeth_benitez_chamorro.pfx").getBytes());
            String clave = "ZW50cmFkYXMyMzgz";

            XAdESBESSignature signer = new XAdESBESSignature(archivoACrear, archivoFirmado, firma, clave);

            signer.firmarDocumento();
        } catch (Exception ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
            return ex.getMessage();
        }
        return respuestaFirma;
    }

    /*
    public static String validaArchivoXSD(String tipoComprobante, String pathArchivoXML) {
        String respuestaValidacion = null;
        try {
            ValidadorEstructuraDocumento validador = new ValidadorEstructuraDocumento();
            String nombreXsd = seleccionaXsd(tipoComprobante);

            String pathArchivoXSD = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                    .getRealPath(ConstantesCompras.PATH_COMPROBANTES_AUTORIZADOS) + "/" + nombreXsd;

            if (pathArchivoXML != null) {
                validador.setArchivoXML(new File(pathArchivoXML));
                validador.setArchivoXSD(new File(pathArchivoXSD));

                respuestaValidacion = validador.validacion();

            }
        } catch (Exception ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex);
        }
        return respuestaValidacion;
    }*/
 /**/
    private static String realizaMarshal(Object comprobante, String pathArchivo) {
        String respuesta = null;

        if ((comprobante instanceof FacturaXML)) {
            respuesta = Java2XML.marshalFactura((FacturaXML) comprobante, pathArchivo);
        } else if ((comprobante instanceof NotaDebito)) {
            respuesta = Java2XML.marshalNotaDeDebito((NotaDebito) comprobante, pathArchivo);
        } else if ((comprobante instanceof NotaCredito)) {
            respuesta = Java2XML.marshalNotaDeCredito((NotaCredito) comprobante, pathArchivo);
        } else if ((comprobante instanceof ComprobanteRetencionXML)) {
            respuesta = Java2XML.marshalComprobanteRetencion((ComprobanteRetencionXML) comprobante, pathArchivo);
        }
        /*else if ((comprobante instanceof GuiaRemision)) {
            respuesta = Java2XML.marshalGuiaRemision((GuiaRemision) comprobante, pathArchivo);
        }*/
        return respuesta;
    }

    /*
    
     */
    public static String crearArchivoXml2(String pathArchivo, Object objetoModelo, String tipoComprobante) {
        String respuestaCreacion = null;
        if (objetoModelo != null) {
            try {
                respuestaCreacion = realizaMarshal(objetoModelo, pathArchivo);
            } catch (Exception ex) {
                Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
            }
        } else {
            respuestaCreacion = "Ingrese los campos obligatorios del comprobante";
        }
        return respuestaCreacion;
    }

    /*public static String obtieneClaveAccesoAutorizacion(Autorizacion item) {
        String claveAcceso = null;

        String xmlAutorizacion = XStreamUtil.getRespuestaLoteXStream().toXML(item);
        File archivoTemporal = new File("temp.xml");

        stringToArchivo(archivoTemporal.getPath(), xmlAutorizacion);
        String contenidoXML = decodeArchivoBase64(archivoTemporal.getPath());

        if (contenidoXML != null) {
            stringToArchivo(archivoTemporal.getPath(), contenidoXML);
            claveAcceso = obtenerValorXML(archivoTemporal, "/*//*infoTributaria/claveAcceso");
        }

        return claveAcceso;
    }*/

    public static String decodeArchivoBase64(String pathArchivo) {
        String xmlDecodificado = null;
        try {
            File file = new File(pathArchivo);
            if (file.exists()) {
                String encd = obtenerValorXML(file, "/*/comprobante");

                xmlDecodificado = encd;
            } else {
                Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, "File not found!");
            }
        } catch (Exception ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        }
        return xmlDecodificado;
    }

    public static boolean anadirMotivosRechazo(File archivo, RespuestaSolicitud respuestaRecepcion) {
        boolean exito = false;
        File respuesta = new File("respuesta.xml");
        Java2XML.marshalRespuestaSolicitud(respuestaRecepcion, respuesta.getPath());
        if (adjuntarArchivo(respuesta, archivo) == true) {
            exito = true;
            respuesta.delete();
        }
        return exito;
    }

    public static boolean adjuntarArchivo(File respuesta, File comprobante) {
        boolean exito = false;
        try {
            Document document = merge("*", new File[]{comprobante, respuesta});

            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(new OutputStreamWriter(new FileOutputStream(comprobante), "UTF-8"));

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.transform(source, result);

        } catch (Exception ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        }
        return exito;
    }

    private static Document merge(String exp, File[] files)
            throws Exception {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();
        XPathExpression expression = xpath.compile(exp);

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        docBuilderFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document base = docBuilder.parse(files[0]);

        Node results = (Node) expression.evaluate(base, XPathConstants.NODE);
        if (results == null) {
            throw new IOException(files[0] + ": expression does not evaluate to node");
        }

        for (int i = 1; i < files.length; i++) {
            Document merge = docBuilder.parse(files[i]);
            Node nextResults = (Node) expression.evaluate(merge, XPathConstants.NODE);
            results.appendChild(base.importNode(nextResults, true));
        }

        return base;
    }

    public static boolean copiarArchivo(File archivoOrigen, String pathDestino) {
        FileReader in = null;
        boolean resultado = false;
        try {
            File outputFile = new File(pathDestino);
            in = new FileReader(archivoOrigen);
            try (FileWriter out = new FileWriter(outputFile)) {
                int c;
                while ((c = in.read()) != -1) {
                    out.write(c);
                }
                in.close();
            }
            resultado = true;
        } catch (IOException ex) {
            Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(ArchivoUtils.class.getName()).log(Level.FATAL, null, ex.getCause());
            }
        }
        return resultado;
    }

    public static File buscarFicheroXML(String pathInicial, int codigoNumerico) {
        File archivoXML = null;
        File directorioInicial = new File(pathInicial);
        if (directorioInicial.isDirectory()) {
            File[] ficheros = directorioInicial.listFiles();
            for (File fichero : ficheros) {
                if (!fichero.isDirectory()) {
                    if (codigoNumerico == Integer.valueOf(fichero.getName().substring(39, 47))) {
                        archivoXML = new File(pathInicial + fichero.getName());
                    }
                }
            }
        }
        return archivoXML;
    }

    public static File buscarFicheroXMLNumDoc(String pathInicial, String numeroDocumento) {
        File archivoXML = null;
        File directorioInicial = new File(pathInicial);
        if (directorioInicial.isDirectory()) {
            File[] ficheros = directorioInicial.listFiles();
            for (File fichero : ficheros) {
                if (!fichero.isDirectory()) {
                    if (numeroDocumento.equals(fichero.getName().substring(24, 39))) {
                        System.out.println(pathInicial + fichero.getName());
                        archivoXML = new File(pathInicial + fichero.getName());
                    }
                }
            }
        }
        return archivoXML;
    }

    public static String getErrorMensaje() {
        return errorMensaje;
    }

    public static void setErrorMensaje(String errorMensaje) {
        ArchivoUtils.errorMensaje = errorMensaje;
    }

}
