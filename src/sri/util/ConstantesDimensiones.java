package sri.util;

public class ConstantesDimensiones {

    public static final Integer LONGITUD_CEDULA = 10;
    public static final Integer LONGITUD_RUC = 13;
    public static final Integer LONGITUD_PASAPORTE = 13;
    public static final Integer LONGITUD_IDENTIFICACION_EXTERIOR = 20;
    public static final Integer LONGITUD_PLACA = 13;
    public static final Integer LONGITUD_TELEFONO = 15;
    public static final Integer LONGITUD_CELULAR = 10;
    public static final Integer LONGITUD_EXTENSION = 6;
    public static final Integer LONGITUD_SECUENCIAS = 9;
    public static final String VALIDADOR_RUC = "001";
}
