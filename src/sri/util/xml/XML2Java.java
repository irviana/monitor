/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util.xml;

import sri.modelo.factura.FacturaXML;
//import sri.modelo.guia.GuiaRemision;
//import sri.modelo.notacredito.NotaCredito;
//import sri.modelo.notadebito.NotaDebito;
import sri.modelo.retencion.ComprobanteRetencionXML;
import ec.gob.sri.comprobantes.ws.aut.Autorizacion;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * XML2Java
 *
 * Descripción:
 *
 * @author Ivan Viana <irvc22gj@hotmail.com>
 * @version SVN: $Id$
 */
public class XML2Java {

    public static FacturaXML unmarshalFactura(String pathArchivo)
            throws Exception {
        JAXBContext context = JAXBContext.newInstance("sri.modelo.factura");

        Unmarshaller unmarshaller = context.createUnmarshaller();

        FacturaXML item = (FacturaXML) unmarshaller.unmarshal(new InputStreamReader(new FileInputStream(pathArchivo), "UTF-8"));
        return item;
    }
/*
    public static NotaDebito unmarshalNotaDebito(String pathArchivo) throws Exception {
        JAXBContext context = JAXBContext.newInstance("sri.modelo.notadebito");

        Unmarshaller unmarshaller = context.createUnmarshaller();

        NotaDebito item = (NotaDebito) unmarshaller.unmarshal(new InputStreamReader(new FileInputStream(pathArchivo), "UTF-8"));
        return item;
    }

    public static NotaCredito unmarshalNotaCredito(String pathArchivo) throws Exception {
        JAXBContext context = JAXBContext.newInstance("sri.modelo.notacredito");

        Unmarshaller unmarshaller = context.createUnmarshaller();

        NotaCredito item = (NotaCredito) unmarshaller.unmarshal(new InputStreamReader(new FileInputStream(pathArchivo), "UTF-8"));

        return item;
    }

    public static GuiaRemision unmarshalGuiaRemision(String pathArchivo) throws Exception {
        JAXBContext context = JAXBContext.newInstance("sri.modelo.guia");

        Unmarshaller unmarshaller = context.createUnmarshaller();

        GuiaRemision item = (GuiaRemision) unmarshaller.unmarshal(new InputStreamReader(new FileInputStream(pathArchivo), "UTF-8"));

        return item;
    }
*/
    public static ComprobanteRetencionXML unmarshalComprobanteRetencion(String pathArchivo) throws Exception {
        JAXBContext context = JAXBContext.newInstance("sri.modelo.retencion");

        Unmarshaller unmarshaller = context.createUnmarshaller();

        ComprobanteRetencionXML item = (ComprobanteRetencionXML) unmarshaller.unmarshal(new InputStreamReader(new FileInputStream(pathArchivo), "UTF-8"));
        return item;
    }

    public static Autorizacion unmarshalAutorizacion(String pathArchivo) throws Exception {
        JAXBContext context = JAXBContext.newInstance("ec.gob.sri.comprobantes.ws.aut.Autorizacion");

        Unmarshaller unmarshaller = context.createUnmarshaller();
        Autorizacion item = (Autorizacion) unmarshaller.unmarshal(new FileReader(pathArchivo));

        return item;
    }
}
