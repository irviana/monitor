/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util.xml;

import java.io.File;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 * ValidadorEstructuraDocumento
 *
 * Descripción:
 *
 * @author Iván Viana <irvc22gj@hotmail.com>
 * @version SVN: $Id$
 */
public class ValidadorEstructuraDocumento {

    private File archivoXSD;
    private File archivoXML;

    public String validacion() {
        validarArchivo(this.archivoXSD, "archivoXSD");
        validarArchivo(this.archivoXML, "archivoXML");

        String mensaje = null;

        SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        Schema schema;
        try {
            schema = schemaFactory.newSchema(this.archivoXSD);
        } catch (SAXException e) {
            throw new IllegalStateException("Existe un error en la sintaxis del esquema", e);
        }
        Validator validator = schema.newValidator();
        try {
            validator.validate(new StreamSource(this.archivoXML));
        } catch (Exception e) {
            return e.getMessage();
        }
        return mensaje;
    }

    protected void validarArchivo(File archivo, String nombre)
            throws IllegalStateException {
        if ((null == archivo) || (archivo.length() <= 0L)) {
            throw new IllegalStateException(nombre + " es nulo o esta vacio");
        }
    }

    public File getArchivoXSD() {
        return this.archivoXSD;
    }

    public void setArchivoXSD(File archivoXSD) {
        this.archivoXSD = archivoXSD;
    }

    public File getArchivoXML() {
        return this.archivoXML;
    }

    public void setArchivoXML(File archivoXML) {
        this.archivoXML = archivoXML;
    }
}
