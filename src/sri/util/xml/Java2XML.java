/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util.xml;

//import gin.modelo.ats.AnexoTransaccionalSimplificadoXML;
import sri.modelo.factura.FacturaXML;
//import sri.modelo.guia.GuiaRemision;
//import sri.modelo.notacredito.NotaCredito;
//import sri.modelo.notadebito.NotaDebito;
import sri.modelo.retencion.ComprobanteRetencionXML;
import ec.gob.sri.comprobantes.ws.RespuestaSolicitud;
/*import gin.modelo.aps.AnexoParticipesSociosXML;
import gin.modelo.b11.EstadosFinancierosMensualesXML;
import gin.modelo.c01.OperacionesConcedidasXML;
import gin.modelo.c02.SaldosOperacionesXML;
import gin.modelo.c03.GarantesCodeudoresGarantiasXML;
import gin.modelo.c04.BienesRecibidosPagosXMl;
import gin.modelo.d01.DepositosXML;
import gin.modelo.f01.ServiciosFinancierosXML;
import gin.modelo.f13.LiquidezXML;
import gin.modelo.i01.PortafolioInversionesFondosDisponiblesXML;
import gin.modelo.i02.SaldosPortalfoliosInversionesFondosDisponiblesXML;
import gin.modelo.rdep.AnexoRelacionDependenciaXML;
import gin.modelo.resu.ResuXML;
import gin.modelo.rotef.AnexoTransaccionesFinancierasXML;
import gin.modelo.s01.SociosXML;*/
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import sri.modelo.notacredito.NotaCredito;
import sri.modelo.notadebito.NotaDebito;

public class Java2XML {

    public static String marshalFactura(FacturaXML comprobante, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{FacturaXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(comprobante, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }
    
    public static String marshalNotaDeDebito(NotaDebito comprobante, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{NotaDebito.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(comprobante, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalNotaDeCredito(NotaCredito comprobante, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{NotaCredito.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(comprobante, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalComprobanteRetencion(ComprobanteRetencionXML comprobante, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{ComprobanteRetencionXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(comprobante, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }
/*
    public static String marshalGuiaRemision(GuiaRemision comprobante, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{GuiaRemision.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(comprobante, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalATS(AnexoTransaccionalSimplificadoXML ats, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{AnexoTransaccionalSimplificadoXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(ats, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }
*/
    /*
    public static String marshalAPS(AnexoParticipesSociosXML aps, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{AnexoParticipesSociosXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(aps, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalB11(EstadosFinancierosMensualesXML b11, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{EstadosFinancierosMensualesXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.seps.gob.ec/balances src/balance.xsd");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(b11, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalC01(OperacionesConcedidasXML c01, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{OperacionesConcedidasXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(c01, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalC02(SaldosOperacionesXML c02, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{SaldosOperacionesXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(c02, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalC03(GarantesCodeudoresGarantiasXML c03, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{GarantesCodeudoresGarantiasXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(c03, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalC04(BienesRecibidosPagosXMl c04, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{BienesRecibidosPagosXMl.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(c04, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalD01(DepositosXML d01, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{DepositosXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.seps.gob.ec/depositos deposito/deposito.xsd");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(d01, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalF01(ServiciosFinancierosXML f01, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{ServiciosFinancierosXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(f01, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalF13(LiquidezXML f13, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{LiquidezXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.seps.gob.ec/fondos fondos/fondo.xsd");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(f13, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalI01(PortafolioInversionesFondosDisponiblesXML i01, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{PortafolioInversionesFondosDisponiblesXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(i01, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalI02(SaldosPortalfoliosInversionesFondosDisponiblesXML i02, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{SaldosPortalfoliosInversionesFondosDisponiblesXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(i02, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalRDEP(AnexoRelacionDependenciaXML rdep, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{AnexoRelacionDependenciaXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(rdep, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalROTEF(AnexoTransaccionesFinancierasXML rotef, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{AnexoTransaccionesFinancierasXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(rotef, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }

    public static String marshalS01(SociosXML s01, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{SociosXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.seps.gob.ec/socios socios/socios.xsd");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(s01, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }
*/
    public static String marshalRespuestaSolicitud(RespuestaSolicitud respuesta, String pathArchivoSalida) {
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{RespuestaSolicitud.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(respuesta, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return null;
    }
    /*
    public static String marshalResu(ResuXML resu, String pathArchivoSalida) {
        String respuesta = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{ResuXML.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
           // marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.seps.gob.ec/socios socios/socios.xsd");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(pathArchivoSalida), "UTF-8");
            marshaller.marshal(resu, out);
        } catch (JAXBException | FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Java2XML.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return respuesta;
    }*/
}
