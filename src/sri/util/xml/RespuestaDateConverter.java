/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util.xml;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import util.Constantes;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * RespuestaDateConverter
 *
 * Descripción:
 *
 * @author Iván Viana <irvc22gj@hotmail.com>
 * @version SVN: $Id$
 */
public class RespuestaDateConverter
        implements Converter {

    public boolean canConvert(Class clazz) {
        return clazz.equals(XMLGregorianCalendarImpl.class);
    }

    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext mc) {
        System.out.println("en RespuestaDateConverter clase  marshal: ");
        XMLGregorianCalendarImpl i = (XMLGregorianCalendarImpl) o;
        writer.setValue(Constantes.dateTimeFormat.format(i.toGregorianCalendar().getTime()));
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext uc) {
        System.out.println("en RespuestaDateConverter clase  unmarshal: ");
        Date date = null;
        try {
            date = Constantes.dateTimeFormat.parse(reader.getValue());
        } catch (ParseException ex) {
            Logger.getLogger(RespuestaDateConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        XMLGregorianCalendarImpl item = new XMLGregorianCalendarImpl(cal);

        return item;
    }
}
