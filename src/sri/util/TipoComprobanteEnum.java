/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;


public enum TipoComprobanteEnum {

    LOTE("00", "lote.xsd", ""),
    FACTURA("01", "factura.xsd", "FACTURA"),
    NOTA_DE_CREDITO("04", "notaCredito.xsd", "NOTA DE CREDITO"),
    NOTA_DE_DEBITO("05", "notaDebito.xsd", "NOTA DE DEBITO"),
    GUIA_DE_REMISION("06", "guiaRemision.xsd", "GUIA DE REMISION"),
    COMPROBANTE_DE_RETENCION("07", "comprobanteRetencion.xsd", "COMPROBANTE DE RETENCION"),
    LIQUIDACION_DE_COMPRAS("03", "", "LIQ.DE COMPRAS");
    private String code;
    private String xsd;
    private String descripcion;

    private TipoComprobanteEnum(String code, String xsd, String descripcion) {
        this.code = code;
        this.xsd = xsd;
        this.descripcion = descripcion;
    }

    public String getCode() {
        return this.code;
    }

    public String getXsd() {
        return this.xsd;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public static String retornaCodigo(String valor) {
        String codigo = null;

        if (valor.equals(FACTURA.getDescripcion())) {
            codigo = FACTURA.getCode();
        } else if (valor.equals(NOTA_DE_DEBITO.getDescripcion())) {
            codigo = NOTA_DE_DEBITO.getCode();
        } else if (valor.equals(NOTA_DE_CREDITO.getDescripcion())) {
            codigo = NOTA_DE_CREDITO.getCode();
        } else if (valor.equals(COMPROBANTE_DE_RETENCION.getDescripcion())) {
            codigo = COMPROBANTE_DE_RETENCION.getCode();
        } else if (valor.equals(GUIA_DE_REMISION.getDescripcion())) {
            codigo = GUIA_DE_REMISION.getCode();
        } else if (valor.equals(LIQUIDACION_DE_COMPRAS.getDescripcion())) {
            codigo = LIQUIDACION_DE_COMPRAS.getCode();
        }

        return codigo;
    }
}
