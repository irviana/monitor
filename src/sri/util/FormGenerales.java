/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;

import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import javax.xml.ws.WebServiceException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class FormGenerales {

    private static char SEPARADOR_DECIMAL = '.';

    public static String devuelveUrlWs(String ambiente, String nombreServicio) {
        StringBuilder url = new StringBuilder();
        String direccionIPServicio = null;

        if (ambiente.equals(TipoAmbienteEnum.PRODUCCION.getCode()) == true) {
            direccionIPServicio = "https://cel.sri.gob.ec";
        } else if (ambiente.equals(TipoAmbienteEnum.PRUEBAS.getCode()) == true) {
            direccionIPServicio = "https://celcer.sri.gob.ec";
        }
        url.append(direccionIPServicio);
        url.append("/comprobantes-electronicos-ws/");
        url.append(nombreServicio);
        url.append("?wsdl");
        Logger.getLogger(FormGenerales.class.getName()).log(Level.INFO, url.toString());
        return url.toString();
    }

    public static String validarUrl(String tipoAmbiente, String nombreServicio) {
        String mensaje = null;
        String url = devuelveUrlWs(tipoAmbiente, nombreServicio);
        Object c = EnvioComprobantesWs.webService(url);
        if ((c instanceof MalformedURLException)) {
            mensaje = ((MalformedURLException) c).getMessage();
        }
        return mensaje;
    }

    public static boolean existConnection(String tipoAmbiente, String nombreServicio) {
        String url = devuelveUrlWs(tipoAmbiente, nombreServicio);
        int i = 0;
        Boolean respuesta = false;
        while (i < 3) {
            Object c = EnvioComprobantesWs.webService(url);
            if (c == null) {
                respuesta = true;
                break;
            }
            if ((c instanceof WebServiceException)) {
                respuesta = false;
            }

            i++;
        }
        return respuesta;
    }

    public static String insertarCaracteres(String cadenaLarga, String aInsertar, int longitud) {
        StringBuilder sb = new StringBuilder(cadenaLarga);

        int i = 0;
        while ((i = sb.indexOf(" ", i + longitud)) != -1) {
            sb.replace(i, i + 1, aInsertar);
        }

        return sb.toString();
    }

    /*    public ClaveContingencia obtieneClaveDeAcceso(String secuencialComprobante, AdmOficina emisor, String claveAcceso, Date fechaEmision, String claveInterna, String tipoComprobante) {
        ClaveContingencia clave = new ClaveContingencia();
        String serie = new String();
        if (emisor != null) {
            serie = emisor.getCodigoEstablecimiento().concat(emisor.getCodigoPuntoEmision());
        }

        if ((emisor != null) && (emisor.getTipoEmisionId().getCodigo().equals("1"))) {
            claveAcceso = new ClaveAcceso().generaClave(fechaEmision, tipoComprobante, emisor.getRuc(), emisor.getTipoAmbienteId().getCodigo(), serie, secuencialComprobante, claveInterna, "1");

            clave.setCodigoComprobante(claveAcceso);
        } else if ((emisor != null) && ((emisor.getTipoEmisionId().getCodigo().equals("2")) || (emisor.getTipoEmisionId().getCodigo().equals("3")))) {
            try {
                //clave = new ClaveContingenciaSQL().obtenerUltimaNoUsada();

                if (clave.getClave() != null) {
                    claveAcceso = new ClaveAcceso().generaClaveContingencia(fechaEmision, tipoComprobante, clave.getClave(), emisor.getTipoEmisionId().getCodigo());
                    clave.setCodigoComprobante(claveAcceso);
                } else {
                    Logger.getLogger(FormGenerales.class.getName()).log(Level.INFO, "No existen claves de contingencia, por favor cargue claves en el Sistema o cambie su estado de Emisión a: NORMAL");
                }
            } catch (Exception ex) {
                Logger.getLogger(FormGenerales.class.getName()).log(Level.FATAL, null, ex);
            }
        }
        return clave;
    }*/
    public String obtieneClaveDeAcceso(String secuencialComprobante, String codigoEstablecimiento, String codigoPuntoEmision,
             String codigoEmision, String codigoAmbiente, String ruc,
             Date fechaEmision, String claveInterna, String tipoComprobante) {
        String clave = null;
        String serie = new String();
        try {
            if (codigoEstablecimiento != null && codigoPuntoEmision != null) {
                serie = codigoEstablecimiento.concat(codigoPuntoEmision);
            }

            if (codigoEmision.equals("1")) {
                clave = new ClaveAcceso().generaClave(fechaEmision, tipoComprobante, ruc, codigoAmbiente, serie, secuencialComprobante, claveInterna, "1");
            } else {
                if (codigoEmision.equals("2")) {
                    clave = new ClaveAcceso().generaClave(fechaEmision, tipoComprobante, ruc, codigoAmbiente, serie, secuencialComprobante, claveInterna, "2");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(FormGenerales.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
        return clave;
    }

    public static Date eliminaHora(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(11, 0);
        cal.set(12, 0);
        cal.set(13, 0);
        cal.set(14, 0);
        return cal.getTime();
    }
}
