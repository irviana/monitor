/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * StringUtil
 *
 * Descripción:
 *
 * @author Iván Viana <irvc22gj@hotmail.com>
 * @version SVN: $Id$
 */
public class StringUtil {

    public static boolean validateEmail(String email) {
        Pattern p = Pattern.compile("[a-zA-Z0-9]+[.[a-zA-Z0-9_-]+]*@[a-z0-9][\\w\\.-]*[a-z0-9]\\.[a-z][a-z\\.]*[a-z]$");
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static String getTipoIdentificacion(String tipoIddentificacion) {
        if (tipoIddentificacion.equals("TODOS")) {
            return null;
        }
        return TipoIdentificacionEnum.obtenerTipoIdentificacionEnumPorDescripcion(tipoIddentificacion).getCode();
    }

    public static String getSlectedItem(String tipoIddentificacion) {
        return TipoIdentificacionEnum.obtenerTipoIdentificacionEnumPorCodigo(tipoIddentificacion).getDescripcion();
    }

    public static String getSlectedItemTipoProducto(String tipoIddentificacion) {
        if (tipoIddentificacion.equals("B")) {
            return "BIEN";
        }
        if (tipoIddentificacion.equals("S")) {
            return "SERVICIO";
        }
        return null;
    }

    public static String getTipoProducto(String tipoProducto) {
        if (tipoProducto.equals("BIEN")) {
            return "B";
        }
        if (tipoProducto.equals("SERVICIO")) {
            return "S";
        }
        return null;
    }

    public static String getSelectedTipoAmbiente(String tipoIddentificacion) {
        if (tipoIddentificacion.equals("2")) {
            return "PRODUCCION";
        }
        if (tipoIddentificacion.equals("1")) {
            return "PRUEBAS";
        }
        return null;
    }

    public static void convertirMayusculas(JTextField field) {
        ConvertirMayusculas filter = new ConvertirMayusculas();
        ((AbstractDocument) field.getDocument()).setDocumentFilter(filter);
    }

    public static void convertirMinusculas(JTextField field) {
        ConvertirMinusculas filter = new ConvertirMinusculas();
        ((AbstractDocument) field.getDocument()).setDocumentFilter(filter);
    }

    public static String obtenerTipoEmision(String valorCombo) {
        if (valorCombo.equalsIgnoreCase("NORMAL")) {
            return "1";
        }
        if (valorCombo.equalsIgnoreCase("INDISPONIBILIDAD DE SISTEMA")) {
            return "2";
        }
        return null;
    }

    public static String obtenerNumeroTipoEmision(String tipoEmision) {
        if (tipoEmision.equalsIgnoreCase("1")) {
            return "NORMAL";
        }
        if (tipoEmision.equalsIgnoreCase("3")) {
            return "BAJA CONECTIVIDAD";
        }
        if (tipoEmision.equalsIgnoreCase("2")) {
            return "INDISPONIBILIDAD DE SISTEMA";
        }
        return null;
    }

    public static String quitarEnters(String cadenConEnters) {
        String cadenaSinEnters = null;
        for (int x = 0; x < cadenConEnters.length(); x++) {
            if (cadenConEnters.charAt(x) == '\t') {
                cadenaSinEnters = cadenaSinEnters + cadenConEnters.charAt(x);
            }
        }
        return cadenaSinEnters;
    }

    public static boolean validarExpresionRegular(String patron, String valor) {
        if ((patron != null) && (valor != null)) {
            Pattern pattern = Pattern.compile(patron);
            Matcher matcher = pattern.matcher(valor);
            return matcher.matches();
        }
        return false;
    }

    public static String obtenerDocumentoModificado(String codDoc) {
        if ("01".equals(codDoc)) {
            return "FACTURA";
        }
        if ("04".equals(codDoc)) {
            return "NOTA DE CRÉDITO";
        }
        if ("05".equals(codDoc)) {
            return "NOTA DE DÉBITO";
        }
        if ("06".equals(codDoc)) {
            return "GUÍA REMISIÓN";
        }
        if ("07".equals(codDoc)) {
            return "COMPROBANTE DE RETENCIÓN";
        }
        return null;
    }

    /**
     * Función que elimina acentos y caracteres especiales de una cadena de
     * texto.
     *
     * @param input
     * @return cadena de texto limpia de acentos y caracteres especiales.
     */
    public static String reemplazarCaracteresEspeciales(String input) {
        String output = input;
        try{
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        
        for (int i = 0; i < original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        }catch(Exception ex){
            System.out.println("Error: "+ex.getMessage());
            Logger.getLogger(StringUtil.class.getName()).log(Level.FATAL, ex.getMessage(), ex.getCause());
        }
        return output;
    }
}
