package sri.util;

public enum TipoCampo {

    NUMERICO,
    FECHA,
    TEXTO;
}
