/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.modelo.factura;

import javax.xml.bind.annotation.XmlRegistry;
import sri.modelo.InfoTributaria;


@XmlRegistry
public class ObjectFactory {

    public FacturaXML.Detalles.Detalle.DetallesAdicionales createFacturaDetallesDetalleDetallesAdicionales() {
        return new FacturaXML.Detalles.Detalle.DetallesAdicionales();
    }

    public FacturaXML.Detalles.Detalle.DetallesAdicionales.DetAdicional createFacturaDetallesDetalleDetallesAdicionalesDetAdicional() {
        return new FacturaXML.Detalles.Detalle.DetallesAdicionales.DetAdicional();
    }

    public FacturaXML.Detalles createFacturaDetalles() {
        return new FacturaXML.Detalles();
    }

    public FacturaXML.Detalles.Detalle createFacturaDetallesDetalle() {
        return new FacturaXML.Detalles.Detalle();
    }    

    public FacturaXML.InfoFactura createFacturaInfoFactura() {
        return new FacturaXML.InfoFactura();
    }

    public FacturaXML.InfoAdicional createFacturaInfoAdicional() {
        return new FacturaXML.InfoAdicional();
    }

    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    public FacturaXML createFactura() {
        return new FacturaXML();
    }

    public FacturaXML.InfoAdicional.CampoAdicional createFacturaInfoAdicionalCampoAdicional() {
        return new FacturaXML.InfoAdicional.CampoAdicional();
    }

    public FacturaXML.InfoFactura.TotalConImpuestos createFacturaInfoFacturaTotalConImpuestos() {
        return new FacturaXML.InfoFactura.TotalConImpuestos();
    }

    public FacturaXML.Detalles.Detalle.Impuestos createFacturaDetallesDetalleImpuestos() {
        return new FacturaXML.Detalles.Detalle.Impuestos();
    }

    public FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto createFacturaInfoFacturaTotalConImpuestosTotalImpuesto() {
        return new FacturaXML.InfoFactura.TotalConImpuestos.TotalImpuesto();
    }
    
    public FacturaXML.Pago createFormaPago() {
        return new FacturaXML.Pago();
    }
}
