/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.modelo.notacredito;

import sri.modelo.InfoTributaria;
import sri.modelo.factura.FacturaXML.InfoFactura.TotalConImpuestos;
import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {

    public NotaCredito createNotaCredito() {
        return new NotaCredito();
    }

    public Detalle createDetalle() {
        return new Detalle();
    }

    public TotalConImpuestos createTotalConImpuestos() {
        return new TotalConImpuestos();
    }

    public TotalConImpuestos.TotalImpuesto createTotalConImpuestosTotalImpuesto() {
        return new TotalConImpuestos.TotalImpuesto();
    }

    public NotaCredito.InfoNotaCredito createNotaCreditoInfoNotaCredito() {
        return new NotaCredito.InfoNotaCredito();
    }

    public NotaCredito.InfoAdicional.CampoAdicional createNotaCreditoInfoAdicionalCampoAdicional() {
        return new NotaCredito.InfoAdicional.CampoAdicional();
    }

    public NotaCredito.Detalles createNotaCreditoDetalles() {
        return new NotaCredito.Detalles();
    }

    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    public NotaCredito.Detalles.Detalle.DetallesAdicionales.DetAdicional createNotaCreditoDetallesDetalleDetallesAdicionalesDetAdicional() {
        return new NotaCredito.Detalles.Detalle.DetallesAdicionales.DetAdicional();
    }

    public NotaCredito.Detalles.Detalle createNotaCreditoDetallesDetalle() {
        return new NotaCredito.Detalles.Detalle();
    }

    public NotaCredito.Detalles.Detalle.DetallesAdicionales createNotaCreditoDetallesDetalleDetallesAdicionales() {
        return new NotaCredito.Detalles.Detalle.DetallesAdicionales();
    }

    public NotaCredito.InfoAdicional createNotaCreditoInfoAdicional() {
        return new NotaCredito.InfoAdicional();
    }

    public NotaCredito.Detalles.Detalle.Impuestos createNotaCreditoDetallesDetalleImpuestos() {
        return new NotaCredito.Detalles.Detalle.Impuestos();
    }
    
    public NotaCredito.InfoNotaCredito.TotalConImpuestos createNotaCreditoInfoNotaCreditoTotalConImpuestos() {
        return new NotaCredito.InfoNotaCredito.TotalConImpuestos();
    }
    
    public NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto createNotaCreditoInfoNotacCreditoTotalConImpuestosTotalImpuesto() {
        return new NotaCredito.InfoNotaCredito.TotalConImpuestos.TotalImpuesto();
    }
}
