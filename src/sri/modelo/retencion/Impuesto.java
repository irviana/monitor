/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.modelo.retencion;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "impuesto", propOrder = {"codigo", "codigoRetencion", "baseImponible", "porcentajeRetener", "valorRetenido", "codDocSustento", "numDocSustento", "fechaEmisionDocSustento"})
public class Impuesto {

    @XmlElement(required = true)
    protected String codigo;
    @XmlElement(required = true)
    protected String codigoRetencion;
    @XmlElement(required = true)
    protected BigDecimal baseImponible;
    @XmlElement(required = true)
    protected BigDecimal porcentajeRetener;
    @XmlElement(required = true)
    protected BigDecimal valorRetenido;
    @XmlElement(required = true)
    protected String codDocSustento;
    protected String numDocSustento;
    protected String fechaEmisionDocSustento;

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String value) {
        this.codigo = value;
    }

    public String getCodigoRetencion() {
        return this.codigoRetencion;
    }

    public void setCodigoRetencion(String value) {
        this.codigoRetencion = value;
    }

    public BigDecimal getBaseImponible() {
        return this.baseImponible;
    }

    public void setBaseImponible(BigDecimal value) {
        this.baseImponible = value;
    }

    public BigDecimal getPorcentajeRetener() {
        return this.porcentajeRetener;
    }

    public void setPorcentajeRetener(BigDecimal value) {
        this.porcentajeRetener = value;
    }

    public BigDecimal getValorRetenido() {
        return this.valorRetenido;
    }

    public void setValorRetenido(BigDecimal value) {
        this.valorRetenido = value;
    }

    public String getCodDocSustento() {
        return this.codDocSustento;
    }

    public void setCodDocSustento(String value) {
        this.codDocSustento = value;
    }

    public String getNumDocSustento() {
        return this.numDocSustento;
    }

    public void setNumDocSustento(String value) {
        this.numDocSustento = value;
    }

    public String getFechaEmisionDocSustento() {
        return this.fechaEmisionDocSustento;
    }

    public void setFechaEmisionDocSustento(String value) {
        this.fechaEmisionDocSustento = value;
    }
    }
