/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.modelo.retencion;

import sri.modelo.InfoTributaria;
import javax.xml.bind.annotation.XmlRegistry;

/**
 * ObjectFactory
 *
 * Descripción:
 *
 * @author Alcides Rivera <alcides@virtualsami.com.ec>
 * @version SVN: $Id$
 */
@XmlRegistry
public class ObjectFactory {

    public ComprobanteRetencionXML.InfoAdicional createComprobanteRetencionInfoAdicional() {
        return new ComprobanteRetencionXML.InfoAdicional();
    }

    public ComprobanteRetencionXML.Impuestos createComprobanteRetencionImpuestos() {
        return new ComprobanteRetencionXML.Impuestos();
    }

    public ComprobanteRetencionXML.InfoCompRetencion createComprobanteRetencionInfoCompRetencion() {
        return new ComprobanteRetencionXML.InfoCompRetencion();
    }

    public ComprobanteRetencionXML.InfoAdicional.CampoAdicional createComprobanteRetencionInfoAdicionalCampoAdicional() {
        return new ComprobanteRetencionXML.InfoAdicional.CampoAdicional();
    }

    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    public ComprobanteRetencionXML createComprobanteRetencion() {
        return new ComprobanteRetencionXML();
    }

    public Impuesto createImpuesto() {
        return new Impuesto();
    }
}
