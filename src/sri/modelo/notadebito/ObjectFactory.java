/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.modelo.notadebito;

import sri.modelo.InfoTributaria;
import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {

    public NotaDebito.InfoAdicional createNotaDebitoInfoAdicional() {
        return new NotaDebito.InfoAdicional();
    }

    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    public NotaDebito.Motivos createNotaDebitoMotivos() {
        return new NotaDebito.Motivos();
    }

    public NotaDebito.InfoNotaDebito.Impuestos createNotaDebitoInfoNotaDebitoImpuestos() {
        return new NotaDebito.InfoNotaDebito.Impuestos();
    }

    public NotaDebito.InfoNotaDebito createNotaDebitoInfoNotaDebito() {
        return new NotaDebito.InfoNotaDebito();
    }

    public NotaDebito.InfoAdicional.CampoAdicional createNotaDebitoInfoAdicionalCampoAdicional() {
        return new NotaDebito.InfoAdicional.CampoAdicional();
    }

    public Detalle createDetalle() {
        return new Detalle();
    }

    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    public NotaDebito createNotaDebito() {
        return new NotaDebito();
    }

    public NotaDebito.Motivos.Motivo createNotaDebitoMotivosMotivo() {
        return new NotaDebito.Motivos.Motivo();
    }
}
