/*
 *  Copyright (C) 2015 VirtualSAMI Cia. Ltda. <amanda@virtualsami.com.ec>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sri.xades;

import sri.util.key.PassStoreKS;
import es.mityc.firmaJava.libreria.utilidades.UtilidadTratarNodo;
import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.FirmaXML;
import es.mityc.javasign.pkstore.CertStoreException;
import es.mityc.javasign.pkstore.IPKStoreManager;
import es.mityc.javasign.pkstore.keystore.KSStore;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * GenericXMLSignature
 *
 * Descripción:
 *
 * @author Ivan Viana <irvc22gj@hotmail.com>
 * @version SVN: $Id$
 */
public abstract class GenericXMLSignature {

    public static InputStream PKCS12_RESOURCE;
    public static String PKCS12_PASSWORD;
    public static final String OUTPUT_DIRECTORY = ".";

    public GenericXMLSignature(String pkcs12, String pkcs12_password) {
        byte[] decoded_firma = Base64.decodeBase64(pkcs12.getBytes());
        String decodedString_firma = new String(decoded_firma);

        InputStream arch = null;
        try {
            arch = new FileInputStream(decodedString_firma);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
        }
        byte[] decoded = Base64.decodeBase64(pkcs12_password.getBytes());
        String decodedString = new String(decoded);
        PKCS12_RESOURCE = arch;

        PKCS12_PASSWORD = decodedString;
    }

    public GenericXMLSignature() {
    }

    protected void execute() {
        IPKStoreManager storeManager = getPKStoreManager();
        if (storeManager == null) {
            System.err.println("El gestor de claves no se ha obtenido correctamente.");
            return;
        }

        X509Certificate certificate = getFirstCertificate(storeManager);
        if (certificate == null) {
            System.err.println("No existe ningún certificado para firmar.");
            return;
        }
        PrivateKey privateKey;
        try {
            privateKey = storeManager.getPrivateKey(certificate);
        } catch (CertStoreException e) {
            System.err.println("Error al acceder al almacén.");
            return;
        }

        Provider provider = storeManager.getProvider(certificate);

        DataToSign dataToSign = createDataToSign();

        FirmaXML firma = new FirmaXML();

        Document docSigned = null;
        try {
            Object[] res = firma.signFile(certificate, dataToSign, privateKey, provider);
            docSigned = (Document) res[0];
        } catch (Exception ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            return;
        }

        String workingDir = System.getProperty("user.dir");

        String filePath = workingDir + File.separatorChar + getSignatureFileName();
        Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.INFO, "Firma salvada en: " + filePath);
        saveDocumentToFile(docSigned, getSignatureFileName());
    }

    protected abstract DataToSign createDataToSign();

    protected abstract String getSignatureFileName();

    private void saveDocumentToFile(Document document, String pathfile) {
        try {
            FileOutputStream fos = new FileOutputStream(pathfile);
            UtilidadTratarNodo.saveDocumentToOutputStream(document, fos, true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        }
    }

    private void saveDocumentToFileUnsafeMode(Document document, String pathfile) {
        TransformerFactory tfactory = TransformerFactory.newInstance();
        try {
            Transformer serializer = tfactory.newTransformer();

            serializer.transform(new DOMSource(document), new StreamResult(new File(pathfile)));
        } catch (TransformerException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        }
    }

    protected Document getDocument(String resource) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        File file = new File(resource);
        try {
            doc = dbf.newDocumentBuilder().parse(file);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (SAXException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (IOException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        }
        return doc;
    }

    protected String getDocumentAsString(String resource) {
        Document doc = getDocument(resource);
        TransformerFactory tfactory = TransformerFactory.newInstance();

        StringWriter stringWriter = new StringWriter();
        try {
            Transformer serializer = tfactory.newTransformer();
            serializer.transform(new DOMSource(doc), new StreamResult(stringWriter));
        } catch (TransformerException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        }

        return stringWriter.toString();
    }

    private IPKStoreManager getPKStoreManager() {
        IPKStoreManager storeManager = null;
        try {
            KeyStore ks = KeyStore.getInstance("PKCS12");

            ks.load(PKCS12_RESOURCE, PKCS12_PASSWORD.toCharArray());
            storeManager = new KSStore(ks, new PassStoreKS(PKCS12_PASSWORD));
        } catch (KeyStoreException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (CertificateException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        } catch (IOException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, null, ex);
            System.exit(-1);
        }
        return storeManager;
    }

    private X509Certificate getFirstCertificate(IPKStoreManager storeManager) {
        List certs = null;
        try {
            certs = storeManager.getSignCertificates();
        } catch (CertStoreException ex) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, "Fallo obteniendo listado de certificados");
            System.exit(-1);
        }
        if ((certs == null) || (certs.size() == 0)) {
            Logger.getLogger(GenericXMLSignature.class.getName()).log(Level.FATAL, "Lista de certificados vacía");
            System.exit(-1);
        }

        X509Certificate certificate = (X509Certificate) certs.get(0);
        return certificate;
    }
}
