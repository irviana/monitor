<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.6.0.final using JasperReports Library version 6.6.0  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="comprobanteRetencion" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="c3eaa0e8-2300-4d0d-bb98-ea16c8d01451">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="10.90.0.128"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<parameter name="RETENCION_ID" class="java.lang.Long">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="CLAVE_ACCESO" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="PATH_LOGO" class="java.lang.String"/>
	<parameter name="FECHA_AUTORIZACION" class="java.sql.Timestamp"/>
	<parameter name="NUMERO_AUTORIZACION" class="java.lang.String"/>
	<parameter name="EMISION" class="java.lang.String"/>
	<parameter name="AMBIENTE" class="java.lang.String"/>
	<parameter name="TRNX_INTERES" class="java.lang.Integer"/>
	<parameter name="TRNX_IMPUESTO" class="java.lang.Integer"/>
	<parameter name="TIPO_DIRECCION" class="java.lang.Long"/>
	<parameter name="NUM_COMP" class="java.lang.String"/>
	<parameter name="IMPUESTO" class="java.lang.String"/>
	<queryString>
		<![CDATA[select e.ruc as ruc
     , e.nombre as razon_social
     , e.nombre_comercial as nombre_comercial
     , e.direccion as direccion_matriz
     , o.direccion as direccion_sucursal
     , o.contribuyente_especial
     , o.obligado_llevar_contabilidad
     , r.numero_retencion
     , r.numero_autorizacion
     , r.fecha_autorizacion
     , r.total
     , (select nombre 
          from comd_tipo_ambiente_ce 
         where id = r.tipo_ambiente_id) as ambiente
     , (select nombre 
          from comd_tipo_emision_ce 
         where id = r.tipo_emision_id) as emision
     , r.clave_acceso
     , c.identificacion as ruc_cliente
     , c.apellidos_nombres_razon_social as razon_social_cliente
     , d.calle_principal || ' ' || d.calle_secundaria as direccion_cliente
     , d.telefono_convencional || ' ' || d.telefono_celular as telefono_cliente
     , d.correo_electronico as correo_electronico_cliente
     , r.fecha
     , (select t.nombre 
          from comd_tipo_comprobante t
         where t.codigo_ce = '12'
           and t.visible   = true) as nombre_comprobante
     , $P{NUM_COMP} as numero_comprobante
     , r.fecha as fecha_emision_comprobante
     , pr.codigo as ejercicio_fiscal
     , pr.ejercicio_contable_id as ejercicio
     , (select
            sum(case when k.id = $P{TRNX_INTERES} then t.valor_efectivo else 0 end)
       from inva_inversion i
          , inva_transaccion_inversion t
          , invc_transaccion c
          , capc_transaccion_tipo k
      where i.id              = r.inversion_id
        and i.id              = t.inversion_id
        and t.fecha_contable  = r.fecha
        and t.fila_activa     = true
        and t.concepto_id     = c.id
        and c.tipo_id         = k.id
        and k.id              = $P{TRNX_INTERES}) base_imponible
     , (select
            sum(case when k.id = $P{TRNX_IMPUESTO} then t.valor_efectivo else 0 end)
       from inva_inversion i
          , inva_transaccion_inversion t
          , invc_transaccion c
          , capc_transaccion_tipo k
      where i.id              = r.inversion_id
        and i.id              = t.inversion_id
        and t.fecha_contable  = r.fecha
        and t.fila_activa     = true
        and t.concepto_id     = c.id
        and c.tipo_id         = k.id
        and k.id              = $P{TRNX_IMPUESTO}) as valor_retenido
    , p.codigo_impuesto_app codigo_impuesto
    , (select im.porcentaje
         from comc_impuesto im
        where codigo_app               = p.codigo_impuesto_app
          and im.ejercicio_contable_id = pr.ejercicio_contable_id
          and im.visible               = true) as porcentaje
    , $P{IMPUESTO} as impuesto
from coma_retencion r
   , inva_inversion i
   , invc_producto p
   , adm_oficina e
   , adm_oficina o
   , clia_cliente c
   , clia_direccion d
   , conc_periodo pr
where r.inversion_id      = i.id
  and r.empresa_id        = e.id
  and i.oficina_id        = o.id
  and i.producto_id       = p.id
  and i.cliente_id        = c.id
  and d.cliente_id        = c.id
  and r.periodo_id        = pr.id
  and d.tipo_direccion_id = $P{TIPO_DIRECCION}
  and r.id = $P{RETENCION_ID}]]>
	</queryString>
	<field name="ruc" class="java.lang.String"/>
	<field name="razon_social" class="java.lang.String">
		<fieldDescription><![CDATA[Razón social del cliente]]></fieldDescription>
	</field>
	<field name="nombre_comercial" class="java.lang.String"/>
	<field name="direccion_matriz" class="java.lang.String"/>
	<field name="direccion_sucursal" class="java.lang.String"/>
	<field name="contribuyente_especial" class="java.lang.String"/>
	<field name="obligado_llevar_contabilidad" class="java.lang.Boolean"/>
	<field name="numero_retencion" class="java.lang.String">
		<fieldDescription><![CDATA[Número completo de retención Ej: 034-009-000178423]]></fieldDescription>
	</field>
	<field name="numero_autorizacion" class="java.lang.String">
		<fieldDescription><![CDATA[Número de autorización del comprobante electrónico]]></fieldDescription>
	</field>
	<field name="fecha_autorizacion" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[Fecha de autorización del comprobante electrónico]]></fieldDescription>
	</field>
	<field name="ambiente" class="java.lang.String"/>
	<field name="emision" class="java.lang.String"/>
	<field name="clave_acceso" class="java.lang.String">
		<fieldDescription><![CDATA[Clave de acceso del comprobante electrónico]]></fieldDescription>
	</field>
	<field name="ruc_cliente" class="java.lang.String"/>
	<field name="razon_social_cliente" class="java.lang.String"/>
	<field name="direccion_cliente" class="java.lang.String"/>
	<field name="telefono_cliente" class="java.lang.String"/>
	<field name="correo_electronico_cliente" class="java.lang.String"/>
	<field name="fecha" class="java.sql.Date">
		<fieldDescription><![CDATA[Fecha en la que se realizó el cambio de estado]]></fieldDescription>
	</field>
	<field name="nombre_comprobante" class="java.lang.String"/>
	<field name="numero_comprobante" class="java.lang.String">
		<fieldDescription><![CDATA[Es el numero de comprobante, se registrará el id de la transacción realizada en el modulo de captaciones]]></fieldDescription>
	</field>
	<field name="fecha_emision_comprobante" class="java.sql.Date"/>
	<field name="ejercicio_fiscal" class="java.lang.String"/>
	<field name="base_imponible" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Base imponible IVA 0%]]></fieldDescription>
	</field>
	<field name="impuesto" class="java.lang.String">
		<fieldDescription><![CDATA[Monto o valor del impuesto]]></fieldDescription>
	</field>
	<field name="porcentaje" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Porcentaje del impuesto]]></fieldDescription>
	</field>
	<field name="valor_retenido" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<variable name="variable1" class="java.lang.String"/>
	<title>
		<band height="403" splitType="Stretch">
			<rectangle>
				<reportElement mode="Transparent" x="0" y="346" width="557" height="41" uuid="17747c8c-c03b-4e54-969d-eea1d07c39ad"/>
			</rectangle>
			<rectangle radius="10">
				<reportElement x="295" y="0" width="260" height="332" uuid="0fbb1eda-d2af-440c-9d41-7c5691e1cbb7"/>
			</rectangle>
			<staticText>
				<reportElement mode="Transparent" x="301" y="11" width="65" height="20" uuid="21867966-8d25-432f-a854-851322a78ee1"/>
				<textElement verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[R.U.C.:]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="301" y="42" width="247" height="21" uuid="305208e5-f891-40eb-b6a9-111202158746"/>
				<textElement verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[COMPROBANTE DE RETENCIÓN]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="301" y="98" width="211" height="20" uuid="920cba27-408a-4892-99fc-94601fcaa1f7"/>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[NÚMERO DE AUTORIZACIÓN]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="300" y="149" width="116" height="26" uuid="59ed0918-766b-4728-b488-83bb45a10011"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[FECHA Y HORA DE AUTORIZACIÓN]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="300" y="245" width="133" height="20" uuid="75c4b573-7188-4f21-85d8-16a7325c720c"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<text><![CDATA[CLAVE DE ACCESO]]></text>
			</staticText>
			<componentElement>
				<reportElement mode="Transparent" x="300" y="265" width="253" height="50" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="e29bc718-a209-4545-b939-bb42f7e471e9"/>
				<jr:Codabar xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" textPosition="none">
					<jr:codeExpression><![CDATA[$P{CLAVE_ACCESO}]]></jr:codeExpression>
				</jr:Codabar>
			</componentElement>
			<rectangle radius="10">
				<reportElement mode="Transparent" x="0" y="169" width="290" height="165" uuid="56a35a33-f1d8-4b9e-a264-8a30ff9fe9d9"/>
			</rectangle>
			<textField>
				<reportElement mode="Transparent" x="301" y="115" width="253" height="25" uuid="21f91fd0-ee8f-4123-835f-7cff0bd5583a"/>
				<textElement verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{NUMERO_AUTORIZACION}==null ?"NO ENVIADA":$P{NUMERO_AUTORIZACION})]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH:mm:ss">
				<reportElement mode="Transparent" x="400" y="149" width="147" height="26" uuid="cf052b2f-28b2-4441-96ed-2561ec90674a"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($P{FECHA_AUTORIZACION}==null ?"":$P{FECHA_AUTORIZACION})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement mode="Transparent" x="361" y="214" width="186" height="20" uuid="511692f2-ffbb-47ca-9d98-d1fc193f237e"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{EMISION}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="366" y="11" width="182" height="20" uuid="4094ff97-bd66-4708-9ed5-d5394d016698"/>
				<textElement verticalAlignment="Middle">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ruc}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement mode="Transparent" x="300" y="310" width="253" height="25" uuid="ae6b7434-b04a-47b5-b2d5-bb06d09db526"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{CLAVE_ACCESO}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="1" y="346" width="182" height="13" uuid="f083eac7-1599-4343-9fb7-642786ffe32e"/>
				<textElement verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Razón Social / Nombres y Apellidos:]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="369" y="346" width="75" height="13" uuid="5e376151-976f-45be-9b5e-7d04ca942c4a"/>
				<textElement verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Identificación:]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1" y="375" width="87" height="12" uuid="7501feea-7442-4dc7-9e76-55a429bbbf28"/>
				<textElement>
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Emisión:]]></text>
			</staticText>
			<textField>
				<reportElement mode="Transparent" x="160" y="346" width="207" height="13" uuid="731a68dc-7a6e-4153-a391-8b4aef4ec5a0"/>
				<textFieldExpression><![CDATA[$F{razon_social_cliente}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="440" y="346" width="112" height="13" uuid="ec9d05f5-b93b-4b50-9aaf-c875359ec703"/>
				<textFieldExpression><![CDATA[$F{ruc_cliente}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Transparent" x="80" y="374" width="114" height="12" uuid="3903f07f-aa52-4aa0-8db5-e5ffbd97b6b8"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fecha}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="301" y="71" width="31" height="20" uuid="40cd2a76-7047-49f5-a6aa-6f9d48dae9e0"/>
				<textElement verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[No.]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="333" y="71" width="161" height="20" uuid="dee3eadd-80ed-4e05-80ff-1eb3acf2d597"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{numero_retencion}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="301" y="188" width="63" height="20" uuid="c7c89a73-eb5d-4dec-9970-b76375792240"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[AMBIENTE:]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="303" y="213" width="55" height="20" uuid="39e43fce-d35f-4fcc-a4d2-076f38d0c6d3"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[EMISIÓN:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="367" y="188" width="180" height="20" uuid="fd6e0b52-dce4-4002-886f-91c020b24a05"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{AMBIENTE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="66" y="234" width="222" height="13" uuid="d0df42a5-574f-406f-9d2b-38d07522514a"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direccion_matriz}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="234" width="48" height="25" uuid="3b0fbf33-ca05-41c5-ae8c-f8b5233e2ba6"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dirección Matriz:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="17" y="206" width="268" height="13" uuid="ee40bc1a-98bf-4e78-94c0-71cf4c9d12db"/>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nombre_comercial}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="16" y="302" width="169" height="14" uuid="bd17e15a-c8b7-4dd4-828f-e60c7fe4547b">
					<printWhenExpression><![CDATA[$F{contribuyente_especial}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Contribuyente Especial Nro ]]></text>
			</staticText>
			<staticText>
				<reportElement x="17" y="316" width="177" height="14" uuid="28212bb5-1a61-471d-bc40-b95e8c3b0693"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[OBLIGADO A LLEVAR CONTABILIDAD]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="66" y="275" width="222" height="13" uuid="b2f48c66-945c-41be-a667-9afc711a23a5"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direccion_sucursal}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="268" width="51" height="25" uuid="f50c2650-c3d3-42d6-bf00-8fdd42cdf49a"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dirección Sucursal:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="17" y="175" width="268" height="13" uuid="25833196-079b-48ab-933d-c135e6afd768"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{razon_social}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="200" y="302" width="85" height="14" uuid="ffc0dea1-ff17-4eba-a68f-dc2ae3feb687"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{contribuyente_especial}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="200" y="317" width="51" height="14" uuid="d5b2f0f5-6888-4be3-8bfa-40da86dd1d3a"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{obligado_llevar_contabilidad})?"SI":"NO"]]></textFieldExpression>
			</textField>
			<image>
				<reportElement x="0" y="0" width="285" height="163" uuid="ae57d053-7f50-4bba-a80a-1fda405de360"/>
				<imageExpression><![CDATA[$P{PATH_LOGO}]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="28" splitType="Stretch">
			<rectangle>
				<reportElement mode="Transparent" x="0" y="0" width="555" height="28" uuid="5fa39b76-790d-47d4-b184-2f895cfd5157"/>
			</rectangle>
			<staticText>
				<reportElement mode="Transparent" x="276" y="0" width="80" height="28" uuid="7fe6a2b4-1c4e-4cc8-a3f7-e5d20d3d98da"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Base Imponible para la Retención ]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="366" y="0" width="51" height="28" uuid="635582c8-5389-43ce-8eaa-0a7858e4cc03"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[IMPUESTO]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="421" y="0" width="59" height="28" uuid="ce81c746-8fbc-41ad-8c0c-ec0952306a48"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[%Retención]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="481" y="0" width="71" height="28" uuid="49eef277-2900-4d70-ae24-26501afe938a"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Valor Retenido]]></text>
			</staticText>
			<line>
				<reportElement mode="Transparent" x="359" y="0" width="1" height="28" uuid="f963ba26-04d9-4ed3-b90c-34f02808ef5c"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="419" y="0" width="1" height="28" uuid="0522ea40-42c2-4c87-b457-0aa626d9a19b"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="480" y="0" width="1" height="28" uuid="5212fd03-5b63-4e09-bc72-a0d474f0f630"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="270" y="0" width="1" height="28" uuid="9f664492-3d63-4149-b7bb-a97d5ff2d5d4"/>
			</line>
			<staticText>
				<reportElement mode="Transparent" x="216" y="0" width="51" height="28" uuid="f7154d7f-c7e5-489e-a917-fd41b49393ae"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Ejercicio Fiscal]]></text>
			</staticText>
			<line>
				<reportElement mode="Transparent" x="214" y="0" width="1" height="28" uuid="ae410c6e-af3e-40cc-a59f-d50b7cf8e634"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="69" y="0" width="1" height="28" uuid="fc29e7b8-a7ad-4f0b-bf92-ebf73b54a1ed"/>
			</line>
			<staticText>
				<reportElement mode="Transparent" x="2" y="0" width="63" height="28" uuid="537eb80c-6908-48a0-b310-28f4de2413f2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Comprobante]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="71" y="0" width="51" height="28" uuid="63a3a47d-7fa1-4c19-a0e7-b48e1e88f251"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Número]]></text>
			</staticText>
			<line>
				<reportElement mode="Transparent" x="149" y="0" width="1" height="28" uuid="1602817c-457e-498c-8ae0-98e31d1fb9eb"/>
			</line>
			<staticText>
				<reportElement mode="Transparent" x="154" y="0" width="58" height="28" uuid="ab220188-b351-4ba0-831c-6685caad7ae5"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Emisión]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="35" splitType="Stretch">
			<rectangle>
				<reportElement mode="Transparent" x="0" y="0" width="555" height="35" uuid="79629ca2-002c-4c3b-abd0-655f125bc173"/>
			</rectangle>
			<line>
				<reportElement mode="Transparent" x="359" y="0" width="1" height="35" uuid="b27d0858-0890-4d74-a05c-c1b37276c1f1"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="419" y="0" width="1" height="35" uuid="e4050faa-d246-4d9f-b0c8-0241f8985c20"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="480" y="0" width="1" height="35" uuid="e7301ea7-6945-41cd-a90c-c326dfc197c4"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="270" y="0" width="1" height="35" uuid="2632a6ce-6935-44a3-b73e-9f59550f792b"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="214" y="0" width="1" height="35" uuid="67993c0a-12b0-4653-80c6-8607f6bb73a1"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="69" y="0" width="1" height="35" uuid="52abd1c1-b845-4d34-b9c4-cd1106239319"/>
			</line>
			<line>
				<reportElement mode="Transparent" x="149" y="0" width="1" height="35" uuid="97e5ab4b-f310-4ef5-b74d-ffd1a91e2b62"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement x="2" y="0" width="63" height="35" uuid="bafe9e37-cd67-44bb-ac00-b4a595db346c"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nombre_comprobante}==null?"OTRO":$F{nombre_comprobante}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="71" y="0" width="77" height="25" uuid="70c85f03-e527-4b38-9f3c-edd6cc2c4416"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{numero_comprobante}.replaceAll("-","")]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="151" y="0" width="61" height="25" uuid="d0940d08-16f4-4b6b-b25b-8972b7dbfe16"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fecha_emision_comprobante}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="217" y="0" width="51" height="25" uuid="e4b8c083-7d59-41fe-9547-b8d70d4f88a0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ejercicio_fiscal}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="276" y="0" width="80" height="25" uuid="f7e1420e-4e04-4dac-8437-bb6ea5dac20f"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{base_imponible}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement mode="Transparent" x="362" y="0" width="56" height="25" uuid="1096e105-ae0e-40bd-808d-eb250166c8ee"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{impuesto}]]></textFieldExpression>
			</textField>
			<textField pattern="###0;-###0">
				<reportElement mode="Transparent" x="421" y="0" width="59" height="25" uuid="a37d497f-9c3d-4424-8c30-17dff6f2089e"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{porcentaje}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00;-###0.00">
				<reportElement mode="Transparent" x="486" y="0" width="67" height="25" uuid="aa38921b-9f41-48e8-a1bf-b4a8c74adf7e"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{valor_retenido}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="182" splitType="Stretch">
			<rectangle>
				<reportElement mode="Transparent" x="2" y="19" width="351" height="146" uuid="e2898020-e3b7-4ea0-b012-f73e74af9c6f"/>
			</rectangle>
			<staticText>
				<reportElement mode="Transparent" x="8" y="21" width="152" height="13" uuid="b7965416-79aa-4e93-93c4-4160fe3664eb"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Información Adicional]]></text>
			</staticText>
			<staticText>
				<reportElement x="8" y="42" width="133" height="18" uuid="baa0fb43-734c-4dd3-ba24-2e56f1487106"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="none">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[DIRECCIÓN:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="141" y="42" width="199" height="18" uuid="ce411c64-466a-44f5-9c49-7dc2a702df8e"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{direccion_cliente}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="8" y="67" width="133" height="18" uuid="2224f550-05db-42c1-bc71-82fa0a6e1119"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="none">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[TELÉFONO:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="141" y="67" width="199" height="18" uuid="fb88050f-b809-4a87-8fe3-0513d807b81c"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{telefono_cliente}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="8" y="92" width="133" height="18" uuid="58b66a83-5b0f-4adf-810f-400f79fb2f5c"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="none">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[CORREO ELECTRÓNICO:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="141" y="92" width="199" height="18" uuid="a4cda11a-a2cc-46cf-9a29-38ab0f3faf46"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{correo_electronico_cliente}]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement mode="Transparent" x="480" y="0" width="75" height="32" uuid="8d26da34-4da7-444f-a7a8-a0b819a10f81"/>
			</rectangle>
			<textField pattern="###0.00;-###0.00">
				<reportElement mode="Transparent" x="494" y="1" width="51" height="26" uuid="27ea604b-1b63-4d86-912e-b1c8a15321fe"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="424" y="2" width="53" height="18" uuid="7ed218fb-cb7b-4f4e-a4b4-54374f01e38a"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="none">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL:]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
